# Voxel Terrain Engine Exploratory Software
This application uses OpenSimplex noise to generate an open-ended 
3D voxel-based terrain.

Developed by Nathan Challinor in accordance with the requirements for the 
degree of BSc Computer Science at the University of Leeds in 2020/21.

# Running the Application
## Running with IntelliJ
The application was developed using IntelliJ Community Edition and can be 
launched easily by opening the project using IntelliJ.

To run the software using IntelliJ:
 - Open IntelliJ.
 - Click File > Open and select the 'TerrainEngine' project folder.
 - Run the main method found in the Source.Client.Control class.
 
# Using the application
The application will start in the main menu.
 
To create a new world:
 - Select 'Create New World'.
 - (optional) Enter a world name.
 - (optional) Enter a seed.
 - Select 'Create World'
 
To load an existing world:
 - Select 'Load World'.
 - Select the save file you wish to load.
 - Select 'Load World'
 
To edit the render settings:
 - Select 'Settings'.
 - Adjust the settings you wish to adjust.
 - Select 'Back'
 
To navigate a world you have loaded / created:
 - Use 'W', 'A', 'S', 'D' to move the camera in the horizontal plane.
 - Move the mouse to change the viewing angle of the camera.
 - Use 'Space' and 'Left Shift' to increase / decrease the camera height.
 - Use 'Left Control' to increase movement speed.
 
To interact with the world you have loaded / created:
 - Use 'Left Mouse' to destroy blocks.
 - Use 'Right Mouse' to place blocks.
 - Use 'Scroll' to change selected building block.

Additional interactions when you have loaded / created a world:
 - Use 'F11' to toggle the debug overlay.
 - use 'Tilde' to open the command console.
   - Typing '/help' or '/?' will give you a list of available commands.
