package Core.Utils;

import Core.Control.Settings;
import Core.Entities.Camera;
import Core.Entities.Entity;
import Core.Terrain.CollisionMesh.CollisionMesh;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Terrain.TerrainManager;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.util.ArrayList;

public class MousePicker {

    private Vector3f currentRay = new Vector3f();

    private Matrix4f projectionMatrix;
    private Matrix4f viewMatrix;
    private Camera camera;

    private Block targetSolidBlock;
    private Vector3f targetSolidBlockEmptyNeighbourCoord;

    public MousePicker(Camera camera, Matrix4f projectionMatrix) {
        this.camera = camera;
        this.projectionMatrix = projectionMatrix;
        this.viewMatrix = Maths.createViewMatrix(camera);
    }

    public void update() {
        ArrayList<Chunk> chunks = TerrainManager.getLoadedChunks().getWithinRange(Maths.worldCoordToChunkCoord(camera.getPosition().x), Maths.worldCoordToChunkCoord(camera.getPosition().z), 1);
        viewMatrix = Maths.createViewMatrix(camera);
        currentRay = calculateMouseRay();

        //calculate the nearest intersection point for solid and liquid entities
        Vector3f nearestSolidIntercept = null;
        for (Chunk chunk : chunks) {
            Entity solidEntity = chunk.getEntity();
            Entity liquidEntity = chunk.getLiquidEntity();
            if (solidEntity == null || liquidEntity == null) {
                break;
            }
            CollisionMesh solidMesh = solidEntity.getCollisionMesh();
            Vector3f solidIntercept = solidMesh.getInterceptWithRay(currentRay, camera.getPosition(), new Vector3f(chunk.getChunkX() * Settings.getChunkSize(), 0, chunk.getChunkZ() * Settings.getChunkSize()));
            if (solidIntercept != null && (nearestSolidIntercept == null || Maths.distance(solidIntercept, camera.getPosition(), 2) < Maths.distance(nearestSolidIntercept, camera.getPosition(), 2))) {
                nearestSolidIntercept = solidIntercept;
            }
        }
        if (nearestSolidIntercept != null && Maths.distance(nearestSolidIntercept, camera.getPosition(), 3) > 5) {
            nearestSolidIntercept = null;
        }

        // get target solid and liquid blocks from intersection coords
        targetSolidBlock = null;
        targetSolidBlockEmptyNeighbourCoord = null;
        if (nearestSolidIntercept != null) {
            float xDecimal = Math.abs(nearestSolidIntercept.x % 1) - 0.5f;
            float yDecimal = Math.abs(nearestSolidIntercept.y % 1) - 0.5f;
            float zDecimal = Math.abs(nearestSolidIntercept.z % 1) - 0.5f;
            Direction face;
            if (Math.abs(xDecimal) > Math.abs(yDecimal) && Math.abs(xDecimal) > Math.abs(zDecimal)) {
                face =  xDecimal < 0 ? Direction.LEFT : Direction.RIGHT;
            } else if (Math.abs(yDecimal) > Math.abs(xDecimal) && Math.abs(yDecimal) > Math.abs(zDecimal)) {
                face = yDecimal < 0 ? Direction.DOWN : Direction.UP;
            } else {
                face = zDecimal < 0 ? Direction.BACK : Direction.FRONT;
            }

            switch (face) {
                case UP:
                    targetSolidBlockEmptyNeighbourCoord = new Vector3f(nearestSolidIntercept.x, (int) nearestSolidIntercept.y + 1, nearestSolidIntercept.z);
                    break;
                case DOWN:
                    targetSolidBlockEmptyNeighbourCoord = new Vector3f(nearestSolidIntercept.x, (int) nearestSolidIntercept.y - 1 , nearestSolidIntercept.z);
                    break;
                case RIGHT:
                    targetSolidBlockEmptyNeighbourCoord = new Vector3f((int) nearestSolidIntercept.x + 1, nearestSolidIntercept.y, nearestSolidIntercept.z);
                    break;
                case LEFT:
                    targetSolidBlockEmptyNeighbourCoord = new Vector3f((int) nearestSolidIntercept.x - 1, nearestSolidIntercept.y, nearestSolidIntercept.z);
                    break;
                case FRONT:
                    targetSolidBlockEmptyNeighbourCoord = new Vector3f(nearestSolidIntercept.x, nearestSolidIntercept.y, (int) nearestSolidIntercept.z + 1);
                    break;
                case BACK:
                    targetSolidBlockEmptyNeighbourCoord = new Vector3f(nearestSolidIntercept.x, nearestSolidIntercept.y, (int) nearestSolidIntercept.z - 1);
                    break;
            }
            Block block = TerrainManager.getBlock(nearestSolidIntercept.x, nearestSolidIntercept.y, nearestSolidIntercept.z);
            if (block != null && !block.getType().isLiquid()) {
                targetSolidBlock = block;
            } else {

                block = TerrainManager.getBlock(targetSolidBlockEmptyNeighbourCoord.x, targetSolidBlockEmptyNeighbourCoord.y, targetSolidBlockEmptyNeighbourCoord.z);
                if (block != null && !block.getType().isLiquid()) {
                    targetSolidBlock = block;
                    targetSolidBlockEmptyNeighbourCoord = nearestSolidIntercept;
                } else {
                    targetSolidBlock = null;
                    targetSolidBlockEmptyNeighbourCoord = null;
                }
            }
        }

        //TODO: calculate target liquid block when needed
    }

    private Vector3f calculateMouseRay() {
        float mouseX = Mouse.getX();
        float mouseY = Mouse.getY();
        Vector2f normalizedCoords = getNormalisedDeviceCoordinates(mouseX, mouseY);
        Vector4f clipCoords = new Vector4f(normalizedCoords.x, normalizedCoords.y, -1.0f, 1.0f);
        Vector4f eyeCoords = toEyeCoords(clipCoords);
        return toWorldCoords(eyeCoords);
    }

    private Vector3f toWorldCoords(Vector4f eyeCoords) {
        Matrix4f invertedView = Matrix4f.invert(viewMatrix, null);
        Vector4f rayWorld = Matrix4f.transform(invertedView, eyeCoords, null);
        Vector3f mouseRay = new Vector3f(rayWorld.x, rayWorld.y, rayWorld.z);
        mouseRay.normalise();
        return mouseRay;
    }

    private Vector4f toEyeCoords(Vector4f clipCoords) {
        Matrix4f invertedProjection = Matrix4f.invert(projectionMatrix, null);
        Vector4f eyeCoords = Matrix4f.transform(invertedProjection, clipCoords, null);
        return new Vector4f(eyeCoords.x, eyeCoords.y, -1f, 0f);
    }

    private Vector2f getNormalisedDeviceCoordinates(float mouseX, float mouseY) {
        float x = (2.0f * mouseX) / Display.getWidth() - 1f;
        float y = (2.0f * mouseY) / Display.getHeight() - 1f;
        return new Vector2f(x, y);
    }

    //**********************************************************

    public Block getTargetSolidBlock() {
        return targetSolidBlock;
    }

    public Vector3f getTargetSolidBlockEmptyNeighbourCoord() {
        return targetSolidBlockEmptyNeighbourCoord;
    }

}
