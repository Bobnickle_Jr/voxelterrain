package Core.Utils;

import Core.Control.Settings;
import Core.Entities.Camera;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.Date;

public final class Maths {

    private Maths() {}

    /**
     * calculate distance between two points
     * p = 1 -> Manhattan
     * p = 2 -> Euclidian
     * p = 3 -> Minkovski
     */
    public static float distance(float x1, float y1, float x2, float y2, double p) {
        return (float) Math.pow(Math.pow(Math.abs(x1 - x2), p) + Math.pow(Math.abs(y1 - y2), p), (1 / p));
    }

    /**
     * calculate distance between two 3D points
     * p = 1 -> Manhattan
     * p = 2 -> Euclidian
     * p = 3 -> Minkovski
     */
    public static float distance(Vector3f coord1, Vector3f coord2, double p) {
        return (float) Math.pow(Math.pow(Math.abs(coord1.x - coord2.x), p)
                + Math.pow(Math.abs(coord1.y - coord2.y), p)
                + Math.pow(Math.abs(coord1.z - coord2.z), p), (1 / p));
    }

    /**
     * Return the coordinates of the chunk containing a given world coordinate
     */
    public static int worldCoordToChunkCoord(float worldCoord) {
        if (worldCoord >= 0) {
            return (int) worldCoord / Settings.getChunkSize();
        } else {
            return (int) ((worldCoord + 0.005) / Settings.getChunkSize()) - 1;
        }
    }

    /**
     * Calculate a long seed from an input string
     */
    public static long calculateLongSeed(String seed) {
        long lSeed = 0;
        if (StringUtils.isNullOrEmptyOrWhiteSpace(seed)) {
            lSeed = new Date().getTime();
        } else {
            for (int i = 0; i < seed.length(); i++) {
                char ch = seed.charAt(i);
                lSeed = lSeed + (long) ch;
            }
        }
        return lSeed;
    }

    /**
     * Convert a world coord value into a chunk coord value
     */
    public static int worldCoordToCoordInChunk(float worldCoord) {
        for (; worldCoord < 0; worldCoord += Settings.getChunkSize());
        for (; worldCoord >= Settings.getChunkSize(); worldCoord -= Settings.getChunkSize());
        return (int) worldCoord;
    }

    /**
     * Create a transformation matrix from translation and scale
     */
    public static Matrix4f createTransformationMatrix(Vector2f translation, Vector2f scale) {
        Matrix4f matrix = new Matrix4f();
        matrix.setIdentity();
        Matrix4f.translate(translation, matrix, matrix);
        Matrix4f.scale(new Vector3f(scale.x, scale.y, 1f), matrix, matrix);
        return matrix;
    }

    /**
     * Create a transformation matrix from translation, rotation, and scale
     */
    public static Matrix4f createTransformationMatrix(Vector3f translation, float rx, float ry, float rz, float scale) {
        Matrix4f matrix = new Matrix4f();
        matrix.setIdentity();
        Matrix4f.translate(translation, matrix, matrix);
        Matrix4f.rotate((float) Math.toRadians(rx), new Vector3f(1,0,0), matrix, matrix);
        Matrix4f.rotate((float) Math.toRadians(ry), new Vector3f(0,1,0), matrix, matrix);
        Matrix4f.rotate((float) Math.toRadians(rz), new Vector3f(0,0,1), matrix, matrix);
        Matrix4f.scale(new Vector3f(scale,scale,scale), matrix, matrix);
        return matrix;
    }

    /**
     * Create a view matrix using the camera location and orientation
     */
    public static Matrix4f createViewMatrix(Camera camera) {
        Matrix4f viewMatrix = new Matrix4f();
        viewMatrix.setIdentity();
        Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), viewMatrix,
                viewMatrix);
        Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);
        Vector3f cameraPos = camera.getPosition();
        Vector3f negativeCameraPos = new Vector3f(-cameraPos.x,-cameraPos.y,-cameraPos.z);
        Matrix4f.translate(negativeCameraPos, viewMatrix, viewMatrix);
        return viewMatrix;
    }

    /**
     * Check if a ray intersects a triangle in 3d space
     */
    public static Vector3f rayIntersectsTriangle(Vector3f ray, Vector3f position, Vector3f vertex1, Vector3f vertex2, Vector3f vertex3) {
        // Compute vectors along two edges of the triangle.
        Vector3f edge1 = Vector3f.sub(vertex2, vertex1, null);
        Vector3f edge2 = Vector3f.sub(vertex3, vertex1, null);

        // Compute the determinant.
        Vector3f directionCrossEdge2 = Vector3f.cross(ray, edge2, null);

        float determinant = Vector3f.dot(directionCrossEdge2, edge1);
        // If the ray and triangle are parallel, there is no collision.
        if (determinant > -.0000001f && determinant < .0000001f) {
            return null;
        }
        float inverseDeterminant = 1.0f / determinant;

        // Calculate the U parameter of the intersection point.
        Vector3f distanceVector = Vector3f.sub(position, vertex1, null);
        float triangleU = Vector3f.dot(directionCrossEdge2, distanceVector);
        triangleU *= inverseDeterminant;

        // Make sure the U is inside the triangle.
        if (triangleU < 0 || triangleU > 1) {
            return null;
        }

        // Calculate the V parameter of the intersection point.
        Vector3f distanceCrossEdge1 = Vector3f.cross(distanceVector, edge1, null);
        float triangleV = Vector3f.dot(ray, distanceCrossEdge1);
        triangleV *= inverseDeterminant;

        // Make sure the V is inside the triangle.
        if (triangleV < 0 || triangleU + triangleV > 1) {
            return null;
        }

        // Get the distance to the face from our ray origin
        float rayDistance = Vector3f.dot(distanceCrossEdge1, edge2);
        rayDistance *= inverseDeterminant;

        // Is the triangle behind us?
        if (rayDistance < 0) {
            return null;
        }

        ray.normalise();
        return Vector3f.add(position, new Vector3f(ray.x * rayDistance, ray.y * rayDistance, ray.z * rayDistance), null);
    }
}
