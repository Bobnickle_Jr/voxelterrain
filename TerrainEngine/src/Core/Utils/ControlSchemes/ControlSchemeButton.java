package Core.Utils.ControlSchemes;

public enum ControlSchemeButton {

    CSB_INTERACT,
    CSB_USE_ITEM_1,
    CSB_USE_ITEM_2,

    CSB_WALK,
    CSB_WALK_BACK,
    CSB_STRAFE_LEFT,
    CSB_STRAFE_RIGHT,

    CSB_JUMP,
    CSB_CROUCH,
    CSB_SPRINT,

    CSB_ROTATE_FURNITURE,

    CSB_PAUSE,
    CSB_CONSOLE,
    CSB_CONSOLE_SUBMIT,
    CSB_OPEN_CHAT,

    CSB_MENU_SELECT,
    CSB_TOGGLE_DEBUG,
    ;

}
