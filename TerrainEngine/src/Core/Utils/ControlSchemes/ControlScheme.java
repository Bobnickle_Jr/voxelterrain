package Core.Utils.ControlSchemes;

import Core.Utils.Logging.Log;

import java.util.ArrayList;

public abstract class ControlScheme {

    ArrayList<ControlSchemeButton> downButtonsLastFrame = new ArrayList<>();
    ArrayList<ControlSchemeButton> downButtons = new ArrayList<>();
    ArrayList<ControlSchemeButton> pressedButtons = new ArrayList<>();
    ArrayList<ControlSchemeButton> releasedButtons = new ArrayList<>();

    public void updateButtons() {
        // down last frame
        downButtonsLastFrame = downButtons;
        downButtons = new ArrayList<>();
        pressedButtons = new ArrayList<>();
        releasedButtons = new ArrayList<>();
        for (ControlSchemeButton button : ControlSchemeButton.values()) {
            // down
            if (controlSchemeSpecificButtonCheck(button)) {
                downButtons.add(button);
            }
            // pressed
            if (buttonDown(button) && !buttonDownLastFrame(button)) {
                pressedButtons.add(button);
            }
            // released
            if (!buttonDown(button) && buttonDownLastFrame(button)) {
                releasedButtons.add(button);
            }
        }
    }

    public boolean buttonReleased(ControlSchemeButton button) {
        return releasedButtons.contains(button);
    }

    public boolean buttonPressed(ControlSchemeButton button) {
        return pressedButtons.contains(button);
    }

    public boolean buttonDown(ControlSchemeButton button) {
        return downButtons.contains(button);
    }

    public boolean buttonDownLastFrame(ControlSchemeButton button) {
        return downButtonsLastFrame.contains(button);
    }

    protected abstract boolean controlSchemeSpecificButtonCheck(ControlSchemeButton button);
}
