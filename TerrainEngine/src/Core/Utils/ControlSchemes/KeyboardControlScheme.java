package Core.Utils.ControlSchemes;

import Core.Utils.Logging.Log;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class KeyboardControlScheme extends ControlScheme {

    @Override
    protected boolean controlSchemeSpecificButtonCheck(ControlSchemeButton button) {
        switch (button) {
            case CSB_WALK:
                return Keyboard.isKeyDown(Keyboard.KEY_W);
            case CSB_WALK_BACK:
                return Keyboard.isKeyDown(Keyboard.KEY_S);
            case CSB_STRAFE_LEFT:
                return Keyboard.isKeyDown(Keyboard.KEY_A);
            case CSB_STRAFE_RIGHT:
                return Keyboard.isKeyDown(Keyboard.KEY_D);
            case CSB_INTERACT:
                return Keyboard.isKeyDown(Keyboard.KEY_E);
            case CSB_USE_ITEM_1:
                return Mouse.isButtonDown(0);
            case CSB_USE_ITEM_2:
                return Mouse.isButtonDown(1);
            case CSB_JUMP:
                return Keyboard.isKeyDown(Keyboard.KEY_SPACE);
            case CSB_CROUCH:
                return Keyboard.isKeyDown(Keyboard.KEY_LCONTROL);
            case CSB_SPRINT:
                return Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
            case CSB_ROTATE_FURNITURE:
                return Keyboard.isKeyDown(Keyboard.KEY_R);
            case CSB_PAUSE:
                return Keyboard.isKeyDown(Keyboard.KEY_ESCAPE);
            case CSB_CONSOLE:
                return Keyboard.isKeyDown(Keyboard.KEY_GRAVE);
            case CSB_CONSOLE_SUBMIT:
                return Keyboard.isKeyDown(Keyboard.KEY_RETURN);
            case CSB_OPEN_CHAT:
                return Keyboard.isKeyDown(Keyboard.KEY_T);
            case CSB_MENU_SELECT:
                return Mouse.isButtonDown(0);
            case CSB_TOGGLE_DEBUG:
                return Keyboard.isKeyDown(Keyboard.KEY_F11);
        }
        Log.warning("No Key Binding In Keyboard Control Scheme: " + button);
        return false;
    }

}
