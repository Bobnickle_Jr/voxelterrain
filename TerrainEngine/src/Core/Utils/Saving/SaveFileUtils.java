package Core.Utils.Saving;

import Core.Control.Settings;
import Core.Entities.Player;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Utils.Logging.Log;
import Core.Utils.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public final class SaveFileUtils {

    /**
     * Create a save file with a given name
     */
    public static String createSaveFile(String fileName) {
        fileName = StringUtils.removeSpaces(fileName);
        fileName = StringUtils.makeAlphaNumeric(fileName);
        return createSaveFile(fileName, 0);
    }

    /**
     * Create a save file with a given name and version
     * If the given name and version already exist, increase the version number
     */
    private static String createSaveFile(String fileName, int version) {
        String fullPath = Settings.getSaveFileLocation() + "/" + fileName + version;
        File theDir = new File(fullPath);
        if (!theDir.exists()){
            theDir.mkdirs();
            // make terrain folder
            createSaveFileSubfolder(fileName + version, "terrain");
            // make player data folder
            createSaveFileSubfolder(fileName + version, "player");

            return fileName + version;
        } else {
            return createSaveFile(fileName, ++version);
        }
    }

    /**
     * Create save file subfolders if they don't already exist
     */
    private static void createSaveFileSubfolder(String saveFileName, String subfolderName) {
        File terrainFile = new File(Settings.getSaveFileLocation() + "/" + saveFileName + "/" + subfolderName);
        if (!terrainFile.exists()) {
            if (!terrainFile.mkdirs()) {
                Log.error("Failed To Create New Save FIle Sub-Folder: " + saveFileName + "/" + saveFileName);
            }
        }
    }

    /**
     * Return a list of all save files found in the save file directory ordered from most recent
     */
    public static ArrayList<WorldMetaData> listAllSaveFiles() {
        ArrayList<WorldMetaData> saveFiles = new ArrayList<>();
        File f = new File(Settings.getSaveFileLocation());
        for (String pathname : Objects.requireNonNull(f.list())) {
            WorldMetaData metaFile = getMetaData(pathname);
            if (metaFile != null) {
                int position = 0;
                for (WorldMetaData listedFile : saveFiles) {
                    if (listedFile.getLastSaved().getTime() < metaFile.getLastSaved().getTime()) {
                        break;
                    }
                    position += 1;
                }
                saveFiles.add(position, metaFile);
            }


        }
        return saveFiles;
    }

    /**
     * Create the meta data file for a given meta data and save it to the related world
     */
    public static void createMetaDataFile(WorldMetaData metaData) {
        try {
            String metaFileName = Settings.getSaveFileLocation() + "/" + metaData.getSaveFileName() + "/meta.sav";
            File metaDataFile = new File(metaFileName);
            metaDataFile.delete();
            if (metaDataFile.createNewFile()) {
                Log.info("Creating Meta Data File.");
                FileWriter writer = new FileWriter(metaFileName);
                writer.write(metaData.getSaveString());
                writer.close();

            } else {
                Log.warning("Tried to Create Meta Data File, File Already Exists.");
            }
        } catch (IOException e) {
            Log.error("Failed to Create Meta Data File.");
            e.printStackTrace();
        }
    }

    /**
     * Load the meta data from a given save file
     */
    public static WorldMetaData getMetaData(String fileName) {
        try {
            String metaFileName = Settings.getSaveFileLocation() + "/" + fileName + "/meta.sav";
            File myObj = new File(metaFileName);
            Scanner myReader = new Scanner(myObj);
            WorldMetaData metaData = new WorldMetaData();
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] splitData = data.split(":");
                if (splitData.length == 2) {
                    metaData.loadAttribute(splitData[0], splitData[1]);
                } else {
                    metaData.loadAttribute(splitData[0], "");
                }
            }
            myReader.close();
            return metaData;
        } catch (Exception e) {
            Log.error("Failed to Read Meta Data File.");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Save a player to the file for the current world
     */
    public static void createPlayerFile(WorldMetaData metaData, Player player) {
        try {
            createSaveFileSubfolder(metaData.getSaveFileName(), "player");
            String playerFileName = Settings.getSaveFileLocation() + "/" + metaData.getSaveFileName() + "/player/" + player.getName() + ".sav";
            File playerFile = new File(playerFileName);
            playerFile.delete();
            if (playerFile.createNewFile()) {
                FileWriter writer = new FileWriter(playerFileName);
                writer.write(player.getSaveString());
                writer.close();
            } else {
                Log.warning("Tried to Create Player Save File, File Already Exists.");
            }
        } catch (IOException e) {
            Log.error("Failed to Create Player Save File.");
            e.printStackTrace();
        }
    }

    /**
     * Load player data from file and return it to caller
     */
    public static String getPlayerData(WorldMetaData metaData, String playerName) {
        try {
            String playerFileName = Settings.getSaveFileLocation() + "/" + metaData.getSaveFileName() + "/player/" + playerName + ".sav";
            File myObj = new File(playerFileName);
            Scanner myReader = new Scanner(myObj);
            String playerData = myReader.nextLine();
            myReader.close();
            return playerData;
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    /**
     * Save a chunk to a file for the related world
     */
    public static void createChunkFile(WorldMetaData metaData, Chunk chunk) {
        try {
            createSaveFileSubfolder(metaData.getSaveFileName(), "terrain");
            String chunkFileName = Settings.getSaveFileLocation() + "/" + metaData.getSaveFileName() + "/terrain/" + chunk.getChunkX() + "_" + chunk.getChunkZ() + ".sav";
            File chunkFile = new File(chunkFileName);
            chunkFile.delete();
            if (chunkFile.createNewFile()) {
                FileWriter writer = new FileWriter(chunkFileName);
                writer.write(chunk.getSaveString());
                writer.close();
            } else {
                Log.warning("Tried to Create Chunk Save File, File Already Exists.");
            }
        } catch (IOException e) {
            Log.error("Failed to Create Chunk Save File.");
            e.printStackTrace();
        }
    }

    /**
     * Load chunk data from file and return it to caller
     */
    public static ArrayList<String> getChunkData(WorldMetaData metaData, int chunkX, int chunkZ) {
        try {
            String chunkFileName = Settings.getSaveFileLocation() + "/" + metaData.getSaveFileName() + "/terrain/" + chunkX + "_" + chunkZ + ".sav";
            File myObj = new File(chunkFileName);
            Scanner myReader = new Scanner(myObj);
            ArrayList<String> chunkData = new ArrayList<>();
            while (myReader.hasNextLine()) {
                chunkData.add(myReader.nextLine());
            }
            myReader.close();
            return chunkData;
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    /**
     * Save a string to a file in a specified location
     */
    public static boolean saveStringToFile(String fileName, String contents) {
        try {
            File file = new File(fileName);
            file.delete();
            if (file.createNewFile()) {
                FileWriter writer = new FileWriter(fileName);
                writer.write(contents);
                writer.close();
                return true;
            } else {
                Log.warning("Tried to Save String to File, File Already Exists.");
                return false;
            }
        } catch (IOException e) {
            Log.error("Failed to  Save String to File.");
            e.printStackTrace();
            return false;
        }
    }
}
