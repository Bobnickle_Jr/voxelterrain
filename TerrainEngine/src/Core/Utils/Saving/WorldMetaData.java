package Core.Utils.Saving;

import Core.Terrain.Data.WorldType;
import Core.Utils.Logging.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WorldMetaData {

    private String saveFileName;
    private Long seed;
    private String sSeed;
    private WorldType worldType;
    private Date lastSaved;

    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

    public WorldMetaData() {}

    public WorldMetaData(String saveFileName, Long seed, String sSeed, WorldType worldType) {
        this.saveFileName = saveFileName;
        this.seed = seed;
        this.sSeed = sSeed;
        this.worldType = worldType;
    }

    public void loadAttribute(String attName, String attValue) throws ParseException {
        switch (attName) {
            case "fileName":
                saveFileName = attValue;
                break;
            case "seed":
                seed = Long.parseLong(attValue);
                break;
            case "sSeed":
                sSeed = attValue;
                break;
            case "worldType":
                worldType = WorldType.getByName(attValue);
                break;
            case "lastSaved":
                lastSaved = formatter.parse(attValue);
                break;
            default:
                Log.error("Unknown Meta Data Attribute Name: " + attName);
        }
    }

    public String getSaveString() {
        StringBuilder sb = new StringBuilder();
        sb.append("fileName:").append(saveFileName).append("\n");
        sb.append("seed:").append(seed).append("\n");
        sb.append("sSeed:").append(sSeed).append("\n");
        sb.append("worldType:").append(worldType).append("\n");
        sb.append("lastSaved:").append(formatter.format(new Date())).append("\n");
        return sb.toString();
    }

    // getters and setters

    public String getSaveFileName() {
        return saveFileName;
    }

    public Long getSeed() {
        return seed;
    }

    public Date getLastSaved() {
        return lastSaved;
    }

    public WorldType getWorldType() {
        return worldType;
    }

    public String getsSeed() {
        return sSeed;
    }

}
