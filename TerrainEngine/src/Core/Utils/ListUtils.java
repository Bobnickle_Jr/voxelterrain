package Core.Utils;

import java.util.ArrayList;

public class ListUtils {

    public static int[] toPrimitiveIntArray(ArrayList<Integer> list) {
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i += 1) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static float[] toPrimitiveFloatArray(ArrayList<Float> list) {
        float[] array = new float[list.size()];
        for (int i = 0; i < list.size(); i += 1) {
            array[i] = list.get(i);
        }
        return array;
    }
}
