package Core.Utils;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public final class StringUtils {

    private StringUtils() {}

    public static boolean isNullOrEmptyOrWhiteSpace(String s) {
        return s == null || s.length() == 0 || isWhitespace(s);
    }

    public static boolean containsCharacter(String s, char c) {
        for (char sc : s.toCharArray()) {
            if (sc == c) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return coords as a pretty string
     */
    public static String coordString(float x, float y) {
        return "(" + x + "," + y + ")";
    }

    /**
     * Return coords as a pretty string
     */
    public static String coordString(float x, float y, float z) {
        return "(" + x + "," + y + "," + z + ")";
    }

    /**
     * Return coords as a pretty string
     */
    public static String coordString(Vector2f coord) {
        return coordString(coord.x, coord.y);
    }

    /**
     * Return coords as a pretty string
     */
    public static String coordString(Vector3f coord) {
        return coordString(coord.x, coord.y, coord.z);
    }

    /**
     * Remove spaces from a string
     */
    public static String removeSpaces(String string) {
        return string.replaceAll("\\s+","");
    }

    /**
     * Remove non-alpha-numeric characters from a string
     */
    public static String makeAlphaNumeric(String string) {
        return string.replaceAll("[^A-Za-z0-9]", "");
    }

    /**
     * Check if a string is null or empty
     */
    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    /**
     * Check if a string is null or whitespace
     */
    public static boolean isNullOrWhitespace(String s) {
        return s == null || isWhitespace(s);
    }

    /**
     * Check if a string is entirely whitespace
     */
    private static boolean isWhitespace(String s) {
        int length = s.length();
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                if (!Character.isWhitespace(s.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
