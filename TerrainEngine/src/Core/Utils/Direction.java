package Core.Utils;

import org.lwjgl.util.vector.Vector3f;

public enum Direction {

    UP(0, 1, 0),
    DOWN(0, -1, 0),
    RIGHT(1, 0, 0),
    LEFT(-1, 0, 0),
    FRONT(0, 0, 1),
    BACK(0, 0, -1),
    ;

    private int dirX;
    private int dirY;
    private int dirZ;

    Direction(int dirX, int dirY, int dirZ) {
        this.dirX = dirX;
        this.dirY = dirY;
        this.dirZ = dirZ;
    }

    public Direction getRotationInY(int y) {
        Direction rotatedSelf = this;
        for (int i = 0; i < y; i += 1) {
            rotatedSelf = rotatedSelf.getRotationInY();
        }
        return rotatedSelf;
    }

    private Direction getRotationInY() {
        switch (this) {
            case FRONT:
                return LEFT;
            case BACK:
                return RIGHT;
            case RIGHT:
                return FRONT;
            case LEFT:
                return BACK;
        }
        return this;
    }

    public Direction getOpposite() {
        switch (this) {
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            case DOWN:
                return UP;
            case UP:
                return DOWN;
            case BACK:
                return FRONT;
            case FRONT:
                return BACK;
        }
        return null;
    }
}
