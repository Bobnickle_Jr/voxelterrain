package Core.Utils.Logging;

import java.util.Date;

public class LogItem {

    private Date timeStamp;
    private LogType type;
    private String logInfo;

    public LogItem(LogType type, String logInfo) {
        this.timeStamp = new Date();
        this.type = type;
        this.logInfo = logInfo;
    }

    public void print() {
        if (type.isUseErrorPrintStream()) {
            System.err.println(timeStamp + " : " + type.getString() + " : " + logInfo);
        } else {
            System.out.println(timeStamp + " : " + type.getString() + " : " + logInfo);
        }
    }
}
