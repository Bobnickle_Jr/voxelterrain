package Core.Utils.Logging;

import java.util.ArrayList;

public final class Log {

    private static ArrayList<LogItem> logs = new ArrayList<>();

    private Log() {}

    public static void console(String logInfo) {
        createLog(LogType.LT_CONSOLE, logInfo);
    }

    public static void info(String logInfo) {
        createLog(LogType.LT_INFO, logInfo);
    }

    public static void debug(String logInfo) {
        createLog(LogType.LT_DEBUG, logInfo);
    }

    public static void warning(String logInfo) {
        createLog(LogType.LT_WARNING, logInfo);
    }

    public static void error(String logInfo) {
        createLog(LogType.LT_ERROR, logInfo);
    }

    private static void createLog(LogType type, String logInfo) {
        LogItem logItem = new LogItem(type, logInfo);
        logItem.print();
        logs.add(logItem);
    }
}
