package Core.Utils.Logging;

public enum LogType {

    LT_CONSOLE("Console", false),
    LT_INFO("Info", false),
    LT_DEBUG("Debug", true),
    LT_ERROR("Error", true),
    LT_WARNING("Warning", true),
    ;

    private String string;
    private boolean useErrorPrintStream;

    LogType(String string, boolean useErrorPrintStream) {
        this.string = string;
        this.useErrorPrintStream = useErrorPrintStream;
    }

    // getters and setters

    public String getString() {
        return string;
    }

    public boolean isUseErrorPrintStream() {
        return useErrorPrintStream;
    }
}
