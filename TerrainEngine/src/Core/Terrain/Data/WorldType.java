package Core.Terrain.Data;

import Core.Utils.Logging.Log;

public enum WorldType {

    WT_STANDARD("Standard", "Procedurally Generated Terrain"),
    WT_FLAT("Flat", "Flat Terrain - Intended solely for testing"),
    ;

    private String name;
    private String description;

    WorldType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public static WorldType getByName(String name) {
        for (WorldType worldType : values()) {
            if (name.equals(worldType.name)) {
                return worldType;
            }
        }
        Log.error("Failed to load world type from name: " + name);
        return null;
    }

    // getters and setters

    @Override
    public String toString() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
