package Core.Terrain.Data.Chunks;

import Client.Rendering.Models.ModelData;
import Core.Terrain.CollisionMesh.CollisionMesh;

public class ChunkEntityData {

    private ModelData modelData;
    private CollisionMesh collisionMesh;

    public ChunkEntityData(ModelData modelData, CollisionMesh collisionMesh) {
        this.modelData = modelData;
        this.collisionMesh = collisionMesh;
    }

    public ModelData getModelData() {
        return modelData;
    }

    public CollisionMesh getCollisionMesh() {
        return collisionMesh;
    }
}
