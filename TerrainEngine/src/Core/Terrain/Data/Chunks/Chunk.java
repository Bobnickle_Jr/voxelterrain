package Core.Terrain.Data.Chunks;

import Client.Rendering.Loader;
import Client.Rendering.Models.ModelData;
import Client.Rendering.Models.RawModel;
import Client.Rendering.Models.TexturedModel;
import Client.Rendering.Textures.ModelTexture;
import Core.Control.Settings;
import Core.Entities.Entity;
import Core.Terrain.CollisionMesh.CollisionMesh;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Blocks.BlockType;
import Core.Terrain.TerrainManager;
import Core.Utils.Logging.Log;
import Core.Utils.Saving.SaveFileUtils;
import Core.Utils.StringUtils;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class Chunk {

    private Loader loader;

    private static ModelTexture blocksModelTexture;
//    private static ModelTexture liquidModelTexture;

    private int chunkX;
    private int chunkZ;
    private Block[][][] blocks;
    private Entity entity;
    private ChunkEntityData solidEntityData;
    private Entity liquidEntity;
    private ChunkEntityData liquidEntityData;

    private boolean needsNewEntity;
    private boolean needsNewEntityData;
    private boolean needsUnloading;

    public Chunk(int chunkX, int chunkZ, Loader loader) {
        this.chunkX = chunkX;
        this.chunkZ = chunkZ;
        this.loader = loader;

        // attempt to load from file, if file doesn't exist, generate new terrain
        Log.info("Loading chunk: " + StringUtils.coordString(chunkX, chunkZ) + ".");
        if (!loadFromFile()) {
            Log.info("No Valid File Found, Generating New Chunk Instead.");
            generateNewTerrain();
        }
    }

    public void addBlock(int x, int y, int z, BlockType blockType) {
        if (x >= 0 && x < Settings.getChunkSize() && y >= 0 && y < Settings.getChunkHeight() && z >= 0 && z < Settings.getChunkSize()) {
            blocks[x][y][z] = new Block(blockType, this, 0, new Vector3f(x, y, z));
            needsNewEntityData = true;
        }
    }

    public void removeBlock(int x, int y, int z) {
        if (x >= 0 && x < Settings.getChunkSize() && y >= 0 && y < Settings.getChunkHeight() && z >= 0 && z < Settings.getChunkSize()) {
            blocks[x][y][z] = null;
            needsNewEntityData = true;
        }
    }

    public Block getBlock(int x, int y, int z) {
        if (x >= 0 && x < Settings.getChunkSize() && y >= 0 && y < Settings.getChunkHeight() && z >= 0 && z < Settings.getChunkSize()) {
            return blocks[x][y][z];
        }
        return null;
    }

    /**
     * Use block data to generate the entity for this chunk
     */
    public void recalculateEntity() {
        if (blocksModelTexture == null) {
            blocksModelTexture = new ModelTexture(loader.loadTexture(Settings.getTextureFileLocation() + "/blocks/solid").getTextureID());
        }
        if (solidEntityData == null || liquidEntityData == null) {
            return;
        }
        unloadEntity(entity);
        entity = generateEntity(false);

        unloadEntity(liquidEntity);
        liquidEntity = generateEntity(true);

        needsNewEntity = false;
    }

    public void recalculateEntityData() {
        solidEntityData = generateEntityData(false);
        liquidEntityData = generateEntityData(true);
        needsNewEntityData = false;
        needsNewEntity = true;
    }

    private ChunkEntityData generateEntityData(boolean isLiquid) {
        ArrayList<Block> renderableBlocks = updateChunkHiddenFaces();
        ModelData modelData = generateModelData(isLiquid, renderableBlocks);
        CollisionMesh collisionMesh = generateCollisionMesh(isLiquid, renderableBlocks);
        return new ChunkEntityData(modelData, collisionMesh);
    }

    private Entity generateEntity(boolean isLiquid) {
        ChunkEntityData entityData = isLiquid ? liquidEntityData : solidEntityData;

        ModelData modelData = entityData.getModelData();
        RawModel model = loader.loadToVAO(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());
        TexturedModel staticModel;
        if (isLiquid) {
            staticModel = new TexturedModel(model, null);
        } else {
            staticModel = new TexturedModel(model, blocksModelTexture);
        }

        if (isLiquid) {
            liquidEntityData = null;
        } else {
            solidEntityData = null;
        }

        return new Entity(staticModel, entityData.getCollisionMesh(), new Vector3f(chunkX * Settings.getChunkSize(), 0, chunkZ * Settings.getChunkSize()), 0, 0, 0, 0.5f);
    }

    private CollisionMesh generateCollisionMesh(boolean isLiquid, ArrayList<Block> renderableBlocks) {
        ArrayList<CollisionMesh> collisionMeshesToMerge = new ArrayList<>();
        ArrayList<Vector3f> collisionMeshPositionsToMerge = new ArrayList<>();
        for (Block block : renderableBlocks) {
            if (isLiquid == block.getType().isLiquid()) {
                collisionMeshesToMerge.add(block.getCollisionMesh());
                collisionMeshPositionsToMerge.add(block.getInChunkPosition());
            }
        }
        return CollisionMesh.mergeMeshes(collisionMeshesToMerge, collisionMeshPositionsToMerge);
    }

    private ModelData generateModelData(boolean isLiquid, ArrayList<Block> renderableBlocks) {
        ArrayList<ModelData> modelDataToMerge = new ArrayList<>();
        ArrayList<Vector3f> positionsToMerge = new ArrayList<>();
        ArrayList<Vector2f> spriteSheetPositionsToMerge = new ArrayList<>();
        for (Block block : renderableBlocks) {
            if (isLiquid == block.getType().isLiquid()) {
                List<ModelData> modelDatas = block.getModelDatas();
                for (ModelData modelData : modelDatas) {
                    modelDataToMerge.add(modelData);
                    positionsToMerge.add(block.getInChunkPosition());
                }
                List<Vector2f> spriteSheetPositions = block.getSpriteSheetPositions();
                spriteSheetPositionsToMerge.addAll(spriteSheetPositions);
            }
        }
        return ModelData.merge(modelDataToMerge, positionsToMerge, spriteSheetPositionsToMerge);
    }

    private ArrayList<Block> updateChunkHiddenFaces() {
        ArrayList<Block> renderableBlocks = new ArrayList<>();
        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int y = 0; y < Settings.getChunkHeight(); y += 1) {
                for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                    Block block = blocks[x][y][z];
                    if (block != null) {
                        updateBlockHiddenFaces(block);
                        if (block.shouldRender()) {
                            renderableBlocks.add(block);
                        }
                    }
                }
            }
        }
        return renderableBlocks;
    }

    private void updateBlockHiddenFaces(Block block) {
        TerrainManager.updateBlockHiddenFaces(block);
    }
    
    /**
     * Attempt to save this chunk to a file
     */
    public void saveToFile() {
        SaveFileUtils.createChunkFile(TerrainManager.getCurrentWorld().getMetaData(), this);
    }

    /**
     * Attempt to load chunk data from file, return true if successful
     */
    private boolean loadFromFile() {
        ArrayList<String> chunkData = SaveFileUtils.getChunkData(TerrainManager.getCurrentWorld().getMetaData(), chunkX, chunkZ);
        if (chunkData == null) {
            return false;
        }
        blocks = new Block[Settings.getChunkSize()][Settings.getChunkHeight()][Settings.getChunkSize()];
        // load from file
        try {
            for (String blockData : chunkData) {
                String[] blockDataSplit = blockData.split(":");
                int x = Integer.parseInt(blockDataSplit[0]);
                int y = Integer.parseInt(blockDataSplit[1]);
                int z = Integer.parseInt(blockDataSplit[2]);
                int blockTypeOrd = Integer.parseInt(blockDataSplit[3]);
                blocks[x][y][z] = new Block(BlockType.getByOrd(blockTypeOrd), this, 0, new Vector3f(x, y, z));
            }
            return true;
        } catch (Exception e) {
            Log.error("Failed to parse data file for chunk: " + StringUtils.coordString(chunkX, chunkZ) + ".");
            return false;
        }
    }

    public void unloadEntities() {
        unloadEntity(entity);
        unloadEntity(liquidEntity);
        entity = null;
        liquidEntity = null;
    }

    private void unloadEntity(Entity entity) {
        if (entity != null) {
            loader.unloadVBOs(entity.getModel().getRawModel().getVBOs());
            loader.unloadVAO(entity.getModel().getRawModel().getVaoID());
        }
    }

    /**
     * Request chunk generation data from terrain builder
     * Generate chunk terrain based on the data
     */
    private void generateNewTerrain() {
        blocks = TerrainManager.getTerrainBuilder().generateChunkTerrain(this);
    }

    /**
     * Generate and return string used in save file
     */
    public String getSaveString() {
        StringBuilder sb = new StringBuilder();
        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int y = 0; y < Settings.getChunkHeight(); y += 1) {
                for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                    if (blocks[x][y][z] != null) {
                        int blockTypeOrd = blocks[x][y][z].getType().ordinal();
                        sb.append(x).append(":").append(y).append(":").append(z).append(":");
                        sb.append(blockTypeOrd).append("\n");
                    }
                }
            }
        }
        return sb.toString();
    }

    // getters and setters

    public int getChunkX() {
        return chunkX;
    }

    public int getChunkZ() {
        return chunkZ;
    }

    public Entity getEntity() {
        return entity;
    }

    public Entity getLiquidEntity() {
        return liquidEntity;
    }

    public Block[][][] getBlocks() {
        return blocks;
    }

    public boolean isNeedsNewEntity() {
        return needsNewEntity;
    }

    public void setNeedsNewEntityData(boolean needsNewEntityData) {
        this.needsNewEntityData = needsNewEntityData;
    }

    public boolean isNeedsNewEntityData() {
        return needsNewEntityData;
    }

    public boolean isNeedsUnloading() {
        return needsUnloading;
    }

    public void setNeedsUnloading(boolean needsUnloading) {
        this.needsUnloading = needsUnloading;
    }
}
