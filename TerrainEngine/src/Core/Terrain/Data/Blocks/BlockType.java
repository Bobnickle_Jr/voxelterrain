package Core.Terrain.Data.Blocks;

import Client.Rendering.Models.ModelData;
import Core.Terrain.CollisionMesh.CollisionMesh;
import Core.Utils.Logging.Log;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;

import static Core.Terrain.Data.Blocks.BlockModel.*;
import static Core.Terrain.Data.Blocks.BlockTexture.*;

public enum BlockType {

    /** DO NOT REORDER - ORDINALS USED IN TEMPLATE FILES **/

    BLT_DIRT("Dirt", "Dirt", 110, false, BLM_FULL_BLOCK, BTX_DIRT), // 0
    BLT_GRASS("Grass", "Grass", 100, false, BLM_FULL_BLOCK, BTX_GRASS, BTX_DIRT, BTX_GRASS, BTX_GRASS, BTX_GRASS, BTX_GRASS), // 1
    BLT_STONE("Stone", "Stone", 100, false, BLM_FULL_BLOCK, BTX_STONE), // 2
    BLT_WOOD_LOG( "Wood Log", "WoodLog",120, false, BLM_FULL_BLOCK, BTX_WOOD_LOG, BTX_WOOD_LOG, BTX_WOOD_LOG_SIDE, BTX_WOOD_LOG_SIDE, BTX_WOOD_LOG_SIDE, BTX_WOOD_LOG_SIDE), // 3
    BLT_LEAVES_1( "Leaves", "Leaves",50, false, BLM_FULL_BLOCK, BTX_LEAVES_1), // 4
    BLT_SAND( "Sand", "Sand", 100, false, BLM_FULL_BLOCK, BTX_SAND), // 5
    BLT_SNOW( "Snow", "Snow", 100, false, BLM_FULL_BLOCK, BTX_SNOW), // 6

    BLT_WATER("Water", "Water", 125, true, BLM_LIQUID_BLOCK, BTX_WATER), // 7
    BLT_WOOD_PLANKS("Wood Planks", "WoodPlanks", 150, false, BLM_FULL_BLOCK, BTX_WOOD_PLANKS), // 8
    BLT_STONE_BRICK("Stone Brick", "StoneBrick", 150, false, BLM_FULL_BLOCK, BTX_STONE_BRICK), // 9

    BLT_LONG_GRASS("Long Grass", "LongGrass", 0, false, BLM_FOLIAGE, BTX_LONG_GRASS), // 10

    BLT_CACTUS("Cactus", "Cactus", 50, false, BLM_FULL_BLOCK, BTX_CACTUS_TOP, BTX_CACTUS_SIDE, BTX_CACTUS_SIDE, BTX_CACTUS_SIDE, BTX_CACTUS_SIDE, BTX_CACTUS_SIDE), // 11

    BLT_DEAD_GRASS("Dead Grass", "DeadGrass", 0, false, BLM_FOLIAGE, BTX_DEAD_GRASS), // 12
    BLT_LEAVES_2("Leaves", "Leaves", 50, false, BLM_FULL_BLOCK, BTX_LEAVES_2), // 13
    BLT_LEAVES_3("Leaves", "Leaves", 50, false, BLM_FULL_BLOCK, BTX_LEAVES_3), // 14

//    BLT_AIR("Air", "Air", 1000, false, null, null), // 15 <--- move to 0 in rearrange

    ;

    private String name;
    private String assetName;
    private int priority;
    private boolean liquid;
    private BlockModel blockModel;
    private BlockTexture[] blockTexture;

    BlockType(String name, String assetName, int priority, boolean liquid, BlockModel blockModel, BlockTexture... blockTexture) {
        this.name = name;
        this.assetName = assetName;
        this.priority = priority;
        this.liquid = liquid;
        this.blockModel = blockModel;
        this.blockTexture = blockTexture;
        if (blockTexture.length == 0) {
            this.blockTexture = new BlockTexture[] { BTX_UNDEFINED };
        }
    }

    public static BlockType getByOrd(int ord) {
        for (BlockType blockType : values()) {
            if (blockType.ordinal() == ord) {
                return blockType;
            }
        }
        Log.warning("Failed to find block type with ord: " + ord + ".");
        return null;
    }


    public ArrayList<ModelData> getModelDatas(int hiddenFaces) {
        return blockModel.getModelData(hiddenFaces);
    }

    public CollisionMesh getCollisionMesh(int hiddenFaces) {
        return blockModel.getCollisionMesh(hiddenFaces);
    }

    public ArrayList<Vector2f> getSpriteSheetPositions(int hiddenFaces) {
        ArrayList<Vector2f> spriteSheetPositions = new ArrayList<>();
        if (blockModel.isHideFacesIfBlocked()) {
            if ((hiddenFaces & 1) == 0) { // up
                spriteSheetPositions.add(blockTexture[0].getTextureLocation());
            }
            if ((hiddenFaces & 2) == 0) { // down
                spriteSheetPositions.add(blockTexture[1 % blockTexture.length].getTextureLocation());
            }
            if ((hiddenFaces & 4) == 0) { // left
                spriteSheetPositions.add(blockTexture[2 % blockTexture.length].getTextureLocation());
            }
            if ((hiddenFaces & 8) == 0) { // right
                spriteSheetPositions.add(blockTexture[3 % blockTexture.length].getTextureLocation());
            }
            if ((hiddenFaces & 16) == 0) { // back
                spriteSheetPositions.add(blockTexture[4 % blockTexture.length].getTextureLocation());
            }
            if ((hiddenFaces & 32) == 0) { // front
                spriteSheetPositions.add(blockTexture[5 % blockTexture.length].getTextureLocation());
            }
        } else {
            if (hiddenFaces != 63) {
                for (int i = 0; i < blockModel.getModelData().length; i += 1) {
                    spriteSheetPositions.add(blockTexture[i % blockTexture.length].getTextureLocation());
                }
            }
        }
        return spriteSheetPositions;
    }

    // getters and setters

    public String getName() {
        return name;
    }

    public String getAssetName() {
        return assetName;
    }

    public BlockModel getBlockModel() {
        return blockModel;
    }

    public boolean isLiquid() {
        return liquid;
    }

    public int getPriority() {
        return priority;
    }
}
