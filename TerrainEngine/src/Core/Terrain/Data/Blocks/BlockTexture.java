package Core.Terrain.Data.Blocks;

import org.lwjgl.util.vector.Vector2f;

public enum BlockTexture {

    BTX_UNDEFINED(3, 3),

    // solid
    BTX_GRASS(0, 0),
    BTX_DIRT(0, 2),

    BTX_WOOD_LOG(1, 0),
    BTX_WOOD_LOG_SIDE(1, 1),
    BTX_WOOD_PLANKS(1, 2),
    BTX_LEAVES_1(0, 3),
    BTX_LEAVES_2(1, 3),
    BTX_LEAVES_3(2, 3),

    BTX_STONE(2, 0),
    BTX_STONE_BRICK(2, 1),
    BTX_SAND(3, 0),
    BTX_SNOW(3, 1),

    BTX_CACTUS_TOP(0, 4),
    BTX_CACTUS_SIDE(0, 5),

    BTX_DEAD_GRASS(1, 4),
    BTX_LONG_GRASS(1, 5),

    // liquid
    BTX_WATER(0, 0),

    ;

    private final Vector2f textureLocation;

    BlockTexture(int x, int y) {
        this.textureLocation = new Vector2f(x, y);
    }

    // getters and setters

    public Vector2f getTextureLocation() {
        return textureLocation;
    }
}
