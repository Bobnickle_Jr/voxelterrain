package Core.Terrain.Data.Blocks;

import Client.Rendering.Models.ModelData;
import Core.Terrain.CollisionMesh.CollisionMesh;
import Core.Terrain.CollisionMesh.CollisionMeshPredef;
import Core.Utils.OBJLoading.OBJFileLoader;

import java.util.ArrayList;
import java.util.Arrays;

import static Core.Terrain.CollisionMesh.CollisionMeshPredef.*;

public enum BlockModel {

    BLM_FULL_BLOCK(true, true, true,"blocks/fullBlock/",
            new String[] {"blockUp", "blockDown", "blockLeft", "blockRight", "blockBack", "blockFront"},
            new CollisionMeshPredef[]{ CMP_FULL_BLOCK_UP, CMP_FULL_BLOCK_DOWN, CMP_FULL_BLOCK_LEFT, CMP_FULL_BLOCK_RIGHT, CMP_FULL_BLOCK_BACK, CMP_FULL_BLOCK_FRONT }),
    BLM_LIQUID_BLOCK(true, false, true,"blocks/fullBlock/",
            new String[] {"blockUp", "blockDown", "blockLeft", "blockRight", "blockBack", "blockFront"},
            new CollisionMeshPredef[]{ CMP_FULL_BLOCK_UP, CMP_FULL_BLOCK_DOWN, CMP_FULL_BLOCK_LEFT, CMP_FULL_BLOCK_RIGHT, CMP_FULL_BLOCK_BACK, CMP_FULL_BLOCK_FRONT }),
    BLM_FOLIAGE(false, false, false,"blocks/foliage/",
            new String[] {"foliage1", "foliage2", "foliage3", "foliage4"},
            new CollisionMeshPredef[]{ CMP_FULL_BLOCK_UP, CMP_FULL_BLOCK_DOWN, CMP_FULL_BLOCK_LEFT, CMP_FULL_BLOCK_RIGHT, CMP_FULL_BLOCK_BACK, CMP_FULL_BLOCK_FRONT }),
    ;

    private boolean hideFacesIfBlocked;
    private boolean blockNeighbouringFaces;
    private boolean blockNeighbouringSameTypeFaces;
    private ModelData[] modelData;
    private CollisionMeshPredef[] collisionMeshPredefs;

    BlockModel(boolean hideFacesIfBlocked, boolean blockNeighbouringFaces, boolean blockNeighbouringSameTypeFaces, String modelFolder, String[] modelDataPaths, CollisionMeshPredef[] meshPredefs) {
        this.hideFacesIfBlocked = hideFacesIfBlocked;
        this.blockNeighbouringFaces = blockNeighbouringFaces;
        this.blockNeighbouringSameTypeFaces = blockNeighbouringSameTypeFaces;
        this.collisionMeshPredefs = meshPredefs;

        this.modelData = new ModelData[modelDataPaths.length];
        for (int i = 0; i < modelDataPaths.length; i += 1) {
            modelData[i] = OBJFileLoader.loadOBJ(modelFolder + modelDataPaths[i]);
        }
    }

    public CollisionMesh getCollisionMesh(int hiddenFaces) {
        CollisionMesh collisionMesh = new CollisionMesh();

        //TODO: only use needed faces
        for (CollisionMeshPredef predef : collisionMeshPredefs) {
            collisionMesh.addNodes(predef.getNodes());
        }

        return collisionMesh;
    }

    public ArrayList<ModelData> getModelData(int hiddenFaces) {
        ArrayList<ModelData> modelDatas = new ArrayList<>();
        if (hideFacesIfBlocked) {
            if ((hiddenFaces & 1) == 0) { // up
                modelDatas.add(modelData[0]);
            }
            if ((hiddenFaces & 2) == 0) { // down
                modelDatas.add(modelData[1]);
            }
            if ((hiddenFaces & 4) == 0) { // left
                modelDatas.add(modelData[2]);
            }
            if ((hiddenFaces & 8) == 0) { // right
                modelDatas.add(modelData[3]);
            }
            if ((hiddenFaces & 16) == 0) { // back
                modelDatas.add(modelData[4]);
            }
            if ((hiddenFaces & 32) == 0) { // front
                modelDatas.add(modelData[5]);
            }
        } else {
            if (hiddenFaces != 63) {
                modelDatas.addAll(Arrays.asList(modelData));
            }
        }
        return modelDatas;
    }

    // getters and setters


    public boolean isHideFacesIfBlocked() {
        return hideFacesIfBlocked;
    }

    public boolean isBlockNeighbouringFaces() {
        return blockNeighbouringFaces;
    }

    public boolean isBlockNeighbouringSameTypeFaces() {
        return blockNeighbouringSameTypeFaces;
    }

    public ModelData[] getModelData() {
        return modelData;
    }
}