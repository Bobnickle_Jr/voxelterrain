package Core.Terrain.Data.Blocks;

import Client.Rendering.Models.ModelData;
import Core.Control.Settings;
import Core.Terrain.CollisionMesh.CollisionMesh;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Utils.Logging.Log;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;

public class Block {

    private BlockType type;
    private Chunk chunk;
    private byte hiddenFaces;
    private Vector3f inChunkPosition;

    public Block(BlockType type, Chunk chunk, int hiddenFaces, Vector3f inChunkPosition) {
        this.type = type;
        if (type == null) {
            Log.warning("Null Typed Block Created.");
        }
        this.chunk = chunk;
        this.hiddenFaces = (byte) hiddenFaces;
        this.inChunkPosition = inChunkPosition;
    }

    public ArrayList<ModelData> getModelDatas() {
        return type.getModelDatas(hiddenFaces);
    }

    public CollisionMesh getCollisionMesh() {
        return type.getCollisionMesh(hiddenFaces);
    }

    public ArrayList<Vector2f> getSpriteSheetPositions() {
        return type.getSpriteSheetPositions(hiddenFaces);
    }

    public boolean shouldRender() {
        return this.hiddenFaces != 63;
    }

    public Vector3f getPosition() {
        int chunkX = chunk.getChunkX() * Settings.getChunkSize();
        int chunkZ = chunk.getChunkZ() * Settings.getChunkSize();
        return new Vector3f(chunkX + inChunkPosition.x, inChunkPosition.y, chunkZ + inChunkPosition.z);
    }

    /**
     * Return coords of chunk
     */
    public Vector2f getChunkCoords() {
        return new Vector2f(chunk.getChunkX(), chunk.getChunkZ());
    }

    // getters and setters

    public BlockType getType() {
        return type;
    }

    public void setHiddenFaces(int hiddenFaces) {
        this.hiddenFaces = (byte) hiddenFaces;
    }

    public Vector3f getInChunkPosition() {
        return inChunkPosition;
    }
}
