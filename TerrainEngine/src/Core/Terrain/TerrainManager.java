package Core.Terrain;

import Client.Rendering.Loader;
import Core.Control.GameWorld;
import Core.Control.Settings;
import Core.Entities.Entity;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Blocks.BlockType;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Terrain.Data.WorldType;
import Core.Terrain.Generation.BiomeType;
import Core.Terrain.Generation.TerrainBuilding.MasterTerrainBuilder;
import Core.Terrain.Generation.TerrainBuilding.TerrainBuilder;
import Core.Terrain.Generation.TerrainBuilding.OverworldTerrainBuilder;
import Core.Utils.Logging.Log;
import Core.Utils.Maths;
import Core.Utils.StringUtils;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.Date;

public final class TerrainManager {


    private static float averageGenerationTime;
    private static int totalGenerationsCompleted;

    private static boolean initialised;
    private static GameWorld currentWorld;
    private static TerrainBuilder terrainBuilder;
    private static Loader loader;
    private static ChunkCollection loadedChunks;

    private TerrainManager() {}

    /**
     * Initialise the terrain manager
     */
    public static void init(GameWorld currentWorld, Loader loader, WorldType worldType) {
        TerrainManager.currentWorld = currentWorld;
        TerrainManager.terrainBuilder = new MasterTerrainBuilder(currentWorld.getMetaData().getSeed(), BiomeType.getAllFindable(), worldType);
        TerrainManager.loader = loader;
        TerrainManager.loadedChunks = new ChunkCollection();
        TerrainManager.initialised = true;
    }

    /**
     *  Update the hidden faces value of a given block based on the blocks around it
     */
    public static void updateBlockHiddenFaces(Block block) {
        // calculate block visible faces
        int coveredSides = 0;
        // up
        coveredSides += (blockCoversNeighbours((int) block.getPosition().x , (int) block.getPosition().y + 1, (int) block.getPosition().z, block.getType()) ? 1 : 0);
        // down
        coveredSides += (blockCoversNeighbours((int) block.getPosition().x, (int) block.getPosition().y - 1,  (int) block.getPosition().z, block.getType()) ? 2 : 0);
        // left
        coveredSides += (blockCoversNeighbours((int) block.getPosition().x - 1, (int) block.getPosition().y, (int) block.getPosition().z, block.getType()) ? 4 : 0);
        // right
        coveredSides += (blockCoversNeighbours((int) block.getPosition().x + 1,  (int) block.getPosition().y, (int) block.getPosition().z, block.getType()) ? 8 : 0);
        // forward
        coveredSides += (blockCoversNeighbours((int) block.getPosition().x, (int) block.getPosition().y, (int) block.getPosition().z - 1, block.getType()) ? 16 : 0);
        // back
        coveredSides += (blockCoversNeighbours((int) block.getPosition().x, (int) block.getPosition().y, (int) block.getPosition().z + 1, block.getType()) ? 32 : 0);

        block.setHiddenFaces(coveredSides);
    }

    /**
     * Check if block in given world position covers its neighbours
     */
    public static boolean blockCoversNeighbours(int x, int y, int z, BlockType neighbourType) {
        int chunkX = Maths.worldCoordToChunkCoord(x);
        int chunkZ = Maths.worldCoordToChunkCoord(z);
        int xInChunk = x - (chunkX * Settings.getChunkSize());
        int zInChunk = z - (chunkZ * Settings.getChunkSize());
        Chunk chunk = loadedChunks.get(chunkX, chunkZ);
        if (chunk == null || y < 0) {
            return true;
        }
        Block block = chunk.getBlock(xInChunk, y, zInChunk);
        if (block == null) {
            return false;
        }
        if (neighbourType == block.getType()) {
            return block.getType().getBlockModel().isBlockNeighbouringSameTypeFaces();
        }
        return block.getType().getBlockModel().isBlockNeighbouringFaces();

    }

    /**
     * Load chunks within render distance of load center
     * Unload chunks outside render distance of load center
     */
    public static void updateLoadedChunks(float loadCenterX, float loadCenterZ, boolean unload) {
        int renderDistance = Settings.getRenderDistance();
        int loadCenterChunkX = Maths.worldCoordToChunkCoord(loadCenterX);
        int loadCenterChunkZ = Maths.worldCoordToChunkCoord(loadCenterZ);

        // load chunks within render distance
        Vector2f chunkToLoad = null;
        for (int x = -renderDistance; x <= renderDistance; x += 1) {
            for (int z = -renderDistance; z <= renderDistance; z += 1) {
                if (Maths.distance(x, z, 0, 0, 2) <= renderDistance) {
                    if (!checkChunkLoaded(loadCenterChunkX + x, loadCenterChunkZ + z) &&
                            (chunkToLoad == null || Maths.distance(x, z, 0, 0, 2) < Maths.distance(chunkToLoad.x, chunkToLoad.y, loadCenterChunkX, loadCenterChunkZ, 2))) {
                        chunkToLoad = new Vector2f(loadCenterChunkX + x, loadCenterChunkZ + z);
                    }
                }
            }
        }
        if (chunkToLoad != null) {
            long genStartTime = new Date().getTime();
            loadChunk((int) chunkToLoad.x, (int) chunkToLoad.y);

            long genEndTime = new Date().getTime();
            float genTime = genEndTime - genStartTime;
            averageGenerationTime = ((averageGenerationTime * totalGenerationsCompleted) + genTime) / (totalGenerationsCompleted + 1);
            totalGenerationsCompleted += 1;
        }

        // update chunk entity datas
        for (Chunk chunk : loadedChunks.getAll()) {
            if (chunk.isNeedsNewEntityData()) {
                chunk.recalculateEntityData();
            }
        }

        // unload chunks outside of render distance
        if (unload) {
            for (Chunk chunk : loadedChunks.getAll()) {
                if (Maths.distance(chunk.getChunkX(), chunk.getChunkZ(), loadCenterChunkX, loadCenterChunkZ, 2) > renderDistance) {
                    chunk.saveToFile();
                    chunk.setNeedsUnloading(true);
                }
            }
        }


    }

    /**
     * Return a list of all solid entities managed by the terrain manager
     */
    public static ArrayList<Entity> getAllSolidEntities() {
        ArrayList<Entity> entities = new ArrayList<>();
        for (Chunk chunk : loadedChunks.getAll()) {
            if (chunk.isNeedsUnloading()) {
                unloadChunk(chunk, false);
                continue;
            }
            if (chunk.isNeedsNewEntity()) {
                chunk.recalculateEntity();
            }
            if (chunk.getEntity() != null) {
                entities.add(chunk.getEntity());
            }
        }
        return entities;
    }

    /**
     * Return a list of all liquid entities managed by the terrain manager
     */
    public static ArrayList<Entity> getAllLiquidEntities() {
        ArrayList<Entity> entities = new ArrayList<>();
        for (Chunk chunk : loadedChunks.getAll()) {
            if (chunk.isNeedsUnloading()) {
                unloadChunk(chunk, false);
                continue;
            }
            if (chunk.isNeedsNewEntity()) {
                chunk.recalculateEntity();
            }
            if (chunk.getLiquidEntity() != null) {
                entities.add(chunk.getLiquidEntity());
            }
        }
        return entities;
    }

    /**
     * Unload a list of chunks
     */
    private static void unloadChunks(ArrayList<Chunk> chunks) {
        for (Chunk chunk : chunks) {
            unloadChunk(chunk, true);
        }
    }

    /**
     * Unload a given chunk
     * Save chunk to file and remove is from loaded chunks list
     */
    private static void unloadChunk(Chunk chunk, boolean save) {
        Log.info("Unloading chunk: " + StringUtils.coordString(chunk.getChunkX(), chunk.getChunkZ()) + ".");
        if (save) {
            chunk.saveToFile();
        }
        chunk.unloadEntities();
        loadedChunks.remove(chunk);
    }

    /**
     * Unload all loaded chunks
     */
    public static void unloadAllChunks() {
        ArrayList<Chunk> chunks = new ArrayList<>(loadedChunks.getAll());
        unloadChunks(chunks);
    }

    /**
     * Attempt to load chunk from file, if not found, generate chunk
     */
    private static void loadChunk(int x, int z) {
        if (!checkChunkLoaded(x, z)) {
            Chunk chunk = new Chunk(x, z, loader);
            loadedChunks.put(chunk);
            chunk.recalculateEntityData();
            setChunkNeedsNewEntityData(x + 1, z);
            setChunkNeedsNewEntityData(x - 1, z);
            setChunkNeedsNewEntityData(x, z + 1);
            setChunkNeedsNewEntityData(x, z - 1);
        }
    }

    private static void setChunkNeedsNewEntityData(int chunkX, int chunkZ) {
        Chunk chunk = loadedChunks.get(chunkX, chunkZ);
        if (chunk != null) {
            chunk.setNeedsNewEntityData(true);
        }
    }

    /**
     * Check if a given chunk is already loaded
     */
    private static boolean checkChunkLoaded(int x, int z) {
        return loadedChunks.get(x, z) != null;
    }

    /**
     * Create a new block at given world position with given block type
     */
    public static void addBlock(Vector3f worldPosition, BlockType blockType) {
        int chunkX = Maths.worldCoordToChunkCoord(worldPosition.x);
        int chunkZ = Maths.worldCoordToChunkCoord(worldPosition.z);
        int xInChunk = Maths.worldCoordToCoordInChunk(worldPosition.x);
        int zInChunk = Maths.worldCoordToCoordInChunk(worldPosition.z);
        Chunk chunk = loadedChunks.get(chunkX, chunkZ);
        if (chunk != null) {
            chunk.addBlock(xInChunk, (int) worldPosition.y, zInChunk, blockType);
        }
    }

    /**
     * Remove the block at a given world position
     */
    public static void removeBlock(Vector3f worldPosition) {
        int chunkX = Maths.worldCoordToChunkCoord(worldPosition.x);
        int chunkZ = Maths.worldCoordToChunkCoord(worldPosition.z);
        int xInChunk = Maths.worldCoordToCoordInChunk(worldPosition.x);
        int zInChunk = Maths.worldCoordToCoordInChunk(worldPosition.z);
        Chunk chunk = loadedChunks.get(chunkX, chunkZ);
        if (chunk != null) {
            chunk.removeBlock(xInChunk, (int) worldPosition.y, zInChunk);
        }
        ArrayList<Chunk> neighboursToUpdate = new ArrayList<>();
        if (xInChunk == 0) {
            neighboursToUpdate.add(loadedChunks.get(chunkX - 1, chunkZ));
        }
        if (zInChunk == 0) {
            neighboursToUpdate.add(loadedChunks.get(chunkX, chunkZ - 1));
        }
        if (xInChunk == Settings.getChunkSize() - 1) {
            neighboursToUpdate.add(loadedChunks.get(chunkX + 1, chunkZ));
        }
        if (zInChunk == Settings.getChunkSize() - 1) {
            neighboursToUpdate.add(loadedChunks.get(chunkX, chunkZ + 1));
        }
        for (Chunk neighbour : neighboursToUpdate) {
            if (neighbour != null) {
                neighbour.setNeedsNewEntityData(true);
            }
        }
    }

    /**
     * Get block at given world position
     */
    public static Block getBlock(float worldX, float worldY, float worldZ) {
        int chunkX = Maths.worldCoordToChunkCoord(worldX);
        int chunkZ = Maths.worldCoordToChunkCoord(worldZ);
        int xInChunk = Maths.worldCoordToCoordInChunk(worldX);
        int zInChunk = Maths.worldCoordToCoordInChunk(worldZ);

        Chunk chunk = loadedChunks.get(chunkX, chunkZ);
        if (chunk != null) {
            return chunk.getBlock(xInChunk, (int) worldY, zInChunk);
        }
        return null;
    }

    // getters and setters

    public static ChunkCollection getLoadedChunks() {
        return loadedChunks;
    }

    public static GameWorld getCurrentWorld() {
        return currentWorld;
    }

    public static TerrainBuilder getTerrainBuilder() {
        return terrainBuilder;
    }

    public static boolean isInitialised() {
        return initialised;
    }

    public static void setInitialised(boolean initialised) {
        TerrainManager.initialised = initialised;
    }

    public static float getAverageGenerationTime() {
        return averageGenerationTime;
    }

    public static void setAverageGenerationTime(float averageGenerationTime) {
        TerrainManager.averageGenerationTime = averageGenerationTime;
    }
}
