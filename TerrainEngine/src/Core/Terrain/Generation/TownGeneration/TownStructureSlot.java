package Core.Terrain.Generation.TownGeneration;

import Core.Utils.Direction;
import org.lwjgl.util.vector.Vector2f;

public class TownStructureSlot {

    private TownStructureTileType slotType;
    private Direction slotDirection;
    private Vector2f slotPosition;
    private boolean input;

    public TownStructureSlot(TownStructureTileType slotType, Direction slotDirection, Vector2f slotPosition, boolean input) {
        this.slotType = slotType;
        this.slotDirection = slotDirection;
        this.slotPosition = slotPosition;
        this.input = input;
    }

    public Vector2f getSlotPosition() {
        return slotPosition;
    }

    public TownStructureTileType getSlotType() {
        return slotType;
    }

    public Direction getSlotDirection() {
        return slotDirection;
    }

    public boolean isInput() {
        return input;
    }
}
