package Core.Terrain.Generation.TownGeneration;

import Core.Terrain.Generation.Structures.StructureSuperTypeWithRotation;
import org.lwjgl.util.vector.Vector2f;

import java.util.HashMap;

public interface ITownBuilder {

    HashMap<Vector2f, StructureSuperTypeWithRotation> generateTown(Long seed, Vector2f townCenter);
}
