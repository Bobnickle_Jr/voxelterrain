package Core.Terrain.Generation.TownGeneration;

import Core.Terrain.Data.Blocks.BlockType;
import Core.Terrain.Generation.Structures.StructureSuperType;
import Core.Terrain.Generation.Structures.StructureTemplate;
import Core.Utils.Direction;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;

public class TownStructureTile {

    private TownStructureTileType type;

    private ArrayList<TownStructureSlot> inputSlots;
    private ArrayList<TownStructureSlot> outputSlots;

    private StructureSuperType structureSuperType;
    private Vector2f position = new Vector2f();
    private int rotation;
    private ArrayList<Vector2f> occupiedBlocks;

    public TownStructureTile(TownStructureTileType type, StructureSuperType structureSuperType, ArrayList<TownStructureSlot> inputSlots, ArrayList<TownStructureSlot> outputSlots, int rotation) {
        this.type = type;
        this.outputSlots = rotateSlots(outputSlots, rotation);
        this.inputSlots = rotateSlots(inputSlots, rotation);
        this.structureSuperType = structureSuperType;
        this.rotation = rotation;
    }

    public boolean collidesWith(TownStructureTile otherTile) {
        ArrayList<Vector2f> occupiedBlocks = getOccupiedBlocks();
        ArrayList<Vector2f> otherTileOccupiedBlocks = otherTile.getOccupiedBlocks();
        for (Vector2f occupiedBlock : occupiedBlocks) {
            for (Vector2f otherTileOccupiedBlock : otherTileOccupiedBlocks) {
                if (occupiedBlock.equals(otherTileOccupiedBlock)) {
                    return true;
                }
            }
        }
        return false;
    }

    private ArrayList<Vector2f> calculateOccupiedBlocks() {
        ArrayList<Vector2f> occupiedBlocks = new ArrayList<>();
        StructureTemplate template = structureSuperType.getFirstChild().getTemplate(0L, rotation);
        BlockType[][][] blocks = template.getBlockTypes();
        for (int x = 0; x < blocks.length; x += 1) {
            for (int y = 0; y < blocks[0].length; y += 1) {
                for (int z = 0; z < blocks[0][0].length; z += 1) {
                    BlockType blockType = blocks[x][y][z];
                    if (blockType != null) {
                        occupiedBlocks.add(new Vector2f(x + position.x - template.getxOffset(), z + position.y - template.getzOffset()));
                    }
                }
            }
        }
        return occupiedBlocks;
    }

    private ArrayList<TownStructureSlot> rotateSlots(ArrayList<TownStructureSlot> defaultSlots, int rotation) {
        ArrayList<TownStructureSlot> rotatedSlots = new ArrayList<>();
        for (TownStructureSlot defaultSlot : defaultSlots) {
            rotatedSlots.add(rotateSlot(defaultSlot, rotation));
        }
        return rotatedSlots;
    }

    private TownStructureSlot rotateSlot(TownStructureSlot defaultSlot, int rotation) {

        // update coord
        Vector2f newCoord = defaultSlot.getSlotPosition();
        for (int i = 0; i < rotation; i += 1) {
            newCoord = new Vector2f(-newCoord.y, newCoord.x);
        }

        // update direction
        Direction newDirection = defaultSlot.getSlotDirection().getRotationInY(rotation);

        return new TownStructureSlot(defaultSlot.getSlotType(), newDirection, newCoord, defaultSlot.isInput());
    }

    // getters and setters

    public ArrayList<Vector2f> getOccupiedBlocks() {
        if (occupiedBlocks == null) {
            occupiedBlocks = calculateOccupiedBlocks();
        }

        return occupiedBlocks;
    }

    public Vector2f getPosition() {
        return position;
    }

    public void setPosition(Vector2f position) {
        this.position = position;
        this.occupiedBlocks = calculateOccupiedBlocks();
    }

    public TownStructureTileType getType() {
        return type;
    }

    public ArrayList<TownStructureSlot> getInputSlots() {
        return inputSlots;
    }

    public ArrayList<TownStructureSlot> getOutputSlots() {
        return outputSlots;
    }

    public StructureSuperType getStructureSuperType() {
        return structureSuperType;
    }

    public int getRotation() {
        return rotation;
    }

}
