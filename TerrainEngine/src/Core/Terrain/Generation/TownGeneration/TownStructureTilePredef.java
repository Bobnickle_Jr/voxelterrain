package Core.Terrain.Generation.TownGeneration;

import Core.Terrain.Generation.Structures.StructureSuperType;
import Core.Utils.Direction;
import org.lwjgl.util.vector.Vector2f;

import java.util.*;

import static Core.Terrain.Generation.Structures.StructureSuperType.*;
import static Core.Terrain.Generation.TownGeneration.TownStructureTileType.*;

public enum TownStructureTilePredef {

    TSTP_TOWN_CENTER(TSTT_TOWN_CENTER, SST_TOWN_CENTER,
            new TownStructureSlot(TSTT_PATH, Direction.LEFT, new Vector2f(-4, 0), true),
            new TownStructureSlot(TSTT_PATH, Direction.RIGHT, new Vector2f(4, 0), true),
            new TownStructureSlot(TSTT_PATH, Direction.FRONT, new Vector2f(0, 4), true),
            new TownStructureSlot(TSTT_PATH, Direction.BACK, new Vector2f(0, -4), true)
    ),
    TSTP_TOWN_PATH_0(TSTT_PATH, SST_TOWN_PATH_0,
            new TownStructureSlot(TSTT_BUILDING, Direction.RIGHT, new Vector2f(1, 0), true),
            new TownStructureSlot(TSTT_BUILDING, Direction.LEFT, new Vector2f(-1, 0), true),
            new TownStructureSlot(TSTT_PATH, Direction.FRONT, new Vector2f(0, 7), true),
            new TownStructureSlot(TSTT_PATH, Direction.BACK, new Vector2f(0, -8), false)
    ),
    TSTP_TOWN_PATH_1(TSTT_PATH, SST_TOWN_PATH_1,
            new TownStructureSlot(TSTT_BUILDING, Direction.FRONT, new Vector2f(0, 6), true),
            new TownStructureSlot(TSTT_PATH, Direction.RIGHT, new Vector2f(1, 5), true),
            new TownStructureSlot(TSTT_PATH, Direction.LEFT, new Vector2f(-1, 5), true),
            new TownStructureSlot(TSTT_PATH, Direction.BACK, new Vector2f(0, -8), false)
    ),
    TSTP_TOWN_PATH_2(TSTT_PATH, SST_TOWN_PATH_2,
            new TownStructureSlot(TSTT_PATH, Direction.RIGHT, new Vector2f(7, 5), true),
            new TownStructureSlot(TSTT_PATH, Direction.LEFT, new Vector2f(-7, 5), true),
            new TownStructureSlot(TSTT_PATH, Direction.BACK, new Vector2f(0, -8), false),
            new TownStructureSlot(TSTT_BUILDING, Direction.FRONT, new Vector2f(0, 6), true),
            new TownStructureSlot(TSTT_BUILDING, Direction.RIGHT, new Vector2f(1, -2), true),
            new TownStructureSlot(TSTT_BUILDING, Direction.LEFT, new Vector2f(-1, -2), true)
    ),
    TSTP_TOWN_HOUSE_0(TSTT_BUILDING, SST_TOWN_HOUSE_0,
            new TownStructureSlot(TSTT_BUILDING, Direction.RIGHT, new Vector2f(1, 0), false)
    ),
    TSTP_TOWN_HOUSE_1(TSTT_BUILDING, SST_TOWN_HOUSE_1,
            new TownStructureSlot(TSTT_BUILDING, Direction.RIGHT, new Vector2f(1, 0), false)
    ),
    TSTP_TOWN_HOUSE_2(TSTT_BUILDING, SST_TOWN_HOUSE_2,
            new TownStructureSlot(TSTT_BUILDING, Direction.RIGHT, new Vector2f(1, 0), false)
    ),
    TSTP_TOWN_HOUSE_3(TSTT_BUILDING, SST_TOWN_HOUSE_0,
            new TownStructureSlot(TSTT_BUILDING, Direction.RIGHT, new Vector2f(1, 0), false)
    ),
    TSTP_TOWN_HOUSE_4(TSTT_BUILDING, SST_TOWN_HOUSE_0,
            new TownStructureSlot(TSTT_BUILDING, Direction.RIGHT, new Vector2f(1, 0), false)
    ),
    TSTP_TOWN_BUSH_0(TSTT_BUILDING, SST_TOWN_BUSH_0,
            new TownStructureSlot(TSTT_BUILDING, Direction.LEFT, new Vector2f(-3, 0), false)
    ),
    TSTP_TOWN_TREE_0(TSTT_BUILDING, SST_TOWN_TREE_0,
            new TownStructureSlot(TSTT_BUILDING, Direction.LEFT, new Vector2f(-3, -1), false)
    ),
    ;

    private TownStructureTileType tileType;
    private StructureSuperType structureSuperType;
    private ArrayList<TownStructureSlot> inputSlots = new ArrayList<>();
    private ArrayList<TownStructureSlot> outputSlots = new ArrayList<>();

    TownStructureTilePredef(TownStructureTileType tileType, StructureSuperType structureSuperType, TownStructureSlot... slots) {
        this.tileType = tileType;
        this.structureSuperType = structureSuperType;

        for (TownStructureSlot slot : slots) {
            if (slot.isInput()) {
                inputSlots.add(slot);
            } else {
                outputSlots.add(slot);
            }
        }
    }

    public static List<TownStructureTilePredef> getShuffledValues(Random random) {
        List<TownStructureTilePredef> shuffled = Arrays.asList(values());
        Collections.shuffle(shuffled, random);
        return shuffled;
    }

    public TownStructureTile generateTile(int rotation) {
        return new TownStructureTile(tileType, structureSuperType, inputSlots, outputSlots, rotation);
    }

    // getters and setters

    public TownStructureTileType getTileType() {
        return tileType;
    }
}
