package Core.Terrain.Generation.TownGeneration;

import Core.Terrain.Generation.Structures.StructureSuperType;
import Core.Terrain.Generation.Structures.StructureSuperTypeWithRotation;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static Core.Terrain.Generation.TownGeneration.TownStructureTileType.TSTT_BUILDING;
import static Core.Terrain.Generation.TownGeneration.TownStructureTileType.TSTT_PATH;

public class JigsawTownBuilder implements ITownBuilder {

    private final static int MAX_PATHS = 15;

    HashMap<Long, HashMap<Vector2f, StructureSuperTypeWithRotation>> cache = new HashMap<>();

    /**
     * Attempt to load a town from the cache based on a town seed
     * If the town does not exist in the cache, generate a new one and save it to the cache
     */
    @Override
    public HashMap<Vector2f, StructureSuperTypeWithRotation> generateTown(Long seed, Vector2f townCenterCoord) {
        if (cache.get(seed) == null) {
            cache.put(seed, generateTownFromScratch(seed, townCenterCoord));
        }
        return cache.get(seed);
    }

    /**
     * Generate a new town based on a given seed
     */
    private HashMap<Vector2f, StructureSuperTypeWithRotation> generateTownFromScratch(Long seed, Vector2f townCenterCoord) {
        Random random = new Random(seed);
        ArrayList<TownStructureTile> tilesInTown = generateTownTiles(townCenterCoord, random);
        return generateMarkersFromTiles(tilesInTown);
    }

    /**
     * Generate structure markers based on a list of town tiles
     */
    private HashMap<Vector2f, StructureSuperTypeWithRotation> generateMarkersFromTiles(ArrayList<TownStructureTile> tilesInTown) {
        HashMap<Vector2f, StructureSuperTypeWithRotation> structureMarkers = new HashMap<>();
        for (TownStructureTile tile : tilesInTown) {
            Vector2f structPosition = tile.getPosition();
            StructureSuperType structSuperType = tile.getStructureSuperType();
            int rotation = tile.getRotation();
            structureMarkers.put(structPosition, new StructureSuperTypeWithRotation(structSuperType, rotation));
        }
        return structureMarkers;
    }

    /**
     * Generate a list of interconnected, non-overlapping town tiles
     */
    private ArrayList<TownStructureTile> generateTownTiles(Vector2f townCenterCoord, Random random) {
        TownStructureTile townCenterTile = TownStructureTilePredef.TSTP_TOWN_CENTER.generateTile(0);
        townCenterTile.setPosition(new Vector2f(townCenterCoord));
        ArrayList<TownStructureTile> tilesInTown = new ArrayList<>();
        ArrayList<TownStructureTile> tilesToEval = new ArrayList<>();
        tilesInTown.add(townCenterTile);
        tilesToEval.add(townCenterTile);

        int totalPaths = 0;
        while (tilesToEval.size() != 0) {
            TownStructureTile currentTile = tilesToEval.get(0);

            for (TownStructureSlot inputSlot : currentTile.getInputSlots()) {
                // find a tile type that can attach to this slot
                TownStructureTile candidate = null;
                TownStructureSlot candidateOutputSlot = null;
                for (TownStructureTilePredef predef : TownStructureTilePredef.getShuffledValues(random)) {
                    if (predef.getTileType() == (totalPaths < MAX_PATHS ? inputSlot.getSlotType() : TSTT_BUILDING)) {
                        for (int r = 0; r < 4; r += 1) {
                            candidate = predef.generateTile(r);
                            for (TownStructureSlot outputSlot : candidate.getOutputSlots()) {
                                if (outputSlot.getSlotDirection() == inputSlot.getSlotDirection().getOpposite()) {
                                    candidateOutputSlot = outputSlot;
                                    // check for collisions
                                    Vector2f slotLineupVector = Vector2f.sub(inputSlot.getSlotPosition(), candidateOutputSlot.getSlotPosition(), null);
                                    Vector2f candidatePosition = new Vector2f(Vector2f.add(currentTile.getPosition(), slotLineupVector, null));
                                    if (collidesWithExistingTiles(candidate, candidatePosition, tilesInTown)) {
                                        candidateOutputSlot = null;
                                    }
                                    break;
                                }
                            }
                            if (candidateOutputSlot != null) {
                                break;
                            }
                        }
                        if (candidateOutputSlot != null) {
                            break;
                        }
                    }
                }

                if (candidateOutputSlot == null) {
                    candidate = null;
                }

                if (candidate != null) {
                    Vector2f slotLineupVector = Vector2f.sub(inputSlot.getSlotPosition(), candidateOutputSlot.getSlotPosition(), null);
                    Vector2f candidatePosition = new Vector2f(Vector2f.add(currentTile.getPosition(), slotLineupVector, null));
                    candidate.setPosition(candidatePosition);
                    tilesInTown.add(candidate);
                    tilesToEval.add(candidate);
                    if (candidate.getType() == TSTT_PATH) {
                        totalPaths += 1;
                    }
                }
            }


            tilesToEval.remove(currentTile);
        }
        return tilesInTown;
    }

    /**
     * Check if a candidate tile collides with tiles that have already been placed
     */
    private boolean collidesWithExistingTiles(TownStructureTile candidate, Vector2f candidatePosition, ArrayList<TownStructureTile> tilesInTown) {
        TownStructureTile newCandidate = new TownStructureTile(candidate.getType(), candidate.getStructureSuperType(), candidate.getInputSlots(), candidate.getOutputSlots(), candidate.getRotation());
        newCandidate.setPosition(candidatePosition);

        for (TownStructureTile existingTile : tilesInTown) {
            if (existingTile.collidesWith(newCandidate)) {
                return true;
            }
        }

        return false;
    }
}
