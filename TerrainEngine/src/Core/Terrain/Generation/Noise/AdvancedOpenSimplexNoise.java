package Core.Terrain.Generation.Noise;

import Core.Control.Settings;
import org.lwjgl.util.vector.Vector2f;

import java.util.HashMap;
import java.util.Map;

public class AdvancedOpenSimplexNoise implements Noise {

    private OpenSimplexNoise noise;

    public AdvancedOpenSimplexNoise(long seed) {
        noise = new OpenSimplexNoise(seed);
    }

    @Override
    public float eval(float x, float y) {
        return (float) noise.eval(x, y);
    }

    /**
     * Generates markers using Poisson Disk Sampling
     */
    @Override
    public Map<Vector2f, Integer> generateMarkers(int minimumRadius, int searchRadius, int chunkX, int chunkZ, int totalVariants) {
        int startX = chunkX * Settings.getChunkSize();
        int endX = startX + Settings.getChunkSize();
        int startZ = chunkZ * Settings.getChunkSize();
        int endZ = startZ + Settings.getChunkSize();

        Map<Vector2f, Integer> markers = new HashMap<>();
        for (int tz = startZ - searchRadius; tz < endZ + searchRadius; tz++) {
            for (int tx = startX - searchRadius; tx < endX + searchRadius; tx++) {

                float value = eval(tx, tz);
                boolean largest = true;
                for (int sz = -minimumRadius; sz <= minimumRadius; sz++) {
                    for (int sx = -minimumRadius; sx <= minimumRadius; sx++) {
                        if (noise.eval(tx + sx, tz + sz) > value) {
                            largest = false;
                            break;
                        }
                    }
                    if (!largest) {
                        break;
                    }
                }
                if (largest) {
                    markers.put(new Vector2f(tx, tz), Math.abs((tx * tz) + tz) % totalVariants);
                }
            }
        }
        return markers;
    }

}
