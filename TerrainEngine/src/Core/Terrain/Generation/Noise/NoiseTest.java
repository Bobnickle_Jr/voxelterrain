package Core.Terrain.Generation.Noise;

import Core.Control.Settings;
import Core.Utils.Maths;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NoiseTest {

    public static void main(String... args) throws IOException {
        Noise noise = new AdvancedOpenSimplexNoise(0);
        Noise noise2 = new AdvancedOpenSimplexNoise(1);
        {
            float[][] noiseMap = genNoiseMap(noise, 3 * Settings.getChunkSize(), 1);
            saveAsRawNoise(noiseMap, "struct_0");
            noiseMap = generateMarkersMap(noiseMap, 1, 5);
            saveAsMarkerMap(noiseMap, "struct_1");
        }
        {
            float[][] noiseMap = genNoiseMap(noise, 3 * Settings.getChunkSize(), 10);
            saveAsRawNoise(noiseMap, "height_0");
            saveAsHeightMap(noiseMap, "height_1");
        }
        {
            float[][] noiseMap = genNoiseMap(noise, 3 * Settings.getChunkSize(), 1);
            saveAsRawNoise(noiseMap, "biome_0");
            saveAsMarkerMap(generateMarkersMap(noiseMap, 5, 5), "biome_1");
            saveAsMarkerMap(generateBiomeMap(noiseMap, 5, 5, 1), "biome_2_1");
            saveAsMarkerMap(generateBiomeMap(noiseMap, 5, 5, 2), "biome_2_2");
            saveAsMarkerMap(generateBiomeMap(noiseMap, 5, 5, 3), "biome_2_3");
        }
        {
            float[][] noiseMap = genNoiseMap(noise2, 3 * Settings.getChunkSize(), 10);
            saveAsRawNoise(noiseMap, "river_0");
            saveAsHeightMap(generateRiverMap(noiseMap, 0.075f), "river_2");
        }
    }

    private static float[][] generateRiverMap(float[][] noiseMap, float a) {
        for (int x = 0; x < noiseMap.length; x += 1) {
            for (int z = 0; z < noiseMap.length; z += 1) {
                noiseMap[x][z] = (Math.abs(noiseMap[x][z]) < a) ? -0.5f : 1;
            }
        }
        return noiseMap;
    }

    private static float[][] generateBiomeMap(float[][] noiseMap, int minRadius, int totalVariants, int p) {
        Map<Vector2f, Integer> markers = new HashMap<>();
        for (int tz = minRadius; tz < noiseMap.length - minRadius; tz++) {
            for (int tx = minRadius; tx < noiseMap.length - minRadius; tx++) {

                float value = noiseMap[tx][tz];
                boolean largest = true;
                for (int sz = -minRadius; sz <= minRadius; sz++) {
                    for (int sx = -minRadius; sx <= minRadius; sx++) {
                        if (tx + sx < 0 || tx + sx >= noiseMap.length || tz + sz < 0 || tz + sz >= noiseMap.length) {
                            break;
                        }
                        if (noiseMap[tx + sx][tz + sz] > value) {
                            largest = false;
                            break;
                        }
                    }
                    if (!largest) {
                        break;
                    }
                }
                if (largest) {
                    markers.put(new Vector2f(tx, tz), Math.abs((tx * tz) + tz) % totalVariants);
                }
            }
        }

        float[][] voronoi = new float[noiseMap.length][noiseMap.length];

        for (int x = 0; x < voronoi.length; x++) {
            for (int z = 0; z < voronoi[0].length; z++) {
                Integer nearestValue = null;
                Vector2f nearestCoord = null;
                for (Vector2f markerCoord : markers.keySet()) {
                    if (nearestCoord == null || (Maths.distance(markerCoord.x, markerCoord.y, x, z, p)
                            <= Maths.distance(nearestCoord.x, nearestCoord.y, x, z, p))) {
                        nearestValue = markers.get(markerCoord);
                        nearestCoord = markerCoord;
                    }
                }
                if (nearestValue != null) {
                    voronoi[x][z] = (0.2f * nearestValue) % 1f;
                }
            }
        }
        return voronoi;
    }

    private static float[][] generateMarkersMap(float[][] noiseMap, int minRadius, int totalVariants) {
        Map<Vector2f, Integer> markers = new HashMap<>();
        for (int tz = minRadius; tz < noiseMap.length - minRadius; tz++) {
            for (int tx = minRadius; tx < noiseMap.length - minRadius; tx++) {

                float value = noiseMap[tx][tz];
                boolean largest = true;
                for (int sz = -minRadius; sz <= minRadius; sz++) {
                    for (int sx = -minRadius; sx <= minRadius; sx++) {
                        if (tx + sx < 0 || tx + sx >= noiseMap.length || tz + sz < 0 || tz + sz >= noiseMap.length) {
                            break;
                        }
                        if (noiseMap[tx + sx][tz + sz] > value) {
                            largest = false;
                            break;
                        }
                    }
                    if (!largest) {
                        break;
                    }
                }
                if (largest) {
                    markers.put(new Vector2f(tx, tz), Math.abs((tx * tz) + tz) % totalVariants);
                }
            }
        }

        noiseMap = new float[noiseMap.length][noiseMap.length];
        for (Vector2f coord : markers.keySet()) {
             int value = markers.get(coord);
             noiseMap[(int) coord.x][(int) coord.y] = (0.2f * value) % 1f;
        }
        return noiseMap;
    }

    private static void saveAsRawNoise(float[][] noiseMap, String filename) throws IOException {
        int size = noiseMap.length;
        BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                double value = noiseMap[x][y];
                int rgb = 0x010101 * (int)((value + 1) * 127.5);
                image.setRGB(x, y, rgb);
            }
        }
        ImageIO.write(image, "png", new File("./" + filename + ".png"));
    }

    private static void saveAsHeightMap(float[][] noiseMap, String filename) throws IOException {

        int size = noiseMap.length;
        BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                double value = noiseMap[x][y];

                int rgb; // = 0x010101 * (int)((value + 1) * 127.5);
                if (value < -0.25) {
                    rgb = 0x0000aa;
                } else if (value < -0.2) {
                    rgb = 0xaaaa00;
                } else if (value < 0) {
                    rgb = 0x00aa00;
                } else if (value < 0.3) {
                    rgb = 0x006600;
                } else if (value < 0.6) {
                    rgb = 0x003300;
                } else {
                    rgb = 0x001100;
                }

                image.setRGB(x, y, rgb);
            }
        }
        ImageIO.write(image, "png", new File("./" + filename + ".png"));
    }

    private static void saveAsMarkerMap(float[][] noiseMap, String filename) throws IOException {

        int size = noiseMap.length;
        BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                double value = noiseMap[x][y];

                int rgb = 0x010101 * (int)((value + 1) * 127.5);
//                if (value > 0.85) {
//                    rgb = 0x0000aa;
//                } else if (value > 0.65) {
//                    rgb = 0xaaaa00;
//                } else if (value > 0.45) {
//                    rgb = 0x00aa00;
//                } else if (value > 0.25) {
//                    rgb = 0x006600;
//                } else if (value > 0.05) {
//                    rgb = 0x003300;
//                } else {
//                    rgb = 0x001100;
//                }

                image.setRGB(x, y, rgb);
            }
        }
        ImageIO.write(image, "png", new File("./" + filename + ".png"));
    }

    private static float[][] genNoiseMap(Noise noise, int size, float frequency) {
        float[][] output = new float[size][size];
        for (int x = 0 ; x < size; x += 1) {
            for (int z = 0; z < size; z += 1) {
                output[x][z] = noise.eval(x / frequency, z / frequency);
            }
        }
        return output;
    }
}
