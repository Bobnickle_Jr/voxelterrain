package Core.Terrain.Generation.Noise;

import org.lwjgl.util.vector.Vector2f;

import java.util.Map;

public interface Noise {

    float eval(float x, float y);

    Map<Vector2f, Integer> generateMarkers(int minimumRadius, int searchRadius, int chunkX, int chunkZ, int totalVariants);

}
