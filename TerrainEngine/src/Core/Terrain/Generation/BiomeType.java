package Core.Terrain.Generation;

import Core.Terrain.Data.Blocks.BlockType;
import Core.Terrain.Generation.Structures.ComplexStructureGenSettings;
import Core.Terrain.Generation.Structures.StructureGenSettings;
import Core.Utils.Logging.Log;

import java.util.ArrayList;
import java.util.Arrays;

import static Core.Terrain.Data.Blocks.BlockType.*;
import static Core.Terrain.Generation.Structures.ComplexStructureGenSettings.CSG_TOWN;
import static Core.Terrain.Generation.Structures.StructureGenSettings.*;

public enum BiomeType {

    BMT_RIVER("River", 8, 5, 20, false,  new StructureGenSettings[] {}, new ComplexStructureGenSettings[] {}, BLT_DIRT, BLT_STONE),
    BMT_LAKE("Lake", 8, 5, 20, true, new StructureGenSettings[] {}, new ComplexStructureGenSettings[] {}, BLT_DIRT, BLT_STONE),

    BMT_PLAINS("Plains", 23, 10, 32, true, new StructureGenSettings[] {SGS_PLANTS, SGS_RARE_TREES}, new ComplexStructureGenSettings[] {CSG_TOWN}, BLT_GRASS, BLT_DIRT, BLT_DIRT, BLT_STONE),
    BMT_FOREST("Forest", 23, 10, 32, true, new StructureGenSettings[] {SGS_PLANTS, SGS_DENSE_TREES}, new ComplexStructureGenSettings[] {CSG_TOWN}, BLT_GRASS, BLT_DIRT, BLT_DIRT, BLT_STONE),
    BMT_DESERT("Desert", 23, 10, 30, true, new StructureGenSettings[] {SGS_CACTUS, SGS_DEAD_BUSH}, new ComplexStructureGenSettings[] {CSG_TOWN}, BLT_SAND, BLT_SAND, BLT_SAND, BLT_SAND, BLT_SAND, BLT_STONE),
    BMT_ARCTIC("Arctic", 23, 10, 25, true, new StructureGenSettings[] {}, new ComplexStructureGenSettings[] {CSG_TOWN}, BLT_SNOW, BLT_SNOW, BLT_STONE),
    BMT_HILLS("Hills", 30, 20, 25, true, new StructureGenSettings[] {SGS_PLANTS, SGS_RARE_TREES}, new ComplexStructureGenSettings[] {CSG_TOWN}, BLT_GRASS, BLT_DIRT, BLT_DIRT, BLT_STONE),

    BMT_MOUNT("Mountain", 45, 70, 22, true, new StructureGenSettings[] {SGS_PLANTS, SGS_RARE_TREES}, new ComplexStructureGenSettings[] {}, BLT_GRASS, BLT_DIRT, BLT_DIRT, BLT_STONE),
    BMT_BARRON_MOUNT("Barron Mountain", 40, 70, 22, true, new StructureGenSettings[] {}, new ComplexStructureGenSettings[] {}, BLT_STONE),
    ;

    private String name;
    private byte minHeight;
    private byte heightVariance;
    private int terrainScale;
    private boolean findable;
    private BlockType[] blockLayers;
    private ArrayList<StructureGenSettings> structureGenSettings;
    private ArrayList<ComplexStructureGenSettings> complexStructureGenSettings;

    BiomeType(String name, int minHeight, int heightVariance, int terrainScale, boolean findable, StructureGenSettings[] structureGenSettings, ComplexStructureGenSettings[] complexStructureGenSettings, BlockType... blockLayers) {
        this.name = name;
        this.minHeight = (byte) minHeight;
        this.heightVariance = (byte) heightVariance;
        this.terrainScale = terrainScale;
        this.findable = findable;
        this.blockLayers = blockLayers;
        this.structureGenSettings = new ArrayList<>(Arrays.asList(structureGenSettings));
        this.complexStructureGenSettings = new ArrayList<>(Arrays.asList(complexStructureGenSettings));
    }

    public static BiomeType getByName(String name) {
        for (BiomeType biomeType : values()) {
            if (biomeType.findable && biomeType.name.equalsIgnoreCase(name)) {
                return biomeType;
            }
        }
        Log.warning("Failed to find biome type with name: " + name + ".");
        return null;
    }

    public static ArrayList<BiomeType> getAllFindable() {
        ArrayList<BiomeType> biomes = new ArrayList<>();
        for (BiomeType biomeType : values()) {
            if (biomeType.findable) {
                biomes.add(biomeType);
            }
        }
        return biomes;
    }

    // getters and setters

    public byte getMinHeight() {
        return minHeight;
    }

    public byte getHeightVariance() {
        return heightVariance;
    }

    public int getTerrainScale() {
        return terrainScale;
    }

    public BlockType[] getBlockLayers() {
        return blockLayers;
    }

    public ArrayList<StructureGenSettings> getStructureGenSettings() {
        return structureGenSettings;
    }

    public ArrayList<ComplexStructureGenSettings> getComplexStructureGenSettings() {
        return complexStructureGenSettings;
    }
}
