package Core.Terrain.Generation.Structures;

public enum ComplexStructureGenSettings {

    CSG_TOWN(20),

    ;

    private final int separationScale;

    ComplexStructureGenSettings(int separationScale) {
        this.separationScale = separationScale;
    }

    // getters and setters

    public int getSeparationScale() {
        return separationScale;
    }
}
