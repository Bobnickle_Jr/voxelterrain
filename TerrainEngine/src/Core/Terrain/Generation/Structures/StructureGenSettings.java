package Core.Terrain.Generation.Structures;

import static Core.Terrain.Generation.Structures.StructureSuperType.*;

public enum StructureGenSettings {


    SGS_RARE_TREES(SST_SMALL_TREES, 20, 8),
    SGS_DENSE_TREES(SST_ALL_TREES, 5, 8),
    SGS_PLANTS(SST_PLANT_LIFE, 1, 0),
    SGS_CACTUS(SST_CACTI, 5, 0),
    SGS_DEAD_BUSH(SST_DEAD_PLANTS, 2, 0),

    ;

    private final StructureSuperType structureSuperType;
    private final int minimumRadius;
    private final int searchRadius;

    StructureGenSettings(StructureSuperType structureSuperType, int minimumRadius, int searchRadius) {
        this.structureSuperType = structureSuperType;
        this.minimumRadius = minimumRadius;
        this.searchRadius = searchRadius;
    }

    // getters and setters

    public StructureSuperType getStructureSuperType() {
        return structureSuperType;
    }

    public int getMinimumRadius() {
        return minimumRadius;
    }

    public int getSearchRadius() {
        return searchRadius;
    }

}
