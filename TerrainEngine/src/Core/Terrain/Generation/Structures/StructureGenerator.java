package Core.Terrain.Generation.Structures;

import Core.Control.Settings;
import Core.Terrain.Data.Blocks.BlockType;
import Core.Utils.Logging.Log;
import Core.Utils.Maths;
import Core.Utils.Saving.SaveFileUtils;
import org.lwjgl.util.vector.Vector3f;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class StructureGenerator {


    public static StructureTemplate generateTemplate(StructureType type) {
        Log.info("Generating Structure Template: " + type.getAssetName());

        // load content of file
        ArrayList<String> data = loadDataFromFile(type);

        // setup blocks
        String[] dimensions = data.get(0).split(":");
        data.remove(0);
        int width = Integer.parseInt(dimensions[0]);
        int height = Integer.parseInt(dimensions[1]);
        int depth = Integer.parseInt(dimensions[2]);
        BlockType[][][] blockTypes = new BlockType[width][height][depth];

        // setup offset
        String[] offset = data.get(0).split(":");
        data.remove(0);
        int xOffset = Integer.parseInt(offset[0]);
        int yOffset = Integer.parseInt(offset[1]);
        int zOffset = Integer.parseInt(offset[2]);

        // load schema
        for (String dataLine : data) {
            try {
                String[] blockData = dataLine.split(":");
                int x = Integer.parseInt(blockData[0]);
                int y = Integer.parseInt(blockData[1]);
                int z = Integer.parseInt(blockData[2]);
                int blockTypeOrd = Integer.parseInt(blockData[3]);
                blockTypes[x][y][z] = BlockType.getByOrd(blockTypeOrd);
            } catch (Exception e) {}
        }
        return new StructureTemplate(blockTypes, width, height, depth, xOffset, yOffset, zOffset);
    }

    public static boolean createStructureFileFromMarkers(String fileName, HashMap<Vector3f, BlockType> markers, int yOffset) {
        if (markers.size() == 0) {
            return false;
        }

        // normalise locations to start from 0, 0, 0
        Vector3f lowestCorner = null;
        Vector3f highestCorner = null;
        for (Vector3f location : markers.keySet()) {
            if (lowestCorner == null) {
                lowestCorner = new Vector3f(location);
            } else {
                lowestCorner.x = Math.min(lowestCorner.x, location.x);
                lowestCorner.y = Math.min(lowestCorner.y, location.y);
                lowestCorner.z = Math.min(lowestCorner.z, location.z);
            }
            if (highestCorner == null) {
                highestCorner = new Vector3f(location);
            } else {
                highestCorner.x = Math.max(highestCorner.x, location.x);
                highestCorner.y = Math.max(highestCorner.y, location.y);
                highestCorner.z = Math.max(highestCorner.z, location.z);
            }
        }

        // calculate dimensions
        Vector3f dimensions = Vector3f.sub(highestCorner, lowestCorner, null);
        dimensions = Vector3f.add(dimensions, new Vector3f(1, 1, 1), null);

        // save schema
        StringBuilder blockData = new StringBuilder();
        blockData.append((int) dimensions.x).append(":");
        blockData.append((int) dimensions.y).append(":");
        blockData.append((int) dimensions.z).append("\n");

        blockData.append((int) (dimensions.x / 2)).append(":");
        blockData.append(yOffset).append(":");
        blockData.append((int) (dimensions.z / 2)).append("\n");

        for (Vector3f location : markers.keySet()) {
            BlockType blockType = markers.get(location);
            blockData.append((int) (location.x - lowestCorner.x)).append(":");
            blockData.append((int) (location.y - lowestCorner.y)).append(":");
            blockData.append((int) (location.z - lowestCorner.z)).append(":");
            blockData.append(blockType.ordinal()).append("\n");
        }

        return SaveFileUtils.saveStringToFile(Settings.getStructureFileLocation() + "/savedFromGame/" + fileName + ".str", blockData.toString());
    }

    public static StructureTemplate generateOrganicTree(Long seed, int trunkDiam, int trunkHeight,
                                                        int canopyDiam, int canopyHeight,
                                                        int clusterDiam, int totalClusters, int canopyRoughness,
                                                        BlockType trunkBlockType, BlockType canopyBlockType) {
        Random generator = new Random(seed);


        // generate trunk
        HashMap<Vector3f, BlockType> trunkBlocks = new HashMap<>();
        for (int y = 0; y < trunkHeight; y += 1) {
            for (int x = -trunkDiam / 2; x < trunkDiam / 2f; x += 1) {
                for (int z = -trunkDiam / 2; z < trunkDiam / 2f; z += 1) {
                    trunkBlocks.put(new Vector3f(x, y, z), trunkBlockType);
                }
            }
        }
        // create canopy clusters
        HashMap<Vector3f, Integer> canopyClusters = new HashMap<>();
        for (int c = 0; c < totalClusters; c += 1) {
            Vector3f clusterLocation = new Vector3f(generator.nextInt(canopyDiam) - canopyDiam / 2f, trunkHeight + generator.nextInt(canopyHeight) - (2 * canopyHeight / 3f), generator.nextInt(canopyDiam) - canopyDiam / 2f);
            int clusterRadius = 5;
            canopyClusters.put(clusterLocation, clusterRadius);
        }
        HashMap<Vector3f, BlockType> canopyBlocks = new HashMap<>();
        for (Vector3f clusterLocation : canopyClusters.keySet()) {
            for (int x = -clusterDiam / 2;  x < clusterDiam / 2f; x += 1) {
                for (int y = -clusterDiam / 2;  y < clusterDiam / 2f; y += 1) {
                    for (int z = -clusterDiam / 2;  z < clusterDiam / 2f; z += 1) {
                        if (Maths.distance(new Vector3f(0, 0, 0), new Vector3f(x, y, z), 2) < (clusterDiam / 2f) - generator.nextInt(canopyRoughness)) {
                            canopyBlocks.put(Vector3f.add(clusterLocation, new Vector3f(x, y, z), null), canopyBlockType);
                        }
                    }
                }
            }
        }

        HashMap<Vector3f, BlockType> generatedBlocks = new HashMap<>();
        generatedBlocks.putAll(canopyBlocks);
        generatedBlocks.putAll(trunkBlocks);

        // normalise vectors
        Vector3f inverseOffset = new Vector3f();
        Vector3f maxCorner = new Vector3f();
        for (Vector3f blockCoord : generatedBlocks.keySet()) {
            inverseOffset.x = Math.min(inverseOffset.x, blockCoord.x);
            inverseOffset.y = Math.min(inverseOffset.y, blockCoord.y);
            inverseOffset.z = Math.min(inverseOffset.z, blockCoord.z);

            maxCorner.x = Math.max(maxCorner.x, blockCoord.x);
            maxCorner.y = Math.max(maxCorner.y, blockCoord.y);
            maxCorner.z = Math.max(maxCorner.z, blockCoord.z);
        }
        Vector3f offset = inverseOffset.negate(null);
        maxCorner = Vector3f.add(maxCorner, offset, null);
        HashMap<Vector3f, BlockType> normalisedBlocks = new HashMap<>();
        for (Vector3f blockCoord : generatedBlocks.keySet()) {
            Vector3f normalisedBlockCoord = Vector3f.add(blockCoord, offset, null);
            normalisedBlocks.put(normalisedBlockCoord, generatedBlocks.get(blockCoord));
        }

        // build template
        BlockType[][][] blockTypes = new BlockType[(int) maxCorner.x + 1][(int) maxCorner.y + 1][(int) maxCorner.z + 1];
        for (Vector3f blockCoord : normalisedBlocks.keySet()) {
            blockTypes[(int) blockCoord.x][(int) blockCoord.y][(int) blockCoord.z] = normalisedBlocks.get(blockCoord);
        }

        return new StructureTemplate(blockTypes, (int) maxCorner.x + 1, (int) maxCorner.y + 1, (int) maxCorner.z + 1, (int) offset.x, (int) offset.y, (int) offset.z);
    }

    private static ArrayList<String> loadDataFromFile(StructureType type) {
        try {
            String structureFileName = Settings.getStructureFileLocation() + "/" + type.getFullAssetPath();
            File myObj = new File(structureFileName);
            Scanner myReader = new Scanner(myObj);
            ArrayList<String> structureData = new ArrayList<>();
            while (myReader.hasNextLine()) {
                structureData.add(myReader.nextLine());
            }
            return structureData;
        } catch (FileNotFoundException e) {
            Log.error("Failed to load Structure FIle for: " + type + ".");
            return null;
        }
    }

}
