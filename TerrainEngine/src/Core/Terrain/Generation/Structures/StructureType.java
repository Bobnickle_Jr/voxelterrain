package Core.Terrain.Generation.Structures;

import Core.Control.Settings;
import Core.Terrain.Data.Blocks.BlockType;
import Core.Utils.Logging.Log;
import org.lwjgl.util.vector.Vector3f;

import java.util.HashMap;
import java.util.Map;

import static Core.Terrain.Data.Blocks.BlockType.*;
import static Core.Terrain.Generation.Structures.StructureGenerationMethod.SGM_ORGANIC_TREE;
import static Core.Terrain.Generation.Structures.StructureGenerationMethod.SGM_TEMPLATE;

public enum StructureType {

    ST_TREE_SMALL_0(null, 10, SGM_ORGANIC_TREE, false, 1, 6, 3, 3, 5, 5, 2, BLT_WOOD_LOG, BLT_LEAVES_1),
    ST_TREE_MED_0(null, 4, SGM_ORGANIC_TREE, false, 2, 10, 5, 5, 7, 7, 2, BLT_WOOD_LOG, BLT_LEAVES_1),
    ST_TREE_LARGE_0(null, 1, SGM_ORGANIC_TREE, false, 3, 16, 10, 10, 10, 20, 2, BLT_WOOD_LOG, BLT_LEAVES_1),
    ST_TREE_SMALL_1(null, 10, SGM_ORGANIC_TREE, false, 1, 6, 3, 3, 5, 5, 2, BLT_WOOD_LOG, BLT_LEAVES_2),
    ST_TREE_MED_1(null, 4, SGM_ORGANIC_TREE, false, 2, 10, 5, 5, 7, 7, 2, BLT_WOOD_LOG, BLT_LEAVES_2),
    ST_TREE_LARGE_1(null, 1, SGM_ORGANIC_TREE, false, 3, 16, 10, 10, 10, 20, 2, BLT_WOOD_LOG, BLT_LEAVES_2),
    ST_TREE_SMALL_2(null, 10, SGM_ORGANIC_TREE, false, 1, 6, 3, 3, 5, 5, 2, BLT_WOOD_LOG, BLT_LEAVES_3),
    ST_TREE_MED_2(null, 4, SGM_ORGANIC_TREE, false, 2, 10, 5, 5, 7, 7, 2, BLT_WOOD_LOG, BLT_LEAVES_3),
    ST_TREE_LARGE_2(null, 1, SGM_ORGANIC_TREE, false, 3, 16, 10, 10, 10, 20, 2, BLT_WOOD_LOG, BLT_LEAVES_3),

    ST_BUSH_0("Plants/BUSH_0", 1,  SGM_TEMPLATE, false),

    ST_DEAD_BUSH_0("DeadPlants/DEAD_BUSH_0", 1,  SGM_TEMPLATE, false),

    ST_CACTUS_0("Cactus/CACTUS_0", 1, SGM_TEMPLATE, false),

    ST_TOWN_WELL_0("Town/Well/TOWN_WELL_0", 1, SGM_TEMPLATE, false),
    ST_TOWN_PATH_0("Town/Path/TOWN_PATH_0", 1, SGM_TEMPLATE, true),
    ST_TOWN_PATH_1("Town/Path/TOWN_PATH_1", 1, SGM_TEMPLATE, true),
    ST_TOWN_PATH_2("Town/Path/TOWN_PATH_2", 1, SGM_TEMPLATE, true),

    ST_TOWN_HOUSE_0("Town/House/TOWN_HOUSE_0", 1, SGM_TEMPLATE, false),
    ST_TOWN_HOUSE_1("Town/House/TOWN_HOUSE_1", 1, SGM_TEMPLATE, false),
    ST_TOWN_HOUSE_2("Town/House/TOWN_HOUSE_2", 1, SGM_TEMPLATE, false),

    ST_TOWN_BUSH_0("Town/Decor/TOWN_BUSH_0", 1, SGM_TEMPLATE, false),
    ST_TOWN_TREE_0("Town/Decor/TOWN_TREE_0", 1, SGM_TEMPLATE, false),

    ;

    private final String assetName;
    private StructureTemplate template = null;
    private int ratioValue;
    private StructureGenerationMethod genMethod;
    private Object[] genVariables;
    private boolean smoothToLandscape;

    private static HashMap<StructureType, HashMap<Long, StructureTemplate>> organicCache = new HashMap<>();

    StructureType(String assetName, int ratioValue, StructureGenerationMethod genMethod, boolean smoothToLandscape,  Object... genVariables) {
        this.assetName = assetName;
        this.ratioValue = ratioValue;
        this.genMethod = genMethod;
        this.genVariables = genVariables;
        this.smoothToLandscape = smoothToLandscape;
    }

    public Map<Vector3f, BlockType> getBlocksInChunk(Vector3f worldCoord, int chunkX, int chunkZ, Long seed, int structRotation) {
        StructureTemplate structureTemplate = getTemplate(seed, structRotation);
        int targetX = (int) worldCoord.x - structureTemplate.getxOffset() - (chunkX * Settings.getChunkSize());
        int targetY = (int) worldCoord.y - structureTemplate.getyOffset();
        int targetZ = (int) worldCoord.z - structureTemplate.getzOffset() - (chunkZ * Settings.getChunkSize());
        BlockType[][][] templateBlocks = structureTemplate.getBlockTypes();
        Map<Vector3f, BlockType> blocksInChunk = new HashMap<>();
        for (int x = targetX; x < targetX + structureTemplate.getWidth(); x += 1) {
            for (int y = targetY; y < targetY + structureTemplate.getHeight(); y += 1) {
                for (int z = targetZ; z < targetZ + structureTemplate.getDepth(); z += 1) {
                    if (x >= 0 && x < Settings.getChunkSize() && y >= 0 && y < Settings.getChunkHeight() && z >= 0 && z < Settings.getChunkSize()) {
                        BlockType blockType = templateBlocks[x - targetX][y - targetY][z - targetZ];
                        if (blockType != null) {
                            blocksInChunk.put(new Vector3f(x, y, z), blockType);
                        }
                    }
                }
            }
        }
        return blocksInChunk;
    }

    public StructureTemplate getTemplate(Long seed, int rotation) {
        try {
            switch (genMethod) {
                case SGM_TEMPLATE:
                    if (template == null) {
                        template = StructureGenerator.generateTemplate(this);
                    }
                    return template.rotate(rotation);
                case SGM_ORGANIC_TREE:
                    template = getOrganicTemplateFromCache(this, seed);
                    if (template == null) {
                        template = StructureGenerator.generateOrganicTree(seed,
                                (int) genVariables[0], (int) genVariables[1], (int) genVariables[2], (int) genVariables[3], (int) genVariables[4],
                                (int) genVariables[5], (int) genVariables[6], (BlockType) genVariables[7], (BlockType) genVariables[8]);
                        cacheOrganicTemplate(this, seed, template);
                    }
                    break;
            }
        } catch (Exception e) {
            Log.error("Failed to Generate Structure Template");
            e.printStackTrace();
        }
        return template;
    }

    private static StructureTemplate getOrganicTemplateFromCache(StructureType type, Long seed) {
        HashMap<Long, StructureTemplate> cacheForType = organicCache.get(type);
        if (cacheForType != null) {
            if (cacheForType.get(seed) != null) {
                return cacheForType.get(seed);
            }
        }
        return null;
    }

    private static void cacheOrganicTemplate(StructureType type, Long seed, StructureTemplate template) {
        HashMap<Long, StructureTemplate> cacheForType = organicCache.computeIfAbsent(type, k -> new HashMap<>());
        cacheForType.putIfAbsent(seed, template);
    }

    public String getFullAssetPath() {
        return assetName + ".str";
    }

    // getters and setters

    public String getAssetName() {
        return assetName;
    }

    public int getRatioValue() {
        return ratioValue;
    }

    public boolean isSmoothToLandscape() {
        return smoothToLandscape;
    }

}
