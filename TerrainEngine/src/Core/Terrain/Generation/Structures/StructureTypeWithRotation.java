package Core.Terrain.Generation.Structures;

public class StructureTypeWithRotation {

    public StructureType type;
    public int rotation;

    public StructureTypeWithRotation(StructureType type, int rotation) {
        this.type = type;
        this.rotation = rotation;
    }
}
