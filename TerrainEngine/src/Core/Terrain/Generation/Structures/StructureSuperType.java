package Core.Terrain.Generation.Structures;

import Core.Control.Settings;

import java.util.ArrayList;
import java.util.Random;

import static Core.Terrain.Generation.Structures.StructureType.*;

public enum StructureSuperType {

    SST_SMALL_TREES(Settings.getWaterLevel(), ST_TREE_SMALL_0, ST_TREE_SMALL_1, ST_TREE_SMALL_2),
    SST_ALL_TREES( Settings.getWaterLevel(), ST_TREE_SMALL_0, ST_TREE_SMALL_1, ST_TREE_SMALL_2,
            ST_TREE_MED_0, ST_TREE_MED_1, ST_TREE_MED_2, ST_TREE_LARGE_0, ST_TREE_LARGE_1, ST_TREE_LARGE_2),
    SST_PLANT_LIFE(Settings.getWaterLevel(), ST_BUSH_0),
    SST_DEAD_PLANTS(Settings.getWaterLevel(), ST_DEAD_BUSH_0),
    SST_CACTI(Settings.getWaterLevel(), ST_CACTUS_0),

    SST_TOWN_CENTER(Settings.getWaterLevel(), ST_TOWN_WELL_0),
    SST_TOWN_PATH_0(Settings.getWaterLevel(), ST_TOWN_PATH_0),
    SST_TOWN_PATH_1(Settings.getWaterLevel(), ST_TOWN_PATH_1),
    SST_TOWN_PATH_2(Settings.getWaterLevel(), ST_TOWN_PATH_2),

    SST_TOWN_HOUSE_0(Settings.getWaterLevel(), ST_TOWN_HOUSE_0),
    SST_TOWN_HOUSE_1(Settings.getWaterLevel(), ST_TOWN_HOUSE_1),
    SST_TOWN_HOUSE_2(Settings.getWaterLevel(), ST_TOWN_HOUSE_2),

    SST_TOWN_BUSH_0(Settings.getWaterLevel(), ST_TOWN_BUSH_0),
    SST_TOWN_TREE_0(Settings.getWaterLevel(), ST_TOWN_TREE_0),
    ;

    private final int minSpawnHeight;
    private final StructureType[] children;
    private ArrayList<StructureType> ratioChildren = null;

    StructureSuperType(int minSpawnHeight, StructureType... children) {
        this.minSpawnHeight = minSpawnHeight;
        this.children = children;
    }

    public StructureType getFirstChild() {
        return children[0];
    }

    public StructureType getRandomChild(Long seed) {
        Random generator = new Random(seed);

        if (ratioChildren == null) {
            ratioChildren = new ArrayList<>();
            for (StructureType child : children) {
                for (int i = 0; i < child.getRatioValue(); i += 1) {
                    ratioChildren.add(child);
                }
            }
        }

        int randomIndex = generator.nextInt(ratioChildren.size());
        return ratioChildren.get(randomIndex);
    }

    // getters and setters

    public int getMinSpawnHeight() {
        return minSpawnHeight;
    }
}
