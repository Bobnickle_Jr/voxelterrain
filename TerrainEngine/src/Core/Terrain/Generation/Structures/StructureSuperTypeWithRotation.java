package Core.Terrain.Generation.Structures;

public class StructureSuperTypeWithRotation {

    public StructureSuperType superType;
    public int rotation;

    public StructureSuperTypeWithRotation(StructureSuperType superType, int rotation) {
        this.superType = superType;
        this.rotation = rotation;
    }
}
