package Core.Terrain.Generation.Structures;

import Core.Terrain.Data.Blocks.BlockType;

public class StructureTemplate {

    private BlockType[][][] blockTypes;
    private int width;
    private int height;
    private int depth;
    private int xOffset;
    private int yOffset;
    private int zOffset;

    public StructureTemplate(BlockType[][][] blockTypes, int width, int height, int depth, int xOffset, int yOffset, int zOffset) {
        this.blockTypes = blockTypes;
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.zOffset = zOffset;
    }

    public StructureTemplate rotate(int rotation) {
        if (rotation == 0) {
            return this;
        }

        // width, depth and offsets
        int newWidth = width;
        int newDepth = depth;
        if (rotation % 2 != 0) {
            newDepth = width;
            newWidth = depth;
        }

        // block positions
        int newXOffset = xOffset;
        int newZOffset = yOffset;
        BlockType[][][] newBlockTypes = new BlockType[newWidth][height][newDepth];
        for (int x = 0; x < newWidth; x += 1) {
            for (int y = 0; y < height; y += 1) {
                for (int z = 0; z < newDepth; z += 1) {
                    switch (rotation) {
                        case 3:
                            newXOffset = zOffset;
                            newZOffset = width - xOffset - 1;
                            newBlockTypes[x][y][z] = blockTypes[width-z-1][y][x];
                            break;
                        case 2:
                            newXOffset = width - xOffset - 1;
                            newZOffset = depth - zOffset - 1;
                            newBlockTypes[x][y][z] = blockTypes[width-x-1][y][depth-z-1];
                            break;
                        case 1:
                            newXOffset = depth - zOffset - 1;
                            newZOffset = xOffset;
                            newBlockTypes[x][y][z] = blockTypes[z][y][depth-x-1];
                            break;
                    }

                }
            }
        }

        return new StructureTemplate(newBlockTypes, newWidth, height, newDepth, newXOffset, yOffset, newZOffset);
    }

    // getters and setters

    public BlockType[][][] getBlockTypes() {
        return blockTypes;
    }

    public int getxOffset() {
        return xOffset;
    }

    public int getyOffset() {
        return yOffset;
    }

    public int getzOffset() {
        return zOffset;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }
}
