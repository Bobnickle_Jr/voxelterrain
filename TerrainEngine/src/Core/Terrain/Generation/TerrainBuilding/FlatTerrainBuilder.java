package Core.Terrain.Generation.TerrainBuilding;

import Core.Control.Settings;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Blocks.BlockType;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Terrain.Generation.BiomeType;
import Core.Terrain.Generation.ChunkGenerationData;
import Core.Terrain.Generation.Noise.AdvancedOpenSimplexNoise;
import Core.Terrain.Generation.Structures.*;
import Core.Terrain.Generation.TownGeneration.ITownBuilder;
import Core.Terrain.Generation.TownGeneration.JigsawTownBuilder;
import Core.Utils.Logging.Log;
import Core.Utils.Maths;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static Core.Terrain.Generation.Structures.ComplexStructureGenSettings.CSG_TOWN;

public class FlatTerrainBuilder extends TerrainBuilder {

    public AdvancedOpenSimplexNoise structureNoiseMap;
    private Long seed;
    private ITownBuilder townBuilder = new JigsawTownBuilder();

    public FlatTerrainBuilder(Long seed) {
        this.seed = seed;
        initialiseNoise();
    }

    /**
     * Use the seed to initialise noise functions that will be used in terrain generation
     */
    private void initialiseNoise() {
        Log.info("Initialising Noise...");
        structureNoiseMap = new AdvancedOpenSimplexNoise(seed + 1);
        Log.info("Noise Initialised.");
    }

    @Override
    public Block[][][] generateChunkTerrain(Chunk chunk) {
        ChunkGenerationData chunkGenData = getChunkGenerationData(chunk.getChunkX(), chunk.getChunkZ());
        return generateChunkTerrainFromData(chunk, chunkGenData);
    }

    /**
     * Generate the data maps needed for the target chunk to build to terrain
     */
    private ChunkGenerationData getChunkGenerationData(int chunkX, int chunkZ) {
        BiomeType[][] biomeMap = generateFlatBiomeMap();
        int[][] heightMap = generateFlatHeightMap();
        Map<Vector3f, StructureTypeWithRotation> structureMap = generateFlatStructureMap(chunkX, chunkZ, biomeMap);

        biomeMap = cropBiomeMap(biomeMap);
        return new ChunkGenerationData(heightMap, biomeMap, structureMap);
    }

    /**
     * Generates a 3 chunk x 3 chunk map centered on the target chunk coordinates
     * and consisting only of Plains biome tiles
     */
    private BiomeType[][] generateFlatBiomeMap() {
        BiomeType[][] biomeMap = new BiomeType[Settings.getChunkSize() * 3][Settings.getChunkSize() * 3];
        for (int x = 0; x < biomeMap.length; x += 1) {
            for (int z = 0; z < biomeMap[0].length; z += 1) {
                biomeMap[x][z] = BiomeType.BMT_PLAINS;
            }
        }
        return biomeMap;
    }

    /**
     * Generate a 1 chunk x 1 chunk height map consisting only of 0 height tiles
     */
    private int[][] generateFlatHeightMap() {
        int[][] heightMap = new int[Settings.getChunkSize()][Settings.getChunkSize()];
        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                heightMap[x][z] = 0;
            }
        }
        return heightMap;
    }

    /**
     * Generate a map of structure types and their suggested world coordinates for a flat world
     */
    private Map<Vector3f, StructureTypeWithRotation> generateFlatStructureMap(int chunkX, int chunkZ, BiomeType[][] biomeMap) {
        Map<Vector3f, StructureTypeWithRotation> structureMap = new HashMap<>();

        // generate simple structures
        Map<Vector2f, StructureSuperType> simpleStructureMarkers;
        simpleStructureMarkers = getStructureMarkers(chunkX, chunkZ, biomeMap, structureNoiseMap);
        // build map from markers
        for (Vector2f structureXZ : simpleStructureMarkers.keySet()) {
            StructureSuperType structureType = simpleStructureMarkers.get(structureXZ);
            int structureX = (int) structureXZ.x;
            int structureZ = (int) structureXZ.y; // this is not wrong, it's just strange
            int structureY = 1;
            Vector3f structureLocation = new Vector3f(structureX, structureY, structureZ);
            StructureTypeWithRotation structureTypeWithRotation =  new StructureTypeWithRotation(structureType.getRandomChild(seed * (long) structureLocation.length()), 0);
            structureMap.putIfAbsent(structureLocation, structureTypeWithRotation);
        }

        // generate complex structures
        Map<Vector2f, StructureSuperTypeWithRotation> complexStructureMarkers = genComplexStructureMarkers(chunkX, chunkZ, biomeMap);
        // build map from markers
        for (Vector2f structureXZ : complexStructureMarkers.keySet()) {
            StructureSuperTypeWithRotation structureType = complexStructureMarkers.get(structureXZ);
            int structureX = (int) structureXZ.x;
            int structureZ = (int) structureXZ.y; // this is not wrong, it's just strange
            int structureY = 1;
            Vector3f structureLocation = new Vector3f(structureX, structureY, structureZ);
            StructureTypeWithRotation structureTypeWithRotation =  new StructureTypeWithRotation(structureType.superType.getRandomChild(seed * (long) structureLocation.length()), structureType.rotation);
            structureMap.putIfAbsent(structureLocation, structureTypeWithRotation);
        }

        return structureMap;
    }

    /**
     * Use raw chunk generation data to construct the chunk terrain
     */
    public Block[][][] generateChunkTerrainFromData(Chunk chunk, ChunkGenerationData chunkGenData) {
        Block[][][] blocks = new Block[Settings.getChunkSize()][Settings.getChunkHeight()][Settings.getChunkSize()];

        // generate chunk terrain using height and biome maps
        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                BiomeType biomeType = chunkGenData.getBiomeMap()[x][z];
                int terrainHeight = chunkGenData.getHeightMap()[x][z];
                for (int y = Settings.getChunkHeight(); y >= 0; y -= 1) {
                    if (y <= terrainHeight) {
                        BlockType targetBlockType = biomeType.getBlockLayers()[Math.min(terrainHeight - y, biomeType.getBlockLayers().length - 1)];
                        blocks[x][y][z] = new Block(targetBlockType, chunk, 0, new Vector3f(x, y, z));
                    }
                }
            }
        }

        // generate structures using structure map
        for (Vector3f structureWorldCoord : chunkGenData.getStructureMap().keySet()) {
            StructureType structureType = chunkGenData.getStructureMap().get(structureWorldCoord).type;
            int structureRotation = chunkGenData.getStructureMap().get(structureWorldCoord).rotation;
            Map<Vector3f, BlockType> blocksInThisChunk = structureType.getBlocksInChunk(structureWorldCoord, chunk.getChunkX(), chunk.getChunkZ(), seed + (long) structureWorldCoord.length(), structureRotation);
            // build structure from template
            for (Vector3f blockLocation : blocksInThisChunk.keySet()) {
                BlockType blockType = blocksInThisChunk.get(blockLocation);
                if (structureType.isSmoothToLandscape()) {
                    blockLocation.y -= blockLocation.y - chunkGenData.getHeightMap()[(int) blockLocation.x][(int) blockLocation.z];
                }
                Block existingBlock = blocks[(int) blockLocation.x][(int) blockLocation.y][(int) blockLocation.z];
                if ((existingBlock == null || existingBlock.getType().getPriority() < blockType.getPriority())) {
                    blocks[(int) blockLocation.x][(int) blockLocation.y][(int) blockLocation.z] = new Block(blockType, chunk, 0, blockLocation);
                }
            }
        }
        return blocks;
    }

    /**
     * Generate a map of complex structure markers
     */
    private Map<Vector2f, StructureSuperTypeWithRotation> genComplexStructureMarkers(int chunkX, int chunkZ, BiomeType[][] biomeMap) {
        // get all biomes in region
        ArrayList<BiomeType> regionBiomes = new ArrayList<>();
        for (BiomeType[] biomeTypes : biomeMap) {
            for (BiomeType bType : biomeTypes) {
                if (!regionBiomes.contains(bType)) {
                    regionBiomes.add(bType);
                }
            }
        }

        // get all complex structure generation settings in region
        ArrayList<ComplexStructureGenSettings> structureGenSettings = new ArrayList<>();
        for (BiomeType biome : regionBiomes) {
            for (ComplexStructureGenSettings genSettings : biome.getComplexStructureGenSettings()) {
                if (!structureGenSettings.contains(genSettings)) {
                    structureGenSettings.add(genSettings);
                }
            }
        }

        // generate complex structure markers
        Map<Vector2f, StructureSuperTypeWithRotation> structureMarkers = new HashMap<>();
        for (ComplexStructureGenSettings genSettings : structureGenSettings) {
            Map<Vector2f, Integer> markers = structureNoiseMap.generateMarkers(10, 10, chunkX / genSettings.getSeparationScale(), chunkZ / genSettings.getSeparationScale(), 1);
            for (Vector2f markerLocation : markers.keySet()) {
                markerLocation.x *= genSettings.getSeparationScale();
                markerLocation.y *= genSettings.getSeparationScale();
                if (getBiomeOfTile((int) markerLocation.x, (int) markerLocation.y).getComplexStructureGenSettings().contains(genSettings)) {
                    structureMarkers.putAll(townBuilder.generateTown(seed + (long) markerLocation.x + (long) markerLocation.y, markerLocation));
                }
            }
        }

        // remove out of bounds markers
        HashMap<Vector2f, StructureSuperTypeWithRotation> allowedStructureMarkers = new HashMap<>();
        for (Vector2f location : structureMarkers.keySet()) {
            int structMapX = (int) location.x - chunkX * Settings.getChunkSize() + Settings.getChunkSize();
            int structMapZ = (int) location.y - chunkZ * Settings.getChunkSize() + Settings.getChunkSize();
            if (structMapX >= 0 && structMapX < biomeMap.length && structMapZ >= 0 && structMapZ < biomeMap.length) {
                BiomeType biomeType = biomeMap[structMapX][structMapZ];
                if (biomeType.getComplexStructureGenSettings().contains(CSG_TOWN)) {
                    allowedStructureMarkers.put(location, structureMarkers.get(location));
                }
            }
        }

        return allowedStructureMarkers;
    }

    /**
     * Calculate the biome of target tile
     */
    private BiomeType getBiomeOfTile(int x, int z) {
        BiomeType[][] biomeMap = generateFlatBiomeMap();
        int xInChunk = Maths.worldCoordToCoordInChunk(x);
        int zInChunk = Maths.worldCoordToCoordInChunk(z);
        return biomeMap[xInChunk + Settings.getChunkSize()][zInChunk + Settings.getChunkSize()];
    }

}
