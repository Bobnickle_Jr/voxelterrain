package Core.Terrain.Generation.TerrainBuilding;

import Core.Control.Settings;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Chunks.Chunk;
import org.lwjgl.util.vector.Vector3f;

import static Core.Terrain.Data.Blocks.BlockType.BLT_STONE;

public class UndergroundTerrainBuilder extends TerrainBuilder {

    @Override
    public Block[][][] generateChunkTerrain(Chunk chunk) {
        Block[][][] chunkTerrain = new Block[Settings.getChunkSize()][Settings.getChunkHeight()][Settings.getChunkSize()];
        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int y = 0; y < Settings.getChunkHeight(); y += 1) {
                for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                    chunkTerrain[x][y][z] = new Block(BLT_STONE, chunk, 0, new Vector3f(x, y, z));
                }
            }
        }
        return chunkTerrain;
    }
}
