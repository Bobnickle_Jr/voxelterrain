package Core.Terrain.Generation.TerrainBuilding;

import Core.Control.Settings;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Terrain.Generation.BiomeType;
import Core.Terrain.Generation.ChunkGenerationData;
import Core.Terrain.Generation.Noise.Noise;
import Core.Terrain.Generation.Structures.StructureGenSettings;
import Core.Terrain.Generation.Structures.StructureSuperType;
import Core.Utils.Maths;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class TerrainBuilder {

    public abstract Block[][][] generateChunkTerrain(Chunk chunk);

    /**
     * Crops a 3 chunk x 3 chunk biome map into a 1 chunk x 1 chunk biome map
     */
    protected BiomeType[][] cropBiomeMap(BiomeType[][] inputMap) {
        BiomeType[][] outputMap = new BiomeType[Settings.getChunkSize()][Settings.getChunkSize()];
        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                outputMap[x][z] = inputMap[x + Settings.getChunkSize()][z + Settings.getChunkSize()];
            }
        }
        return outputMap;
    }

    /**
     * Generate a map of structure markers
     */
    protected Map<Vector2f, StructureSuperType> getStructureMarkers(int chunkX, int chunkZ, BiomeType[][] biomeMap, Noise structureNoiseMap) {
        // get all biomes in region
        ArrayList<BiomeType> regionBiomes = new ArrayList<>();
        for (BiomeType[] biomeTypes : biomeMap) {
            for (BiomeType bType : biomeTypes) {
                if (!regionBiomes.contains(bType)) {
                    regionBiomes.add(bType);
                }
            }
        }

        // get all structure generation settings in region
        ArrayList<StructureGenSettings> structureGenSettings = new ArrayList<>();
        for (BiomeType biome : regionBiomes) {
            for (StructureGenSettings genSettings : biome.getStructureGenSettings()) {
                if (!structureGenSettings.contains(genSettings)) {
                    structureGenSettings.add(genSettings);
                }
            }
        }

        // generate structure markers
        Map<Vector2f, StructureSuperType> structureMarkers = new HashMap<>();
        for (StructureGenSettings genSettings : structureGenSettings) {
            Map<Vector2f, Integer> markers = structureNoiseMap.generateMarkers(genSettings.getMinimumRadius(), genSettings.getSearchRadius(), chunkX, chunkZ, 1);
            for (Vector2f markerLocation : markers.keySet()) {
                int structMapX = (int) markerLocation.x - chunkX * Settings.getChunkSize() + Settings.getChunkSize();
                int structMapZ = (int) markerLocation.y - chunkZ * Settings.getChunkSize() + Settings.getChunkSize();
                if (biomeMap[structMapX][structMapZ].getStructureGenSettings().contains(genSettings)) {
                    structureMarkers.putIfAbsent(markerLocation, genSettings.getStructureSuperType());
                }
            }
        }

        return structureMarkers;
    }

    /**
     * Create a 3 chunk x 3 chunk Vonoroi Map from Markers
     */
    protected static int[][] createVoronoi(Map<Vector2f, Integer> markers, int chunkX, int chunkZ, int p) {
        int startX = (chunkX - 1) * Settings.getChunkSize();
        int startZ = (chunkZ - 1) * Settings.getChunkSize();
        int[][] voronoi = new int[Settings.getChunkSize() * 3][Settings.getChunkSize() * 3];

        for (int x = 0; x < voronoi.length; x++) {
            for (int z = 0; z < voronoi[0].length; z++) {
                Integer nearestValue = null;
                Vector2f nearestCoord = null;
                for (Vector2f markerCoord : markers.keySet()) {
                    if (nearestCoord == null || (Maths.distance(markerCoord.x, markerCoord.y, x + startX, z + startZ, p)
                            <= Maths.distance(nearestCoord.x, nearestCoord.y, x + startX, z + startZ, p))) {
                        nearestValue = markers.get(markerCoord);
                        nearestCoord = markerCoord;

                    }
                }
                if (nearestValue != null) {
                    voronoi[x][z] = nearestValue;
                }
            }
        }
        return voronoi;
    }
}
