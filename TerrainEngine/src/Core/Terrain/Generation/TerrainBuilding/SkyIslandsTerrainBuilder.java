package Core.Terrain.Generation.TerrainBuilding;

import Core.Control.Settings;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Blocks.BlockType;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Terrain.Generation.BiomeType;
import Core.Terrain.Generation.Noise.AdvancedOpenSimplexNoise;
import Core.Utils.Logging.Log;
import org.lwjgl.util.vector.Vector3f;

import static Core.Terrain.Data.Blocks.BlockType.BLT_STONE;
import static Core.Terrain.Generation.BiomeType.BMT_PLAINS;

public class SkyIslandsTerrainBuilder extends TerrainBuilder {

    public AdvancedOpenSimplexNoise heightNoise;
    private Long seed;

    public SkyIslandsTerrainBuilder(Long seed) {
        this.seed = seed;
        initialiseNoise();
    }

    @Override
    public Block[][][] generateChunkTerrain(Chunk chunk) {

        int[][] topHeightMap = generateHeightMap(chunk.getChunkX(), chunk.getChunkZ(), 50, 55, 30);
        int[][] bottomHeightMap = generateHeightMap(chunk.getChunkX(), chunk.getChunkZ(), 20, 60, 80);


        Block[][][] chunkTerrain = new Block[Settings.getChunkSize()][Settings.getChunkHeight()][Settings.getChunkSize()];
        // generate chunk terrain using height and biome maps
        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                int bottomTerrainHeight = bottomHeightMap[x][z];
                int topTerrainHeight = topHeightMap[x][z];
//                if (heightNoise.eval((chunk.getChunkX() * Settings.getChunkSize() + x) / 200f, (chunk.getChunkZ() * Settings.getChunkSize() + z) / 200f) > 0) {
                    for (int y = Settings.getChunkHeight() - 1; y >= 0; y -= 1) {
                        if (y >= bottomTerrainHeight && y < topTerrainHeight) {
                            BlockType targetBlockType = BMT_PLAINS.getBlockLayers()[Math.min(topTerrainHeight - y - 1, BMT_PLAINS.getBlockLayers().length - 1)];
                            chunkTerrain[x][y][z] = new Block(targetBlockType, chunk, 0, new Vector3f(x, y, z));
                        }
                    }
//                }
            }
        }

        return chunkTerrain;
    }

    /**
     * Generate a 1 chunk x 1 chunk height map for the target chunk coordinates
     * based on the corresponding biome map
     */
    private int[][] generateHeightMap(int chunkX, int chunkZ, int terrainScale, int minHeight, int heightVariance) {
        int[][] heightMap = new int[Settings.getChunkSize()][Settings.getChunkSize()];

        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                int tileX = chunkX * Settings.getChunkSize() + x;
                int tileZ = chunkZ * Settings.getChunkSize() + z;
                float eval = heightNoise.eval((float) tileX / terrainScale, (float) tileZ / terrainScale) / 2f;
                heightMap[x][z] = minHeight + (int) (eval * heightVariance);
            }
        }

        return heightMap;
    }

    /**
     * Use the seed to initialise noise functions that will be used in terrain generation
     */
    private void initialiseNoise() {
        Log.info("Initialising Noise...");
        heightNoise = new AdvancedOpenSimplexNoise(seed);
        Log.info("Noise Initialised.");
    }

}
