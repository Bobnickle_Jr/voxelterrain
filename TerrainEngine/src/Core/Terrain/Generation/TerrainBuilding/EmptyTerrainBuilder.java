package Core.Terrain.Generation.TerrainBuilding;

import Core.Control.Settings;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Chunks.Chunk;

public class EmptyTerrainBuilder extends TerrainBuilder {

    @Override
    public Block[][][] generateChunkTerrain(Chunk chunk) {
        return new Block[Settings.getChunkSize()][Settings.getChunkHeight()][Settings.getChunkSize()];
    }
}
