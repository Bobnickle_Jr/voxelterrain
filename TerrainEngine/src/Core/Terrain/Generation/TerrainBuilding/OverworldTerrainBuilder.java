package Core.Terrain.Generation.TerrainBuilding;

import Core.Control.Settings;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Blocks.BlockType;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Terrain.Data.WorldType;
import Core.Terrain.Generation.BiomeType;
import Core.Terrain.Generation.ChunkGenerationData;
import Core.Terrain.Generation.Noise.AdvancedOpenSimplexNoise;
import Core.Terrain.Generation.Noise.Noise;
import Core.Terrain.Generation.Structures.*;
import Core.Terrain.Generation.TownGeneration.ITownBuilder;
import Core.Terrain.Generation.TownGeneration.JigsawTownBuilder;
import Core.Utils.Logging.Log;
import Core.Utils.Maths;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static Core.Terrain.Generation.Structures.ComplexStructureGenSettings.CSG_TOWN;

public class OverworldTerrainBuilder extends TerrainBuilder {

    public AdvancedOpenSimplexNoise biomeNoiseMap;
    public AdvancedOpenSimplexNoise structureNoiseMap;
    private Long seed;
    private ArrayList<BiomeType> biomes;
    private ITownBuilder townBuilder = new JigsawTownBuilder();

    public OverworldTerrainBuilder(Long seed, ArrayList<BiomeType> biomes) {
        this.seed = seed;
        this.biomes = biomes;
        initialiseNoise();
    }

    @Override
    public Block[][][] generateChunkTerrain(Chunk chunk) {
        ChunkGenerationData chunkGenData = getChunkGenerationData(chunk.getChunkX(), chunk.getChunkZ());
        return generateChunkTerrainFromData(chunk, chunkGenData);
    }

    /**
     * Generate the data maps needed for the target chunk to build to terrain
     */
    private ChunkGenerationData getChunkGenerationData(int chunkX, int chunkZ) {
        BiomeType[][] biomeMap = generateBiomeMap(chunkX, chunkZ);
        int[][] heightMap = generateHeightMap(chunkX, chunkZ, biomeMap);
        Map<Vector3f, StructureTypeWithRotation> structureMap = generateStructureMap(chunkX, chunkZ, biomeMap);

        biomeMap = cropBiomeMap(biomeMap);
        return new ChunkGenerationData(heightMap, biomeMap, structureMap);
    }

    /**
     * Use raw chunk generation data to construct the chunk terrain
     */
    public Block[][][] generateChunkTerrainFromData(Chunk chunk, ChunkGenerationData chunkGenData) {
        Block[][][] blocks = new Block[Settings.getChunkSize()][Settings.getChunkHeight()][Settings.getChunkSize()];

        // generate chunk terrain using height and biome maps
        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                BiomeType biomeType = chunkGenData.getBiomeMap()[x][z];
                int terrainHeight = chunkGenData.getHeightMap()[x][z];
                if (terrainHeight > Settings.getSnowLevel()) {
                    blocks[x][terrainHeight + 1][z] = new Block(BlockType.BLT_SNOW, chunk, 0, new Vector3f(x, terrainHeight + 1, z));
                }
                for (int y = Settings.getChunkHeight(); y >= 0; y -= 1) {
                    if (y <= terrainHeight) {
                        BlockType targetBlockType = biomeType.getBlockLayers()[Math.min(terrainHeight - y, biomeType.getBlockLayers().length - 1)];
                        blocks[x][y][z] = new Block(targetBlockType, chunk, 0, new Vector3f(x, y, z));
                    } else if (y <= Settings.getWaterLevel()) {
                        blocks[x][y][z] = new Block(BlockType.BLT_WATER, chunk, 0, new Vector3f(x, y, z));
                    }
                }
            }
        }

        // generate structures using structure map
        for (Vector3f structureWorldCoord : chunkGenData.getStructureMap().keySet()) {
            StructureType structureType = chunkGenData.getStructureMap().get(structureWorldCoord).type;
            int structureRotation = chunkGenData.getStructureMap().get(structureWorldCoord).rotation;
            Map<Vector3f, BlockType> blocksInThisChunk = structureType.getBlocksInChunk(structureWorldCoord, chunk.getChunkX(), chunk.getChunkZ(), seed + (long) structureWorldCoord.length(), structureRotation);
            // build structure from template
            for (Vector3f blockLocation : blocksInThisChunk.keySet()) {
                BlockType blockType = blocksInThisChunk.get(blockLocation);
                if (structureType.isSmoothToLandscape()) {
                    blockLocation.y -= blockLocation.y - chunkGenData.getHeightMap()[(int) blockLocation.x][(int) blockLocation.z];
                }
                Block existingBlock = blocks[(int) blockLocation.x][(int) blockLocation.y][(int) blockLocation.z];
                if ((existingBlock == null || existingBlock.getType().getPriority() < blockType.getPriority())) {
                    blocks[(int) blockLocation.x][(int) blockLocation.y][(int) blockLocation.z] = new Block(blockType, chunk, 0, blockLocation);
                }
            }
        }
        return blocks;
    }

    /**
     * Use the seed to initialise noise functions that will be used in terrain generation
     */
    private void initialiseNoise() {
        Log.info("Initialising Noise...");
        biomeNoiseMap = new AdvancedOpenSimplexNoise(seed);
        structureNoiseMap = new AdvancedOpenSimplexNoise(seed + 1);
        Log.info("Noise Initialised.");
    }

    /**
     * Generate a 3 chunk x 3 chunk biome map centered on the target chunk coordinates
     */
    private BiomeType[][] generateBiomeMap(int chunkX, int chunkZ) {
        BiomeType[][] biomeMap = new BiomeType[Settings.getChunkSize() * 3][Settings.getChunkSize() * 3];

        // create markers
        int biomeScale = Settings.getBiomeScale();
        Map<Vector2f, Integer> biomeMarkers = biomeNoiseMap.generateMarkers(5, 30, chunkX / biomeScale, chunkZ / biomeScale, biomes.size());
        for (Vector2f markerCoord : biomeMarkers.keySet()) {
            markerCoord.x *= biomeScale;
            markerCoord.y *= biomeScale;
        }

        // generate Voronoi from markers
        int[][] regions = createVoronoi(biomeMarkers, chunkX, chunkZ, 10);
        BiomeType[][] rivers = generateRiverMap(biomeNoiseMap, chunkX, chunkZ);

        for (int x = 0; x < biomeMap.length; x += 1) {
            for (int z = 0; z < biomeMap[0].length; z += 1) {
                if (rivers[x][z] != null) {
                    biomeMap[x][z] = rivers[x][z];
                } else {
                    biomeMap[x][z] = biomes.get(regions[x][z]);
                }
            }
        }

        return biomeMap;
    }

    /**
     * Generate a 3 chunk x 3 river map centered on the target chunk coordinates
     */
    private BiomeType[][] generateRiverMap(Noise noise, int chunkX, int chunkZ) {
        BiomeType[][] riverMap = new BiomeType[Settings.getChunkSize() * 3][Settings.getChunkSize() * 3];
        float riverScale = Settings.getRiverScale();
        for (int x = 0; x < Settings.getChunkSize() * 3; x += 1) {
            for (int z = 0; z < Settings.getChunkSize() * 3; z += 1) {
                float value = noise.eval((x + chunkX * Settings.getChunkSize()) / riverScale, (z + chunkZ * Settings.getChunkSize()) / riverScale);
                if (value > -Settings.getRiverWidth() && value < Settings.getRiverWidth()) {
                    riverMap[x][z] = BiomeType.BMT_RIVER;
                }
            }
        }

        return riverMap;
    }

    /**
     * Generate a 1 chunk x 1 chunk height map for the target chunk coordinates
     * based on the corresponding biome map
     */
    private int[][] generateHeightMap(int chunkX, int chunkZ, BiomeType[][] biomeMap) {
        int[][] heightMap = new int[Settings.getChunkSize()][Settings.getChunkSize()];

        for (int x = 0; x < Settings.getChunkSize(); x += 1) {
            for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                int tileX = chunkX * Settings.getChunkSize() + x;
                int tileZ = chunkZ * Settings.getChunkSize() + z;
                heightMap[x][z] = calculateBlendedTileHeight(tileX, tileZ, x + Settings.getChunkSize(), z + Settings.getChunkSize(), biomeMap);
            }
        }

        return heightMap;
    }

    /**
     * Calculate the height of the target tile after blending with nearby biomes
     */
    private int calculateBlendedTileHeight(int tileX, int tileZ, int xOnMap, int zOnMap, BiomeType[][] biomeMap) {
        int thisHeight = calculateTileHeight(tileX, tileZ, biomeMap[xOnMap][zOnMap]);

        BiomeType thisBiome = biomeMap[xOnMap][zOnMap];
        Float borderDistance = null;
        Integer nearestOtherBiomeHeight = null;
        int biomeBlend = Settings.getDefaultBiomeBlendFactor();
        for (int x = -biomeBlend; x < biomeBlend; x += 1) {
            for (int z = -biomeBlend; z < biomeBlend; z += 1) {
                try {
                    if (thisBiome != biomeMap[xOnMap + x][zOnMap + z] && (borderDistance == null || borderDistance > Maths.distance(x, z, 0, 0, 3))) {
                        borderDistance = Maths.distance(x, z, 0, 0, 3);
                        nearestOtherBiomeHeight = calculateTileHeight(tileX + x, tileZ + z, biomeMap[xOnMap + x][zOnMap + z]);
                    }
                } catch (Exception ignored) {}
            }
        }
        if (borderDistance != null && nearestOtherBiomeHeight != null) {
            int delta = thisHeight - nearestOtherBiomeHeight;
            if (delta < 0) {
                thisHeight -= delta / (10 * borderDistance); // going up
            } else {
                thisHeight -= delta / (1.25 * borderDistance); // going down
            }
        }
        return thisHeight;
    }

    /**
     * Calculate the height of the target tile based on a given biome type
     */
    private int calculateTileHeight(int tileX, int tileZ, BiomeType biomeType) {
        int terrainScale = biomeType.getTerrainScale();
        float eval = biomeNoiseMap.eval((float) tileX / terrainScale, (float) tileZ / terrainScale) / 2f;
        return biomeType.getMinHeight() + (int) (eval * biomeType.getHeightVariance());
    }

    /**
     * Generate a map of structure types and their suggested world coordinates
     */
    private Map<Vector3f, StructureTypeWithRotation> generateStructureMap(int chunkX, int chunkZ, BiomeType[][] biomeMap) {
        Map<Vector3f, StructureTypeWithRotation> structureMap = new HashMap<>();

        // generate simple structures
        Map<Vector2f, StructureSuperType> simpleStructureMarkers;
        simpleStructureMarkers = getStructureMarkers(chunkX, chunkZ, biomeMap, structureNoiseMap);
        // build map from markers
        for (Vector2f structureXZ : simpleStructureMarkers.keySet()) {
            StructureSuperType structureType = simpleStructureMarkers.get(structureXZ);
            int structureX = (int) structureXZ.x;
            int structureZ = (int) structureXZ.y; // this is not wrong, it's just strange
            int structMapX = structureX - chunkX * Settings.getChunkSize() + Settings.getChunkSize();
            int structMapZ = structureZ - chunkZ * Settings.getChunkSize() + Settings.getChunkSize();
            int structureY = calculateBlendedTileHeight(structureX, structureZ, structMapX, structMapZ, biomeMap) + 1;
            if (structureY > structureType.getMinSpawnHeight()) {
                Vector3f structureLocation = new Vector3f(structureX, structureY, structureZ);
                StructureTypeWithRotation structureTypeWithRotation = new StructureTypeWithRotation(structureType.getRandomChild(seed * (long) structureLocation.length()), 0);
                structureMap.putIfAbsent(structureLocation, structureTypeWithRotation);
            }
        }
        // generate complex structures
        Map<Vector2f, StructureSuperTypeWithRotation> complexStructureMarkers = genComplexStructureMarkers(chunkX, chunkZ, biomeMap);
        // build map from markers
        for (Vector2f structureXZ : complexStructureMarkers.keySet()) {
            StructureSuperType structureType = complexStructureMarkers.get(structureXZ).superType;
            int structureX = (int) structureXZ.x;
            int structureZ = (int) structureXZ.y; // this is not wrong, it's just strange
            int structMapX = structureX - chunkX * Settings.getChunkSize() + Settings.getChunkSize();
            int structMapZ = structureZ - chunkZ * Settings.getChunkSize() + Settings.getChunkSize();
            int structureY = calculateBlendedTileHeight(structureX, structureZ, structMapX, structMapZ, biomeMap) + 1;
            if (structureY > structureType.getMinSpawnHeight()) {
                Vector3f structureLocation = new Vector3f(structureX, structureY, structureZ);
                StructureTypeWithRotation structureTypeWithRotation = new StructureTypeWithRotation(structureType.getRandomChild(seed * (long) structureLocation.length()), complexStructureMarkers.get(structureXZ).rotation);
                structureMap.putIfAbsent(structureLocation, structureTypeWithRotation);
            }
        }

        return structureMap;
    }

    /**
     * Generate a map of complex structure markers
     */
    private Map<Vector2f, StructureSuperTypeWithRotation> genComplexStructureMarkers(int chunkX, int chunkZ, BiomeType[][] biomeMap) {
        // get all biomes in region
        ArrayList<BiomeType> regionBiomes = new ArrayList<>();
        for (BiomeType[] biomeTypes : biomeMap) {
            for (BiomeType bType : biomeTypes) {
                if (!regionBiomes.contains(bType)) {
                    regionBiomes.add(bType);
                }
            }
        }

        // get all complex structure generation settings in region
        ArrayList<ComplexStructureGenSettings> structureGenSettings = new ArrayList<>();
        for (BiomeType biome : regionBiomes) {
            for (ComplexStructureGenSettings genSettings : biome.getComplexStructureGenSettings()) {
                if (!structureGenSettings.contains(genSettings)) {
                    structureGenSettings.add(genSettings);
                }
            }
        }

        // generate complex structure markers
        Map<Vector2f, StructureSuperTypeWithRotation> structureMarkers = new HashMap<>();
        for (ComplexStructureGenSettings genSettings : structureGenSettings) {
            Map<Vector2f, Integer> markers = structureNoiseMap.generateMarkers(10, 10, chunkX / genSettings.getSeparationScale(), chunkZ / genSettings.getSeparationScale(), 1);
            for (Vector2f markerLocation : markers.keySet()) {
                markerLocation.x *= genSettings.getSeparationScale();
                markerLocation.y *= genSettings.getSeparationScale();
                if (getBiomeOfTile((int) markerLocation.x, (int) markerLocation.y).getComplexStructureGenSettings().contains(genSettings)) {
                    structureMarkers.putAll(townBuilder.generateTown(seed + (long) markerLocation.x + (long) markerLocation.y, markerLocation));
                }
            }
        }

        // remove out of bounds markers
        HashMap<Vector2f, StructureSuperTypeWithRotation> allowedStructureMarkers = new HashMap<>();
        for (Vector2f location : structureMarkers.keySet()) {
            int structMapX = (int) location.x - chunkX * Settings.getChunkSize() + Settings.getChunkSize();
            int structMapZ = (int) location.y - chunkZ * Settings.getChunkSize() + Settings.getChunkSize();
            if (structMapX >= 0 && structMapX < biomeMap.length && structMapZ >= 0 && structMapZ < biomeMap.length) {
                BiomeType biomeType = biomeMap[structMapX][structMapZ];
                if (biomeType.getComplexStructureGenSettings().contains(CSG_TOWN)) {
                    allowedStructureMarkers.put(location, structureMarkers.get(location));
                }
            }
        }

        return allowedStructureMarkers;
    }

    /**
     * Calculate the biome of target tile
     */
    private BiomeType getBiomeOfTile(int x, int z) {
        int chunkX = Core.Utils.Maths.worldCoordToChunkCoord(x);
        int chunkZ = Core.Utils.Maths.worldCoordToChunkCoord(z);
        BiomeType[][] biomeMap = generateBiomeMap(chunkX, chunkZ);
        int xInChunk = Maths.worldCoordToCoordInChunk(x);
        int zInChunk = Maths.worldCoordToCoordInChunk(z);
        return biomeMap[xInChunk + Settings.getChunkSize()][zInChunk + Settings.getChunkSize()];
    }


}
