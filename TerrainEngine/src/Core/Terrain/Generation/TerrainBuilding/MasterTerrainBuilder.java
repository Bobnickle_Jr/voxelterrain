package Core.Terrain.Generation.TerrainBuilding;

import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Chunks.Chunk;
import Core.Terrain.Data.WorldType;
import Core.Terrain.Generation.BiomeType;
import Core.Utils.Logging.Log;

import java.util.ArrayList;

public class MasterTerrainBuilder extends TerrainBuilder {

    private WorldType worldType;
    private TerrainBuilder overworldTerrainBuilder;
    private TerrainBuilder flatTerrainBuilder;
    private TerrainBuilder emptyTerrainBuilder;
    //private TerrainBuilder skyIslandsTerrainBuilder; // WIP: 3D chunks
    //private TerrainBuilder undergroundTerrainBuilder; // WIP: 3d chunks

    public MasterTerrainBuilder(Long seed, ArrayList<BiomeType> biomes, WorldType worldType) {
        this.worldType = worldType;

        this.overworldTerrainBuilder = new OverworldTerrainBuilder(seed, biomes);
        this.flatTerrainBuilder = new FlatTerrainBuilder(seed);
        this.emptyTerrainBuilder = new EmptyTerrainBuilder();
        //this.skyIslandsTerrainBuilder = new SkyIslandsTerrainBuilder(seed);
        //this.undergroundTerrainBuilder = new UndergroundTerrainBuilder();
    }

    @Override
    public Block[][][] generateChunkTerrain(Chunk chunk) {
        TerrainBuilder terrainBuilder = assignTerrainBuilder(chunk);
        return terrainBuilder.generateChunkTerrain(chunk);
    }

    private TerrainBuilder assignTerrainBuilder(Chunk chunk) {
        // TODO: when implementing 3D chunks, take into account chunk.getPosition().y
        switch (worldType) {
            case WT_FLAT:
                return flatTerrainBuilder;
            case WT_STANDARD:
                return overworldTerrainBuilder;
            default:
                Log.error("Unknown world type: " + worldType + " - Could not load valid terrain builder");
                return emptyTerrainBuilder;
        }
    }
}
