package Core.Terrain.Generation;

import Core.Terrain.Generation.Structures.StructureTypeWithRotation;
import org.lwjgl.util.vector.Vector3f;

import java.util.Map;

public class ChunkGenerationData {

    private int[][] heightMap;
    private BiomeType[][] biomeMap;
    private Map<Vector3f, StructureTypeWithRotation> structureMap;

    public ChunkGenerationData(int[][] heightMap, BiomeType[][] biomeMap, Map<Vector3f, StructureTypeWithRotation> structureMap) {
        this.heightMap = heightMap;
        this.biomeMap = biomeMap;
        this.structureMap = structureMap;
    }

    // getters and setters

    public int[][] getHeightMap() {
        return heightMap;
    }

    public BiomeType[][] getBiomeMap() {
        return biomeMap;
    }

    public Map<Vector3f, StructureTypeWithRotation> getStructureMap() {
        return structureMap;
    }
}
