package Core.Terrain.CollisionMesh;

import Core.Utils.Maths;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.Arrays;

public class CollisionMesh {

    private ArrayList<Vector3f> nodes = new ArrayList<>();

    /**
     * Generate a single collision mesh by combining a list of collision meshes and their respective locations
     */
    public static CollisionMesh mergeMeshes(ArrayList<CollisionMesh> collisionMeshes, ArrayList<Vector3f> locations) {
        CollisionMesh collisionMesh = new CollisionMesh();
        for (int i = 0; i < collisionMeshes.size(); i += 1) {
            ArrayList<Vector3f> nodes = collisionMeshes.get(i).getNodes();
            for (Vector3f node : nodes) {
                collisionMesh.addNode(Vector3f.add(node, locations.get(i), null));
            }
        }
        return collisionMesh;
    }

    /**
     * Check if a given ray intercepts the collision mesh
     */
    public Vector3f getInterceptWithRay(Vector3f ray, Vector3f rayOrigin, Vector3f meshLocation) {
        Vector3f nearestIntersect = null;
        if (nodes == null) {
            return null;
        }
        for (int i = 0; i < nodes.size(); i += 3) {
            // i, i + 1, i + 2 form a face
            Vector3f intersect = Maths.rayIntersectsTriangle(ray, rayOrigin, Vector3f.add(nodes.get(i), meshLocation, null), Vector3f.add(nodes.get(i + 1), meshLocation, null), Vector3f.add(nodes.get(i + 2), meshLocation, null));
            if (intersect != null && (nearestIntersect == null || Maths.distance(intersect, rayOrigin, 2) < Maths.distance(nearestIntersect, rayOrigin, 2))) {
                nearestIntersect = intersect;
            }
        }

        return nearestIntersect;
    }

    /**
     * Add a single node to the mesh
     */
    private void addNode(Vector3f newNode) {
        nodes.add(newNode);
    }

    /**
     * Add an array of nodes to the mesh
     */
    public void addNodes(Vector3f[] newNodes) {
        nodes.addAll(Arrays.asList(newNodes));
    }

    // getters and setters

    public ArrayList<Vector3f> getNodes() {
        return nodes;
    }
}
