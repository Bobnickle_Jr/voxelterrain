package Core.Terrain.CollisionMesh;

import org.lwjgl.util.vector.Vector3f;

public enum CollisionMeshPredef {

    CMP_FULL_BLOCK_UP(new Vector3f(0, 1, 0), new Vector3f(0, 1, 1), new Vector3f(1, 1, 0),
                      new Vector3f(1, 1, 1), new Vector3f(0, 1, 1), new Vector3f(1, 1, 0)),

    CMP_FULL_BLOCK_DOWN(new Vector3f(0, 0, 0), new Vector3f(0, 0, 1), new Vector3f(1, 0, 0),
                        new Vector3f(1, 0, 1), new Vector3f(0, 0, 1), new Vector3f(1, 0, 0)),

    CMP_FULL_BLOCK_LEFT(new Vector3f(0, 0, 0), new Vector3f(0, 0, 1), new Vector3f(0, 1, 0),
                        new Vector3f(0, 1, 1), new Vector3f(0, 0, 1), new Vector3f(0, 1, 0)),

    CMP_FULL_BLOCK_RIGHT(new Vector3f(1, 0, 0), new Vector3f(1, 0, 1), new Vector3f(1, 1, 0),
                         new Vector3f(1, 1, 1), new Vector3f(1, 0, 1), new Vector3f(1, 1, 0)),

    CMP_FULL_BLOCK_BACK(new Vector3f(0, 0, 1), new Vector3f(1, 0, 1), new Vector3f(0, 1, 1),
                        new Vector3f(1, 1, 1), new Vector3f(1, 0, 1), new Vector3f(0, 1, 1)),

    CMP_FULL_BLOCK_FRONT(new Vector3f(0, 0, 0), new Vector3f(1, 0, 0), new Vector3f(0, 1, 0),
                         new Vector3f(1, 1, 0), new Vector3f(1, 0, 0), new Vector3f(0, 1, 0)),

    ;

    private final Vector3f[] nodes;

    CollisionMeshPredef(Vector3f... nodes) {
        this.nodes = nodes;
    }

    // getters and setters

    public Vector3f[] getNodes() {
        return nodes;
    }

}
