package Core.Terrain;

import Core.Terrain.Data.Chunks.Chunk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to store and manage a set of chunks
 */
public class ChunkCollection {

    private final Map<Integer, Map<Integer, Chunk>> chunks = Collections.synchronizedMap(new HashMap<>());

    /**
     * Get chunk from collection based on chunk coords
     */
    public Chunk get(int chunkX, int chunkZ) {
        Map<Integer, Chunk> xChunks = chunks.get(chunkX);
        if (xChunks == null) {
            return null;
        }
        return xChunks.get(chunkZ);
    }

    /**
     * Get all chunks stored in the collection
     */
    public ArrayList<Chunk> getAll() {
        ArrayList<Chunk> allChunks = new ArrayList<>();
        synchronized(chunks) {
            for (Map<Integer, Chunk> xChunks : chunks.values()) {
                allChunks.addAll(xChunks.values());
            }
        }
        return allChunks;
    }

    /**
     * Get all chunks in the collection that are within a given chunk distance of a coordinate
     */
    public ArrayList<Chunk> getWithinRange(int centerX, int centerZ, int radius) {
        ArrayList<Chunk> foundChunks = new ArrayList<>();
        for (int x = centerX - radius; x <= centerX + radius; x += 1) {
            for (int z = centerZ - radius; z <= centerZ + radius; z += 1) {
                Map<Integer, Chunk> xChunks = chunks.get(x);
                if (xChunks != null) {
                    Chunk chunk = xChunks.get(z);
                    if (chunk != null) {
                        foundChunks.add(chunk);
                    }
                }
            }
        }
        return foundChunks;
    }

    /**
     * Add a chunk to the collection
     */
    public void put(Chunk chunk) {
        Map<Integer, Chunk> xChunks = chunks.computeIfAbsent(chunk.getChunkX(), k -> Collections.synchronizedMap(new HashMap<>()));
        xChunks.put(chunk.getChunkZ(), chunk);
        chunks.put(chunk.getChunkX(), xChunks);
    }
    /**
     * Remove a chunk from the collection
     */
    public void remove(Chunk chunk) {
        chunks.get(chunk.getChunkX()).remove(chunk.getChunkZ());
    }
}
