package Core.Control;

import Client.GameViews.GameView;
import Client.GameViews.ViewManager;
import Client.Gui.Console.Console;
import Client.Gui.GuiOverlay;
import Client.Gui.GuiUtils;
import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Client.Gui.Widgets.*;
import Client.Rendering.DisplayManager;
import Client.Rendering.GUIs.GuiRenderer;
import Client.Rendering.GUIs.fontRendering.TextAlign;
import Client.Rendering.Loader;
import Client.Rendering.MasterRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Entities.Camera;
import Core.Entities.Entity;
import Core.Entities.Light;
import Core.Entities.Player;
import Core.Terrain.Data.WorldType;
import Core.Terrain.TerrainManager;
import Core.Utils.Logging.Log;
import Core.Utils.Maths;
import Core.Utils.MousePicker;
import Core.Utils.Saving.SaveFileUtils;
import Core.Utils.Saving.WorldMetaData;
import Core.Utils.StringUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;

import static Core.Utils.ControlSchemes.ControlSchemeButton.*;

public class GameWorld extends GameView {

    private Player player;
    private WorldMetaData metaData;
    private Console console;
    private Loader loader;

    private ArrayList<Light> lights = new ArrayList<>();
    private Light sun;

    private boolean commandMenuOpen = false;
    private GuiWidget pauseMenu;
    private GuiWidget settingsMenu;
    private GuiWidget hud;
    private GuiWidget hotbar;

    public GameWorld(WorldMetaData metaData, Loader loader, ViewManager viewManager) {
        super(viewManager, false);
        this.metaData = metaData;
        this.player = new Player(this, "Player", loader);
        TerrainManager.init(this, loader, metaData.getWorldType());
        this.console = new Console(this);
        this.loader = loader;
        setUpGui();
    }

    @Override
    protected void setUpGui() {
        guiOverlay = new GuiOverlay();

        // pause menu
        pauseMenu = guiOverlay.addChild(new GuiWidgetBox());
        pauseMenu.addChild(new GuiImage(new Vector2f(0, 0), GuiTexturePredef.GTP_GRAY_OUT, 20));
        pauseMenu.addChild(new GuiImage(new Vector2f(0, 0.0f), GuiTexturePredef.GTP_400x400_BROWN, 1));
        pauseMenu.addChild(new GuiLabel(new Vector2f(0, 0.45f), "Paused", null, 2, TextAlign.TA_CENTERED));
        pauseMenu.addChild(new GuiButton(new Vector2f(0, -0.20f), "Resume", GuiTexturePredef.GTP_256x32_BROWN));
        pauseMenu.addChild(new GuiButton(new Vector2f(0, -0.30f), "Settings", GuiTexturePredef.GTP_256x32_BROWN));
        pauseMenu.addChild(new GuiButton(new Vector2f(0, -0.45f), "Save And Exit", GuiTexturePredef.GTP_256x32_BROWN));
        pauseMenu.setHidden(true);

        // settings menu
        settingsMenu = guiOverlay.addChild(new GuiSettingsWidget());
        settingsMenu.pushChild(new GuiImage(new Vector2f(0, 0), GuiTexturePredef.GTP_GRAY_OUT, 20));
        settingsMenu.setHidden(true);

        // hud
        hud = guiOverlay.addChild(new GuiWidgetBox());
        hotbar = hud.addChild(new GuiWidgetBox(new Vector2f(0, -0.92f)));
        for (int i = -5; i < 5; i += 1) {
            hotbar.addChild(new GuiInventorySlot(new Vector2f((i + 0.5f) * 0.08f, 0), 1, player.getInventory(), i + 5));
        }

    }

    /**
     * Load game world from save file
     */
    public static GameWorld loadGameWorld(String fileName, Loader loader, ViewManager viewManager) {
        Log.info("Loading World: '" + fileName + "'.");

        // load meta data
        WorldMetaData metaData = SaveFileUtils.getMetaData(fileName);
        if (metaData != null) {
            return new GameWorld(metaData, loader, viewManager);
        } else {
            return null;
        }
    }

    /**
     * Create new world and save data to file
     */
    public static GameWorld createGameWorld(String seed, String fileName, Loader loader, ViewManager viewManager, WorldType worldType) {
        Log.info("Creating New " + worldType + " World: '" + fileName + "' From Seed: '" + seed + "'.");

        // create save file
        if (StringUtils.isNullOrEmptyOrWhiteSpace(fileName)) {
            fileName = "NewWorld";
        }
        fileName = SaveFileUtils.createSaveFile(fileName);

        // convert seed
        long lSeed = Maths.calculateLongSeed(seed);

        // save meta data
        WorldMetaData metaData = new WorldMetaData(fileName, lSeed, seed, worldType);
        SaveFileUtils.createMetaDataFile(metaData);

        return new GameWorld(metaData, loader, viewManager);
    }

    /**
     * Initialise the game world
     */
    public void init(Camera camera) {
        // setup camera
        camera.setTargetEntity(player);

        // setup lights
        float change = 0f;//.75f;
        sun = new Light(new Vector3f(-25000, 100000, -50000), new Vector3f(0.75f + change, 0.75f + change, 0.75f + change), new Vector3f(0.5f, 0f, 0f));
        lights.add(sun);

        // setup misc
        Mouse.setCursorPosition(Display.getWidth() / 2, Display.getHeight() / 2);
    }

    /**
     * update the world
     */
    @Override
    public void update(MasterRenderer renderer, GuiRenderer guiRenderer, Camera camera, Loader loader, MousePicker picker, WaterFrameBuffers fbos) {
        super.update(renderer, guiRenderer, camera, loader, picker, fbos);

        // process game inputs
        processInputs();

        if (gameCanRun()) {
            // movement
            player.doMovement();
            camera.move();
            picker.update();

            // update entities
            player.update(picker);
        }

        // draw everything
        drawDebugOverlay(picker);

        ArrayList<Entity> solidEntities = gatherAllSolidEntities();
        ArrayList<Entity> liquidEntities = gatherAllLiquidEntities();

        renderer.renderScene(solidEntities, liquidEntities, sun, lights, camera, fbos);
    }

    /**
     * Process Close and Save
     */
    public void closeAndSave() {
        Log.info("Saving Meta Data...");
        SaveFileUtils.createMetaDataFile(metaData);
        Log.info("Saving Player...");
        player.saveToFile();
        Log.info("Saving Terrain...");
        TerrainManager.setInitialised(false);
        TerrainManager.unloadAllChunks();
        Log.info("Terrain Saved.");
    }

    /**
     * Draw debug overlay to the game display
     */
    public void drawDebugOverlay(MousePicker picker) {
        if (Settings.isDrawDebugOverlay()) {
            GuiUtils.drawTextToDebugOverlay(Settings.getAppTitle() + " - " + Settings.getAppVersion());
            GuiUtils.drawTextToDebugOverlay("FPS: " + 1 / DisplayManager.getFrameTimeSeconds() + " (Cap: " + Settings.getFpsCap() + ")");
            GuiUtils.drawTextToDebugOverlay("Average Render Time: " + MasterRenderer.getAverageRenderTimeMilliseconds());
            GuiUtils.drawTextToDebugOverlay("Average Generation Time: " + TerrainManager.getAverageGenerationTime());

            GuiUtils.drawTextToDebugOverlay("");
            GuiUtils.drawTextToDebugOverlay("Player:");
            GuiUtils.drawTextToDebugOverlay("Name: " + player.getName());
            GuiUtils.drawTextToDebugOverlay("Location: " + StringUtils.coordString(player.getPosition()) + " " + player.getChunkCoordString());
            GuiUtils.drawTextToDebugOverlay("Looking: " + player.getRotY() + " " + player.getLookPitch());

            GuiUtils.drawTextToDebugOverlay("Selected Item: " + player.getInventory().getSelectedSlot());
            GuiUtils.drawTextToDebugOverlay("");
            GuiUtils.drawTextToDebugOverlay("Rendering:");
            GuiUtils.drawTextToDebugOverlay("Render Distance: " + Settings.getRenderDistance());
            GuiUtils.drawTextToDebugOverlay("Loaded Chunks: " + TerrainManager.getLoadedChunks().getAll().size());
            GuiUtils.drawTextToDebugOverlay("Shadows Enabled: " + Settings.isEnableShadows());
            GuiUtils.drawTextToDebugOverlay("");
            GuiUtils.drawTextToDebugOverlay("Memory:");
            GuiUtils.drawTextToDebugOverlay("VAOs: " + loader.getVaos().size());
            GuiUtils.drawTextToDebugOverlay("VBOs: " + loader.getVbos().size());
            GuiUtils.drawTextToDebugOverlay("Textures: " + loader.getTextures().size());
            GuiUtils.drawTextToDebugOverlay("");
            GuiUtils.drawTextToDebugOverlay("Looking At:");
            GuiUtils.drawTextToDebugOverlay("Solid: " + (picker.getTargetSolidBlock() == null ? "Unknown" :
                    picker.getTargetSolidBlock().getType().getName() + ": " + StringUtils.coordString(picker.getTargetSolidBlock().getPosition()) + " " + StringUtils.coordString(picker.getTargetSolidBlock().getChunkCoords())));
            GuiUtils.drawTextToDebugOverlay("");
            GuiUtils.drawTextToDebugOverlay("Misc:");
            GuiUtils.drawTextToDebugOverlay("Can Run: " + gameCanRun());
            GuiUtils.drawTextToDebugOverlay("Paused: " + !pauseMenu.isHidden());
            GuiUtils.drawTextToDebugOverlay("Console: " + commandMenuOpen);
        }

    }

    @Override
    protected void closeView() {
        closeAndSave();
    }

    /**
     * Returns a list of all solid entities in the game world
     */
    public ArrayList<Entity> gatherAllSolidEntities() {
        return new ArrayList<>(TerrainManager.getAllSolidEntities());
    }

    /**
     * Returns a list of all liquid entities in the game world
     */
    public ArrayList<Entity> gatherAllLiquidEntities() {
        return new ArrayList<>(TerrainManager.getAllLiquidEntities());
    }

    /**
     * Determines if the game should update as usual, or function as if paused
     */
    public boolean gameCanRun() {
        return pauseMenu.isHidden() && settingsMenu.isHidden() && !commandMenuOpen;
    }

    /**
     * Process inputs
     */
    public void processInputs() {
        if (Settings.getControlScheme().buttonPressed(CSB_TOGGLE_DEBUG)) {
            Settings.toggleDrawDebugOverlay();
        }

        processPauseMenu();
        processSettingsMenu();
        processCommandMenu();
    }

    /**
     * Process command menu
     */
    public void processCommandMenu() {
        if (commandMenuOpen) {
            console.drawToScreen();
            if (console.processKeyboardInput(player.getName())) {
                commandMenuOpen = false;
            }
            if (Settings.getControlScheme().buttonReleased(CSB_CONSOLE) || Settings.getControlScheme().buttonReleased(CSB_PAUSE)) {
                commandMenuOpen = false;
            }
        } else if (gameCanRun()) {
            console.drawRecentToScreen();
            if (Settings.getControlScheme().buttonReleased(CSB_CONSOLE) || Settings.getControlScheme().buttonReleased(CSB_OPEN_CHAT) ) {
                commandMenuOpen = true;
                console.clearCurrentString();
            }
        }
        console.flushKeyboard();
    }

    /**
     * Process pause menu
     */
    public void processPauseMenu() {
        if (!pauseMenu.isHidden()) {
            // draw pause screen and process pause menu inputs
            GuiWidget pressedButton = pauseMenu.getPressedChild();
            if (pressedButton != null) {
                switch (pressedButton.getText()) {
                    case "Resume":
                        pauseMenu.setHidden(true);
                        break;
                    case "Settings":
                        pauseMenu.setHidden(true);
                        settingsMenu.setHidden(false);
                        break;
                    case "Save And Exit":
                        viewManager.popView();
                        break;
                }
            }

            if (Settings.getControlScheme().buttonReleased(CSB_PAUSE)) {
                pauseMenu.setHidden(true);
                Mouse.setCursorPosition(Settings.getDisplayWidth() / 2, Settings.getDisplayHeight() / 2);
            }
        } else if (gameCanRun()) {
            if (Settings.getControlScheme().buttonReleased(CSB_PAUSE)) {
                pauseMenu.setHidden(false);
            }
        }
    }

    /**
     * Process settings menu
     */
    public void processSettingsMenu() {
        if (!settingsMenu.isHidden()) {
            // draw pause screen and process pause menu inputs
            GuiWidget pressedButton = settingsMenu.getPressedChild();
            if (pressedButton != null) {
                if ("Back".equals(pressedButton.getName())) {
                    settingsMenu.setHidden(true);
                    pauseMenu.setHidden(false);
                }
            }

            if (Settings.getControlScheme().buttonReleased(CSB_PAUSE)) {
                settingsMenu.setHidden(true);
                Mouse.setCursorPosition(Settings.getDisplayWidth() / 2, Settings.getDisplayHeight() / 2);
            }
        }
    }

    // getters and setters

    public Player getPlayer() {
        return player;
    }

    public WorldMetaData getMetaData() {
        return metaData;
    }

}
