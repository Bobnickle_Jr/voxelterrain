package Core.Control;

import Core.Utils.ControlSchemes.ControlScheme;
import Core.Utils.ControlSchemes.KeyboardControlScheme;

public final class Settings {

    /** app meta data **/
    private static final String appTitle = "Voxel Terrain";
    private static final String appVersion = "Alpha";
    private static final String copyrightOwner = "Nathan Challinor";
    private static final String copyrightNotice = "Copyright " + copyrightOwner + " - All Rights Reserved - Do Not Distribute";

    /** debug settings **/
    private static boolean drawDebugOverlay = false;

    /** display settings **/
    private static final int defaultDisplayWidth = 1280;
    private static final int defaultDisplayHeight = 720;
    private static final int displayWidth = 1280;
    private static final int displayHeight = 720;
    private static final int fpsCap = 120;
    private static boolean exitRequested = false;

    /** resource locations **/
    private final static String structureFileLocation = "./res/structures";
    private final static String modelFileLocation = "./res/models";
    private final static String textureFileLocation = "./res/textures";
    private final static String shaderFileLocation = "./res/shaders";
    private final static String fontFileLocation = "./res/fonts";

    /** save file settings **/
    private static String saveFileLocation = "./savefiles";

    /** sprite sheet settings **/
    private final static float terrainSpriteSheetSize = 128;
    private final static float terrainSpriteSheetTileSize = 20;

    /** control settings **/
    private static ControlScheme controlScheme = new KeyboardControlScheme();
    private static boolean invertHotbarScroll = true;

    /** rendering settings **/
    private static Byte[] renderDistanceOptions = new Byte[] {5, 10, 15, 20};
    private static byte renderDistanceIndex = 2;
    private static final int shadowMapSize = 8192;
    private static boolean enableWaterDistortions = true;
    private static boolean enableWaterReflections = true;
    private static boolean enableShadows = true;

    /** generation settings **/
    private static final byte defaultBiomeBlendFactor = 10;
    private static final byte biomeScale = 10;
    private static final int riverScale = 500;
    private static final float riverWidth = 0.025f;
    private static final byte snowLevel = 45;
    private static final byte waterLevel = 16;

    /** chunk and block settings **/
    private static final int blockSize = 2;
    private static final int chunkSize = 16;
    private static final int chunkHeight = 128;

    /** POV settings **/
    public static final float FOV = 70;
    public static final float NEAR_PLANE = 0.1f;
    public static final float FAR_PLANE = 1000;

    /** font settings **/
    public static final String selectedFont = "candara";// "Bahnschrift_DF";

    private Settings() {}

    // getters and setters

    public static String getStructureFileLocation() {
        return structureFileLocation;
    }

    public static String getModelFileLocation() {
        return modelFileLocation;
    }

    public static String getTextureFileLocation() {
        return textureFileLocation;
    }

    public static String getSaveFileLocation() {
        return saveFileLocation;
    }

    public static byte getRenderDistance() {
        return renderDistanceOptions[renderDistanceIndex];
    }

    public static void setRenderDistance(byte renderDistance) {
        renderDistanceIndex = (byte) ((renderDistance / 5) - 1);
    }

    public static ControlScheme getControlScheme() {
        return controlScheme;
    }

    public static byte getBiomeScale() {
        return biomeScale;
    }

    public static byte getDefaultBiomeBlendFactor() {
        return defaultBiomeBlendFactor;
    }

    public static String getShaderFileLocation() {
        return shaderFileLocation;
    }

    public static String getAppTitle() {
        return appTitle;
    }

    public static  String getAppVersion() {
        return appVersion;
    }

    public static int getDisplayWidth() {
        return displayWidth;
    }

    public static int getDisplayHeight() {
        return displayHeight;
    }

    public static int getFpsCap() {
        return fpsCap;
    }

    public static int getBlockSize() {
        return blockSize;
    }

    public static int getChunkSize() {
        return chunkSize;
    }

    public static int getChunkHeight() {
        return chunkHeight;
    }

    public static float getTerrainSpriteSheetSize() {
        return terrainSpriteSheetSize;
    }

    public static String getFontFileLocation() {
        return fontFileLocation;
    }

    public static float getTerrainSpriteSheetTileSize() {
        return terrainSpriteSheetTileSize;
    }

    public static byte getSnowLevel() {
        return snowLevel;
    }

    public static int getRiverScale() {
        return riverScale;
    }

    public static byte getWaterLevel() {
        return waterLevel;
    }

    public static float getNearPlane() {
        return NEAR_PLANE;
    }

    public static float getFarPlane() {
        return FAR_PLANE;
    }

    public static String getSelectedFont() {
        return selectedFont;
    }

    public static boolean isExitRequested() {
        return exitRequested;
    }

    public static void setExitRequested(boolean exitRequested) {
        Settings.exitRequested = exitRequested;
    }

    public static boolean isEnableShadows() {
        return enableShadows;
    }

    public static void setEnableShadows(boolean enableShadows) {
        Settings.enableShadows = enableShadows;
    }

    public static int getDefaultDisplayWidth() {
        return defaultDisplayWidth;
    }

    public static int getDefaultDisplayHeight() {
        return defaultDisplayHeight;
    }

    public static boolean isDrawDebugOverlay() {
        return drawDebugOverlay;
    }

    public static void toggleDrawDebugOverlay() {
        drawDebugOverlay = !drawDebugOverlay;
    }

    public static Byte[] getRenderDistanceOptions() {
        return renderDistanceOptions;
    }

    public static byte getRenderDistanceIndex() {
        return renderDistanceIndex;
    }

    public static boolean isInvertHotbarScroll() {
        return invertHotbarScroll;
    }

    public static float getRiverWidth() {
        return riverWidth;
    }

    public static boolean isEnableWaterDistortions() {
        return enableWaterDistortions;
    }

    public static void setEnableWaterDistortions(boolean enableWaterDistortions) {
        Settings.enableWaterDistortions = enableWaterDistortions;
    }

    public static boolean isEnableWaterReflections() {
        return enableWaterReflections;
    }

    public static void setEnableWaterReflections(boolean enableWaterReflections) {
        Settings.enableWaterReflections = enableWaterReflections;
    }

    public static String getCopyrightNotice() {
        return copyrightNotice;
    }

    public static int getShadowMapSize() {
        return shadowMapSize;
    }
}
