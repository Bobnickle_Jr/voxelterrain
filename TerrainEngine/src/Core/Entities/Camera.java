package Core.Entities;

import org.lwjgl.util.vector.Vector3f;

public class Camera {

	private float distanceFromPlayer = 0;

	private Vector3f position = new Vector3f(0,15,0);
	private float pitch;
	private float yaw = 180;

	private Entity targetEntity;

	public Camera() {}
	
	public void move(){
		calculatePitch();
		float horizontalDistance = calculateHorizontalDistance();
		float verticalDistance = calculateVerticalDistance();
		calculateCameraPosition(horizontalDistance, verticalDistance + 1.75f);
		this.yaw = 180 - (targetEntity.getRotY());
	}

	private void calculateCameraPosition(float horizDistance, float verticDistance) {
		float theta = targetEntity.getRotY();
		float xOffset = (float) (horizDistance * Math.sin(Math.toRadians(theta)));
		float zOffset = (float) (horizDistance * Math.cos(Math.toRadians(theta)));
		position.x = targetEntity.getPosition().x - xOffset;
		position.z = targetEntity.getPosition().z - zOffset;

		position.y = targetEntity.getPosition().y + verticDistance;

	}

	public void invertPitch() {
		this.pitch = -pitch;
	}

	public Vector3f getPosition() {
		return position;
	}

	public float getPitch() {
		return pitch;
	}

	public float getYaw() {
		return yaw;
	}

	private float calculateHorizontalDistance() {
		return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
	}

	private float calculateVerticalDistance() {
		return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
	}

	private void calculatePitch() {
		pitch = targetEntity.getLookPitch();
	}

	public void setTargetEntity(Entity targetEntity) {
		this.targetEntity = targetEntity;
	}
}
