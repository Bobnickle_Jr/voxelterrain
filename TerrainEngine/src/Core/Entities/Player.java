package Core.Entities;

import Client.Rendering.DisplayManager;
import Client.Rendering.Loader;
import Core.Control.GameWorld;
import Core.Control.Settings;
import Core.Entities.Inventory.Inventory;
import Core.Entities.Inventory.InventoryItem;
import Core.Entities.Inventory.ItemRoutineBlock;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.TerrainManager;
import Core.Utils.ControlSchemes.ControlScheme;
import Core.Utils.Logging.Log;
import Core.Utils.Maths;
import Core.Utils.MousePicker;
import Core.Utils.Saving.SaveFileUtils;
import Core.Utils.StringUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import static Core.Entities.Inventory.InventoryItemType.IIT_BLOCK;
import static Core.Terrain.Data.Blocks.BlockType.*;
import static Core.Utils.ControlSchemes.ControlSchemeButton.*;

public class Player extends Creature {

    private Inventory inventory;

    public Player(GameWorld gameWorld, String name, Loader loader) {
        super(gameWorld, name, null, new Vector3f(), 0, 0, 0, 1);

        Log.info("Loading player: " + name + ".");
        if (!loadFromFile()) {
            Log.info("No Valid File Found, Creating New Player Instead");
            initializeNewPlayer();
        }
        //TODO: move this to init and load once inventory can be changed in game
        this.inventory = new Inventory(10, 0);
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_STONE_BRICK, loader));
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_WOOD_PLANKS, loader));
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_GRASS, loader));
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_DIRT, loader));
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_SNOW, loader));
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_SAND, loader));
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_STONE, loader));
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_WOOD_LOG, loader));
        inventory.addItem(new InventoryItem(IIT_BLOCK, BLT_LEAVES_1, loader));

        Log.info("Spawned Player at: " + StringUtils.coordString(getPosition()) + ".");
    }


    public void initializeNewPlayer() {
        // TODO: set coords to world spawn point once it is defined in meta data
        getPosition().x = 0;
        getPosition().y = 100;
        getPosition().z = 0;
    }

    public boolean loadFromFile() {
        String playerData = SaveFileUtils.getPlayerData(gameWorld.getMetaData(), name);
        if (playerData == null) {
            return false;
        }
        try {
            String[] playerDataSplit = playerData.split(":");
            getPosition().x = Float.parseFloat(playerDataSplit[0]);
            getPosition().y = Float.parseFloat(playerDataSplit[1]);
            getPosition().z = Float.parseFloat(playerDataSplit[2]);
            setRotY(Float.parseFloat(playerDataSplit[3]));
            setLookPitch(Float.parseFloat(playerDataSplit[4]));
            return true;
        } catch (Exception e) {
            Log.error("Failed to parse data file for player: " + name + ".");
            e.printStackTrace();
            return false;
        }
    }


    public void doMovement() {
        checkMovementInputs();
        enforceMovement();
    }

    public void update(MousePicker picker) {
        // change hotbar selection
        int hotbarChange = Mouse.getDWheel() / 120;
        if (hotbarChange != 0) {
            inventory.increaseSelectedSlot(hotbarChange);
        }

        // interact with blocks
        Block targetBlock = picker.getTargetSolidBlock();
        if (targetBlock != null) {
            if (Settings.getControlScheme().buttonPressed(CSB_USE_ITEM_2) && inventory.selectedItemInstanceOf(IIT_BLOCK)) {
                Vector3f blockPosition = picker.getTargetSolidBlockEmptyNeighbourCoord();
                TerrainManager.addBlock(blockPosition, ((ItemRoutineBlock) inventory.getSelectedItem().getRoutine()).getBlockType());
            } else if (Settings.getControlScheme().buttonPressed(CSB_USE_ITEM_1)) {
                TerrainManager.removeBlock(targetBlock.getPosition());
            }
        }
    }

    private void enforceMovement() {
        // rotate
        super.increaseRotation(0, currentTurnSpeed , 0);
        super.increaseLookPitch( currentLookSpeed );
        // move
        float distance = currentSpeed * DisplayManager.getFrameTimeSeconds();
        float dx = (float) (distance * Math.sin(Math.toRadians(super.getRotY())));
        float dz = (float) (distance * Math.cos(Math.toRadians(super.getRotY())));
        super.increasePosition(dx, 0, dz);
        // strafe
        float stDistance = currentStrafeSpeed * DisplayManager.getFrameTimeSeconds();
        float stDx = (float) (stDistance * Math.sin(Math.toRadians(super.getRotY() - 90)));
        float stDz = (float) (stDistance * Math.cos(Math.toRadians(super.getRotY() - 90)));
        super.increasePosition(stDx, 0, stDz);
        // increase / decrease height
        super.increasePosition(0, upwardsSpeed * DisplayManager.getFrameTimeSeconds(), 0);
    }

    private void checkMovementInputs() {
        ControlScheme controlScheme = Settings.getControlScheme();
        // move
        this.currentSpeed = 0;
        if (controlScheme.buttonDown(CSB_WALK)) {
            this.currentSpeed = RUN_SPEED;
        } else if(controlScheme.buttonDown(CSB_WALK_BACK)) {
            this.currentSpeed = -RUN_SPEED;
        }

        // strafe
        this.currentStrafeSpeed = 0;
        if (controlScheme.buttonDown(CSB_STRAFE_RIGHT)){
            this.currentStrafeSpeed = STRAFE_SPEED;
        } else if(controlScheme.buttonDown(CSB_STRAFE_LEFT)){
            this.currentStrafeSpeed = -STRAFE_SPEED;
        }

        // sprint
        if (controlScheme.buttonDown(CSB_SPRINT)) {
            this.currentSpeed *= 2;
            this.currentStrafeSpeed *= 2;
        }

        // turn and look
        currentTurnSpeed = ((Display.getWidth() / 2f) - Mouse.getX());
        currentLookSpeed = ((Display.getHeight() / 2f) - Mouse.getY());
        Mouse.setCursorPosition(Display.getWidth() / 2, Display.getHeight() / 2);
        if (Math.abs(currentTurnSpeed) < 1) {
            currentTurnSpeed = 0;
        }
        if (Math.abs(currentLookSpeed) < 1) {
            currentLookSpeed = 0;
        }
        currentTurnSpeed *= 0.25;
        currentLookSpeed *= 0.25;

        // increase position
        if (controlScheme.buttonDown(CSB_JUMP)) {
            upwardsSpeed = UP_SPEED;
        } else if (controlScheme.buttonDown(CSB_CROUCH)) {
            upwardsSpeed = -UP_SPEED;
        } else {
            upwardsSpeed = 0;
        }
    }

    /**
     * Generate and return string used in save file
     */
    public String getSaveString() {
        StringBuilder sb = new StringBuilder();

        sb.append(getPosition().x).append(":");
        sb.append(getPosition().y).append(":");
        sb.append(getPosition().z).append(":");
        sb.append(getRotY()).append(":");
        sb.append(getLookPitch()).append("\n");

        return sb.toString();
    }

    /**
     * Return coords as a pretty string
     */
    public String getChunkCoordString() {
        return "(" + Maths.worldCoordToChunkCoord(getPosition().x) + "," + Maths.worldCoordToChunkCoord(getPosition().z) + ")";
    }

    /**
     * Save player data to save file
     */
    public void saveToFile() {
        SaveFileUtils.createPlayerFile(gameWorld.getMetaData(), this);
    }

    public Inventory getInventory() {
        return inventory;
    }
}
