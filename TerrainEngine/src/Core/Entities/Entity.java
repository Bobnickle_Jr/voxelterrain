package Core.Entities;

import Client.Rendering.Models.TexturedModel;
import Core.Terrain.CollisionMesh.CollisionMesh;
import org.lwjgl.util.vector.Vector3f;

public class Entity {

	private TexturedModel model;
	private CollisionMesh collisionMesh;
	private Vector3f position;
	private float rotX, rotY, rotZ;
	private float scale;

	private float lookPitch;

	public Entity(TexturedModel model, CollisionMesh collisionMesh, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		this.model = model;
		this.collisionMesh = collisionMesh;
		this.position = position;
		this.rotX = rotX;
		this.rotY = rotY;
		this.rotZ = rotZ;
		this.scale = scale;
	}

	public void increasePosition(float dx, float dy, float dz) {
		this.position.x += dx;
		this.position.y += dy;
		this.position.z += dz;
	}

	public void increaseRotation(float dx, float dy, float dz) {
		this.rotX += dx;
		this.rotY += dy;
		this.rotZ += dz;
	}

	public void increaseLookPitch(float dp) {
		this.lookPitch = (lookPitch + dp % 360);
		if (lookPitch > 90) {
			lookPitch = 90;
		} else if (lookPitch < -90) {
			lookPitch = -90;
		}
	}

	public TexturedModel getModel() {
		return model;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getRotX() {
		return rotX;
	}

	public float getRotY() {
		return rotY;
	}

	public void setRotY(float rotY) {
		this.rotY = rotY % 360;
	}

	public float getRotZ() {
		return rotZ;
	}

	public float getScale() {
		return scale;
	}

	public float getLookPitch() {
		return lookPitch;
	}

	public void setLookPitch(float lookPitch) {
		this.lookPitch = lookPitch;
	}

	public CollisionMesh getCollisionMesh() {
		return collisionMesh;
	}
}
