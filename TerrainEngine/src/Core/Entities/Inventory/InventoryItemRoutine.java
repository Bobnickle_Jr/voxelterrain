package Core.Entities.Inventory;

import org.newdawn.slick.opengl.Texture;

public interface InventoryItemRoutine {

    Texture getTexture();
}
