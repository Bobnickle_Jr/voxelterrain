package Core.Entities.Inventory;

import Client.Rendering.Loader;
import Core.Utils.Logging.Log;

public enum InventoryItemType {

    IIT_BLOCK(ItemRoutineBlock.class),
    ;

    private Class<?> routine;

    InventoryItemType(Class routine) {
        this.routine = routine;
    }

    public InventoryItemRoutine getNewRoutine(Object subType, Loader loader) {
        try {
            if (routine != null) {
                return ((InventoryItemRoutine) routine.getDeclaredConstructor(Object.class, Loader.class).newInstance(subType, loader));
            }
        } catch (Exception e) {
            Log.error("Failed to create routine for inventory item type: " + this);
        }
        return null;
    }
}
