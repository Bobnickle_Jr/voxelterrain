package Core.Entities.Inventory;

import Client.Rendering.Loader;
import org.newdawn.slick.opengl.Texture;

public class InventoryItem {

    private InventoryItemType type;
    private InventoryItemRoutine routine;

    public InventoryItem(InventoryItemType type, Object subType, Loader loader) {
        this.type = type;
        this.routine = type.getNewRoutine(subType, loader);
    }

    public Texture getTexture() {
        return routine.getTexture();
    }

    // getters and setters

    public InventoryItemType getType() {
        return type;
    }

    public InventoryItemRoutine getRoutine() {
        return routine;
    }

}
