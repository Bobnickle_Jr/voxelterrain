package Core.Entities.Inventory;

import Core.Control.Settings;

public class Inventory {

    private InventoryItem[] items;
    private int selectedSlot;

    public Inventory(int totalSlots, int startingSelection) {
        this.items = new InventoryItem[totalSlots];
        this.selectedSlot = startingSelection;
    }

    public InventoryItem addItem(InventoryItem inventoryItem) {
        for (int i = 0; i < items.length; i += 1) {
            inventoryItem = addItem(inventoryItem, i);
            if (inventoryItem == null) {
                return null;
            }
        }
        return inventoryItem;
    }

    public void increaseSelectedSlot(int increase) {
        selectedSlot += increase * (Settings.isInvertHotbarScroll() ? -1 : 1);
        selectedSlot = Math.max(0, Math.min(items.length - 1, selectedSlot));
    }

    public boolean selectedItemInstanceOf(InventoryItemType type) {
        return getSelectedItem() != null && getSelectedItem().getType() == type;
    }

    public InventoryItem getSelectedItem() {
        return (selectedSlot >= 0 && selectedSlot < items.length) ? items[selectedSlot] : null;
    }

    public boolean isSlotSelected(int slot) {
        return selectedSlot == slot;
    }

    public InventoryItem addItem(InventoryItem item, int slot) {
        if (items[slot] == null) {
            items[slot] = item;
            return null;
        }
        return item;
    }

    public InventoryItem getItem(int slot) {
        return items[slot];
    }

    public InventoryItem removeItem(int slot) {
        InventoryItem item = items[slot];
        items[slot] = null;
        return item;
    }

    // getters and setters

    public int getSelectedSlot() {
        return selectedSlot;
    }
}
