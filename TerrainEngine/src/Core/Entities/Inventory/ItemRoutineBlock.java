package Core.Entities.Inventory;

import Client.Rendering.Loader;
import Core.Control.Settings;
import Core.Terrain.Data.Blocks.BlockType;
import org.newdawn.slick.opengl.Texture;

public class ItemRoutineBlock implements InventoryItemRoutine {

    private BlockType blockType;
    private Texture texture;

    public ItemRoutineBlock(Object blockType, Loader loader) {
        this.blockType = (BlockType) blockType;
        texture = loader.loadTexture(Settings.getTextureFileLocation() + "/items/blocks/" + this.blockType.getAssetName());
    }

    @Override
    public Texture getTexture() {
        return texture;
    }

    //  getters and setters

    public BlockType getBlockType() {
        return blockType;
    }
}
