package Core.Entities;

import Client.Rendering.Models.TexturedModel;
import Core.Control.GameWorld;
import org.lwjgl.util.vector.Vector3f;

public class Creature extends Entity {

    protected static final float RUN_SPEED = 10; // units per second
    protected static final float STRAFE_SPEED = 8; // units per second
    protected static final float UP_SPEED = 15; // units per second

    protected float currentSpeed = 0;
    protected float currentStrafeSpeed = 0;
    protected float currentTurnSpeed = 0;
    protected float currentLookSpeed = 0;
    protected float upwardsSpeed = 0;

    protected GameWorld gameWorld;

    protected String name;

    public Creature(GameWorld gameWorld, String name, TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        super(model, null, position, rotX, rotY, rotZ, scale);
        this.gameWorld = gameWorld;
        this.name = name;
    }

    // getters and setters

    public String getName() {
        return name;
    }
}
