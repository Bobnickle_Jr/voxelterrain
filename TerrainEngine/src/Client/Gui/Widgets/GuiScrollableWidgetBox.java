package Client.Gui.Widgets;

import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import org.lwjgl.util.vector.Vector2f;

public class GuiScrollableWidgetBox extends GuiWidget {

    private GuiWidget scrollableWidgets;
    private GuiWidget upButton;
    private GuiWidget downButton;

    public GuiScrollableWidgetBox(Vector2f location, GuiTexturePredef texturePredef, float imageScale) {
        super(location, null, texturePredef, imageScale);
        scrollableWidgets = addChild(new GuiWidgetBox());
        upButton = addChild(new GuiButton(new Vector2f(0.26f, 0.3f), "^", GuiTexturePredef.GTP_32x48_BROWN));
        downButton = addChild(new GuiButton(new Vector2f(0.26f, -0.3f), "v", GuiTexturePredef.GTP_32x48_BROWN));
    }

    public void addScrollableChild(GuiButton guiWidget) {
        guiWidget.setParent(scrollableWidgets);
        guiWidget.setLocation(new Vector2f(0, 0.3f - 0.2f * scrollableWidgets.getChildren().size()));
        guiWidget.setSelectable(true);
        scrollableWidgets.addChild(guiWidget);
    }

    public GuiWidget getSelectedScrollableChild() {
        for (GuiWidget child : scrollableWidgets.getChildren()) {
            if (child.isSelected()) {
                return child;
            }
        }
        return null;
    }

    @Override
    protected void updateSelf() {
        GuiWidget pressedChild = getPressedChild();
        if (pressedChild != null) {
            switch (pressedChild.getName()) {
                case "^":
                    if (scrollableWidgets.getLocation().y > 0.1f) {
                        scrollableWidgets.increasePosition(new Vector2f(0, -0.2f));
                    }
                    break;
                case "v":
                    if (scrollableWidgets.getLocation().y < 0.2f * (scrollableWidgets.getChildren().size() - 4)) {
                        scrollableWidgets.increasePosition(new Vector2f(0, 0.2f));
                    }
                    break;
            }
        }
        GuiWidget pressedScrollChild = scrollableWidgets.getPressedChild();
        if (pressedScrollChild != null) {
            for (GuiWidget scrollableChild : scrollableWidgets.getChildren()) {
                if (scrollableChild != pressedScrollChild) {
                    scrollableChild.setSelected(false);
                }
            }
        }

        upButton.setLocked(scrollableWidgets.getLocation().y <= 0.1f);
        downButton.setLocked(scrollableWidgets.getLocation().y >= 0.2f * (scrollableWidgets.getChildren().size() - 4));

        for (GuiWidget widget : scrollableWidgets.getChildren()) {
            widget.setHidden((scrollableWidgets.getLocation().y + widget.getLocation().y) > 0.4f
                    || (scrollableWidgets.getLocation().y + widget.getLocation().y) < -0.4f);
        }
    }
}
