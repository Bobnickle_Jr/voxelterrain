package Client.Gui.Widgets;

import Client.Gui.GuiUtils;
import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Core.Control.Settings;
import Core.Utils.ControlSchemes.ControlSchemeButton;
import org.lwjgl.util.vector.Vector2f;

public class GuiTextBox extends GuiWidget {

    private String value = "";
    private String placeholder;
    private int maxLength;

    public GuiTextBox(Vector2f location, String name, String placeholder, int maxLength, GuiTexturePredef texturePredef, float imageScale) {
        super(location, name, texturePredef, imageScale);
        this.placeholder = placeholder;
        this.maxLength = maxLength;
    }

    @Override
    public String getText() {
        return value + (selected ? "|" : (value.length() == 0 ? placeholder : ""));
    }

    @Override
    protected void updateSelf() {
        if (Settings.getControlScheme().buttonPressed(ControlSchemeButton.CSB_USE_ITEM_1)) {
            selected = collidesWithCursor();
        }

        if (selected) {
            value = GuiUtils.processKeyboardInput(value, maxLength);
        }
    }

    // getters and setters

    public String getValue() {
        return value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
