package Client.Gui.Widgets;

import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Core.Control.Settings;
import org.lwjgl.util.vector.Vector2f;

import static Client.Rendering.GUIs.fontRendering.TextAlign.TA_CENTERED;

public class GuiSettingsWidget extends GuiWidgetBox {

    public GuiSettingsWidget() {
        super(new Vector2f());
        init();
    }

    public GuiSettingsWidget(Vector2f location) {
        super(location);
        init();
    }

    public void init() {
        addChild(new GuiImage(new Vector2f(0, 0), GuiTexturePredef.GTP_400x400_BROWN, 1));

        // water settings
        addChild(new GuiLabel(new Vector2f(0, 0.30f), "Water Visuals", null, 1, TA_CENTERED));
        addChild(new GuiToggleButton(new Vector2f(-0.15f, 0.20f), "Water Reflections", GuiTexturePredef.GTP_192x32_BROWN, Settings.isEnableWaterReflections() ? 0 : 1, true, false));
        addChild(new GuiToggleButton(new Vector2f(0.15f, 0.20f), "Water Distortions", GuiTexturePredef.GTP_192x32_BROWN, Settings.isEnableWaterDistortions() ? 0 : 1, true, false));

        // solid settings
        addChild(new GuiLabel(new Vector2f(0, 0.10f), "Solid Visuals", null, 1, TA_CENTERED));
        addChild(new GuiToggleButton(new Vector2f(-0.15f, 0.0f), "Enable Shadows", GuiTexturePredef.GTP_192x32_BROWN, Settings.isEnableShadows() ? 0 : 1, true, false));
        addChild(new GuiToggleButton(new Vector2f(0.15f, 0.0f), "Render Distance", GuiTexturePredef.GTP_192x32_BROWN, Settings.getRenderDistanceIndex(), Settings.getRenderDistanceOptions()));

        addChild(new GuiButton(new Vector2f(0, -0.45f), "Back", GuiTexturePredef.GTP_256x32_RED));
    }

    public void update() {
        super.update();
        GuiWidget pressedButton = getPressedChild();
        if (pressedButton != null) {
            switch (pressedButton.getName()) {
                case "Water Reflections":
                    Settings.setEnableWaterReflections((boolean) ((GuiToggleButton) pressedButton).getCurrentValue());
                    break;
                case "Water Distortions":
                    Settings.setEnableWaterDistortions((boolean) ((GuiToggleButton) pressedButton).getCurrentValue());
                    break;
                case "Render Distance":
                    Settings.setRenderDistance((byte) ((GuiToggleButton) pressedButton).getCurrentValue());
                    break;
                case "Enable Shadows":
                    Settings.setEnableShadows((boolean) ((GuiToggleButton) pressedButton).getCurrentValue());
                    break;
            }
        }
    }
}
