package Client.Gui.Widgets;

import Client.Gui.GuiWidget;
import org.lwjgl.util.vector.Vector2f;

public class GuiWidgetBox extends GuiWidget {

    public GuiWidgetBox() {
        super(new Vector2f(), null, null, 1);
    }

    public GuiWidgetBox(Vector2f location) {
        super(location, null, null, 1);
    }
}
