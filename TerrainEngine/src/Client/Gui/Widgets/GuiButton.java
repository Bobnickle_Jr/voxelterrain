package Client.Gui.Widgets;

import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiColour;
import Client.Gui.Utils.GuiTexturePredef;
import Core.Control.Settings;
import org.lwjgl.util.vector.Vector2f;

import static Core.Utils.ControlSchemes.ControlSchemeButton.CSB_USE_ITEM_1;

public class GuiButton extends GuiWidget {

    boolean selectable;

    public GuiButton(Vector2f location, String text, GuiTexturePredef texturePredef) {
        super(location, text, texturePredef, 1);
    }

    @Override
    protected void updateSelf() {
        if (!locked) {
            if (collidesWithCursor()) {
                updateTexture(GuiTexturePredef.find(pixelWidth, pixelHeight, GuiColour.GC_RED));
                if (Settings.getControlScheme().buttonReleased(CSB_USE_ITEM_1)){
                    pressed = true;
                    selected = selectable;
                } else {
                    pressed = false;
                }

            } else {
                updateTexture(GuiTexturePredef.find(pixelWidth, pixelHeight, GuiColour.GC_BROWN));
                pressed = false;
            }
        } else {
            updateTexture(GuiTexturePredef.find(pixelWidth, pixelHeight, GuiColour.GC_GRAY));
            pressed = false;
        }
        if (selected) {
            updateTexture(GuiTexturePredef.find(pixelWidth, pixelHeight, GuiColour.GC_RED));
        }
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }
}
