package Client.Gui.Widgets;

import Client.Gui.Utils.GuiTexturePredef;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.Arrays;

public class GuiToggleButton extends GuiButton {

    private ArrayList<Object> options;
    private int currentOptionIndex;

    public GuiToggleButton(Vector2f location, String text, GuiTexturePredef texturePredef, int startingIndex, Object... options) {
        super(location, text, texturePredef);
        this.currentOptionIndex = startingIndex;
        this.options = new ArrayList<>(Arrays.asList(options));
    }

    @Override
    public String getText() {
        return name + ": " + getCurrentValue();
    }

    @Override
    protected void updateSelf() {
        super.updateSelf();
        if (pressed) {
            currentOptionIndex = (currentOptionIndex + 1) % options.size();
        }
    }

    public Object getCurrentValue() {
        return options.get(currentOptionIndex);
    }
}
