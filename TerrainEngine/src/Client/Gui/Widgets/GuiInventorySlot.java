package Client.Gui.Widgets;

import Client.Gui.GuiUtils;
import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Core.Entities.Inventory.Inventory;
import Core.Entities.Inventory.InventoryItem;
import org.lwjgl.util.vector.Vector2f;

public class GuiInventorySlot extends GuiWidget {

    private Inventory inventory;
    private int targetInventorySlot;

    public GuiInventorySlot(Vector2f location, float imageScale, Inventory inventory, int targetInventorySlot) {
        super(location, null, GuiTexturePredef.GTP_48x48_BROWN, imageScale);
        this.inventory = inventory;
        this.targetInventorySlot = targetInventorySlot;
    }

    @Override
    protected void drawSelf() {
        super.drawSelf();
        InventoryItem item = inventory.getItem(targetInventorySlot);
        if (item != null) {
            GuiUtils.drawTextureToGui(item.getTexture(), getAbsoluteLocation().x, getAbsoluteLocation().y, imageScale * 1.75f);
        }
    }

    @Override
    protected void updateSelf() {
        if (inventory.isSlotSelected(targetInventorySlot)) {
            updateTexture(GuiTexturePredef.GTP_48x48_RED);
        } else {
            updateTexture(GuiTexturePredef.GTP_48x48_BROWN);
        }
    }

}
