package Client.Gui.Widgets;

import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Client.Rendering.GUIs.fontRendering.TextAlign;
import org.lwjgl.util.vector.Vector2f;

public class GuiLabel extends GuiWidget {
    public GuiLabel(Vector2f location, String text, GuiTexturePredef texturePredef, int fontSize, TextAlign textAlign) {
        super(location, text, texturePredef, 1);
        this.fontSize = fontSize;
        this.textAlign = textAlign;
    }
}
