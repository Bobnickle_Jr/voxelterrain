package Client.Gui.Widgets;

import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Client.Rendering.GUIs.fontRendering.TextAlign;
import Core.Control.Settings;
import org.lwjgl.util.vector.Vector2f;

import static Client.Rendering.GUIs.fontRendering.TextAlign.TA_LEFT;

public class GuiCopyrightWidget extends GuiLabel {

    public GuiCopyrightWidget() {
        super(new Vector2f(-0.98f, -0.95f), Settings.getCopyrightNotice(), null, 1, TA_LEFT);
    }
}
