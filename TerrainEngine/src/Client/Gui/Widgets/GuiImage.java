package Client.Gui.Widgets;

import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import org.lwjgl.util.vector.Vector2f;

public class GuiImage extends GuiWidget {
    public GuiImage(Vector2f location, GuiTexturePredef guiTexturePredef, float imageScale) {
        super(location, null, guiTexturePredef, imageScale);
    }
}
