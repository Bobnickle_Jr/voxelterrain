package Client.Gui.Utils;


public enum GuiColour {

    GC_BROWN("Brown"),
    GC_GRAY("Gray"),
    GC_BEIGE("Beige"),
    GC_RED("Red"),
    ;

    private String assetName;

    GuiColour(String assetName) {
        this.assetName = assetName;
    }
}
