package Client.Gui.Utils;

import Client.Gui.GuiUtils;
import Core.Utils.Logging.Log;
import org.newdawn.slick.opengl.Texture;

public enum GuiTexturePredef {

    GTP_GRAY_OUT("grayOut", 64, 64),
    GTP_MAIN_MENU_BACKGROUND("MainMenuBackground", 1280, 720),

    GTP_48x48_BROWN("48x48_BROWN", 48, 48, GuiColour.GC_BROWN),
    GTP_48x48_RED("48x48_RED", 48, 48, GuiColour.GC_RED),

    GTP_32x48_BROWN("32x48_BROWN", 32, 48, GuiColour.GC_BROWN),
    GTP_32x48_RED("32x48_RED", 32, 48, GuiColour.GC_RED),
    GTP_32x48_GRAY("32x48_GRAY", 32, 48, GuiColour.GC_GRAY),

    GTP_192x32_BROWN("192x32_BROWN", 192, 32, GuiColour.GC_BROWN),
    GTP_192x32_RED("192x32_RED",192, 32, GuiColour.GC_RED),
    GTP_192x32_GRAY("192x32_GRAY",192, 32, GuiColour.GC_GRAY),
    GTP_192x32_BEIGE("192x32_BEIGE", 192, 32, GuiColour.GC_BEIGE),

    GTP_256x32_BROWN("256x32_BROWN", 256, 32, GuiColour.GC_BROWN),
    GTP_256x32_RED("256x32_RED", 256, 32, GuiColour.GC_RED),
    GTP_256x32_GRAY("256x32_GRAY", 256, 32, GuiColour.GC_GRAY),
    GTP_256x32_BEIGE("256x32_BEIGE", 256, 32, GuiColour.GC_BEIGE),

    GTP_256x64_BROWN("256x64_BROWN", 256, 64, GuiColour.GC_BROWN),
    GTP_256x64_RED("256x64_RED", 256, 64, GuiColour.GC_RED),

    GTP_384x64_BEIGE("384x64_BEIGE", 384, 64, GuiColour.GC_BEIGE),

    GTP_400x400_BROWN("400x400_BROWN", 400, 400, GuiColour.GC_BROWN),
    ;

    private Texture texture;
    private String fileName;
    private int pixelWidth;
    private int pixelHeight;
    private GuiColour colour;

    GuiTexturePredef(String fileName, int pixelWidth, int pixelHeight, GuiColour colour) {
        this.fileName = fileName;
        this.pixelWidth = pixelWidth;
        this.pixelHeight = pixelHeight;
        this.colour = colour;
    }

    GuiTexturePredef(String fileName, int pixelWidth, int pixelHeight) {
        this.fileName = fileName;
        this.pixelWidth = pixelWidth;
        this.pixelHeight = pixelHeight;
    }

    public static GuiTexturePredef find(int pixelWidth, int pixelHeight, GuiColour colour) {
        for (GuiTexturePredef predef : values()) {
            if (predef.pixelWidth == pixelWidth && predef.pixelHeight == pixelHeight && predef.colour == colour) {
                return predef;
            }
        }
        Log.warning("Could not find GUI Texture Predef for: " + pixelWidth + " " + pixelHeight + " " + colour);
        return null;
    }

    // getters and setters

    public Texture getTexture() {
        if (texture == null) {
            texture = GuiUtils.loadGuiTexture(fileName);
        }
        return texture;
    }

    public int getPixelWidth() {
        return pixelWidth;
    }

    public int getPixelHeight() {
        return pixelHeight;
    }

}
