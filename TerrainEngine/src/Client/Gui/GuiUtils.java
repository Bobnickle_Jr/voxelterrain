package Client.Gui;

import Client.Gui.Utils.GuiColour;
import Client.Rendering.GUIs.GuiTexture;
import Client.Rendering.GUIs.fontMeshCreator.FontType;
import Client.Rendering.GUIs.fontMeshCreator.GUIText;
import Client.Rendering.GUIs.fontRendering.TextAlign;
import Client.Rendering.Loader;
import Core.Control.Settings;
import Core.Utils.StringUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GuiUtils {

    private static Loader loader;

    private static FontType font;

    private static ArrayList<GUIText> textCache = new ArrayList<>();
    private static int debugRows;
    private static int consoleRows;

    private static int screenWidth;
    private static int screenHeight;
    private static List<GuiTexture> guiTextures = new ArrayList<>();

    /**
     * Initialise Gui Utils
     */
    public static void init(Loader load, int width, int height) {
        loader = load;
        font = loadFont(Settings.getSelectedFont());
        screenWidth = width;
        screenHeight = height;
    }

    /**
     * Load a texture from the gui resources folder
     */
    public static Texture loadGuiTexture(String fileName) {
        return loader.loadTexture(Settings.getTextureFileLocation() + "/gui/" + fileName);
    }

    /**
     * Load a font from font files
     */
    private static FontType loadFont(String fontName) {
        return new FontType(loader.loadTexture(Settings.getFontFileLocation() + "/" + fontName).getTextureID(),
                new File(Settings.getFontFileLocation() + "/" + fontName + ".fnt"));
    }

    /**
     * Check that a given character is in the set of valid characters
     */
    public static boolean characterExists(char character) {
        return Character.isDigit(character) || Character.isAlphabetic(character) || Character.isSpaceChar(character)
                || StringUtils.containsCharacter("/.?!\"$%^&*()_+-=[];,#~", character);
    }

    /**
     * Draw a given texture to the GUI centered on a given point and with a given scale
     */
    public static void drawTextureToGui(Texture texture, float x, float y, float scale) {
        Vector2f position = new Vector2f(x, y);
        float xScale = scale * ((float) texture.getImageWidth() / (float) screenWidth);
        float yScale = xScale * ((float) texture.getImageHeight() / (float) texture.getImageWidth()) * ((float) screenWidth / (float) screenHeight);
        Vector2f vScale = new Vector2f(xScale * Settings.getDisplayWidth() / Settings.getDefaultDisplayWidth(), yScale * Settings.getDisplayHeight() / Settings.getDefaultDisplayHeight());

        GuiTexture gui = new GuiTexture(texture.getTextureID(), position, vScale);
        guiTextures.add(gui);
    }

    /**
     * Modify the input string based on keyboard inputs
     */
    public static String processKeyboardInput(String currentString, int maxLength) {
        boolean shift = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) && !Keyboard.isKeyDown(Keyboard.KEY_RSHIFT);
        StringBuilder currentStringBuilder = new StringBuilder(currentString);
        while (Keyboard.next()) {
            if (Keyboard.isKeyDown(Keyboard.KEY_DELETE)) {
                currentStringBuilder = new StringBuilder();
            } else if (Keyboard.isKeyDown(Keyboard.KEY_BACK) && Keyboard.getEventKeyState()) {
                try {
                    currentStringBuilder = new StringBuilder(currentStringBuilder.substring(0, currentStringBuilder.length() - 1));
                } catch (StringIndexOutOfBoundsException ignored) {}
            } else if (Keyboard.getEventKeyState()) {
                char character = Keyboard.getEventCharacter();
                if (shift) {
                    character = Character.toUpperCase(character);
                }
                if (GuiUtils.characterExists(character) && currentStringBuilder.length() < maxLength) {
                    currentStringBuilder.append(character);
                }
            }

        }
        currentString = currentStringBuilder.toString();
        return currentString;
    }

    /**
     * Draw Text to the screen with given settings
     */
    public static void drawTextToScreen(String string, int fontSize, float x, float y, float maxLineLength, TextAlign textAlign) {
        GUIText text = new GUIText(string, fontSize, font, new Vector2f(x, y), maxLineLength, textAlign);
        textCache.add(text);
    }

    /**
     * Draw String to debug overlay
     */
    public static void drawTextToDebugOverlay(String string) {
        debugRows += 1;
        GUIText text = new GUIText(string, 1, font, new Vector2f(1.0f - 0.0125f, 0.025f * debugRows), 1, TextAlign.TA_RIGHT);
        textCache.add(text);
    }

    /**
     * Draw String to console overlay
     */
    public static void drawTextToConsoleOverlay(String string) {
        consoleRows += 1;
        GUIText text = new GUIText(string, 1, font, new Vector2f(0.0125f, 1.0f - 0.025f * (consoleRows + 1)), 1, TextAlign.TA_LEFT);
        textCache.add(text);
    }

    /**
     * Clear cache of gui items and cleanup memory usage
     */
    public static void clearCache() {
        debugRows = 0;
        consoleRows = 0;
        textCache.forEach(GUIText::remove);
        textCache = new ArrayList<>();
        guiTextures = new ArrayList<>();
    }

    // getters and setters

    public static List<GuiTexture> getGuiTextures() {
        return guiTextures;
    }
}
