package Client.Gui.Console;

import java.util.Date;

public class ConsoleString {

    private String string;
    private Date timeStamp;

    public ConsoleString(String string, Date timeStamp) {
        this.string = string;
        this.timeStamp = timeStamp;
    }

    // getters and setters

    public String getString() {
        return string;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }
}
