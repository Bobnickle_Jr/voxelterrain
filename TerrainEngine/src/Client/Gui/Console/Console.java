package Client.Gui.Console;

import Client.Gui.GuiUtils;
import Client.Rendering.DisplayManager;
import Core.Control.GameWorld;
import Core.Control.Settings;
import Core.Entities.Player;
import Core.Terrain.Data.Blocks.Block;
import Core.Terrain.Data.Blocks.BlockType;
import Core.Terrain.Generation.BiomeType;
import Core.Terrain.Generation.Structures.StructureGenerator;
import Core.Terrain.TerrainManager;
import Core.Utils.ControlSchemes.ControlSchemeButton;
import Core.Utils.Logging.Log;
import Core.Utils.Maths;
import Core.Utils.StringUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Console {

    private ArrayList<ConsoleString> consoleLog = new ArrayList<>();

    private GameWorld gameWorld;

    private String currentString = "";
    private float cursorBlinkTimer;
    private boolean cursorBlink;

    public Console(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        writeToConsole(Settings.getAppTitle() + " - " + Settings.getAppVersion());
    }

    private void writeToConsole(String string) {
        writeToConsole("System", string);
    }

    public void flushKeyboard() {
        while (Keyboard.next()) {
            Keyboard.getEventKeyState();
        }
    }

    public void clearCurrentString() {
        currentString = "";
    }

    public boolean processKeyboardInput(String author) {
        currentString = GuiUtils.processKeyboardInput(currentString, 200);

        // submit
        if (Settings.getControlScheme().buttonReleased(ControlSchemeButton.CSB_CONSOLE_SUBMIT)) {
            writeToConsole(author, currentString);
            currentString = "";
            return true;
        }
        return false;
    }

    public void drawRecentToScreen() {
        GuiUtils.drawTextToConsoleOverlay("");

        for (int i = consoleLog.size() - 1; i >= 0 ; i -= 1) {
            ConsoleString logItem = consoleLog.get(i);
            if (new Date().getTime() - logItem.getTimeStamp().getTime() < 5000) {
                GuiUtils.drawTextToConsoleOverlay(logItem.getString());
            }
        }
    }

    public void drawToScreen() {
        cursorBlinkTimer += DisplayManager.getFrameTimeSeconds();
        if (cursorBlinkTimer >= 0.5) {
            cursorBlinkTimer = 0;
            cursorBlink = !cursorBlink;
        }
        GuiUtils.drawTextToConsoleOverlay(">>> " + currentString + (cursorBlink ? "|" : ""));

        for (int i = consoleLog.size() - 1; i >= 0 ; i -= 1) {
            ConsoleString logItem = consoleLog.get(i);
            GuiUtils.drawTextToConsoleOverlay(logItem.getString());
        }
    }

    public void writeToConsole(String writer, String string) {
        if (StringUtils.isNullOrEmptyOrWhiteSpace(string)) {
            return;
        }

        consoleLog.add(new ConsoleString("<" + writer + "> " + string, new Date()));
        Log.console("<" + writer + "> " + string);

        if (string.charAt(0) == '/') {
            interpretCommand(string);
        }
    }


    private void interpretCommand(String string) {
        String[] command = string.split(" ");
        switch (command[0].toLowerCase()) {
            case "/help":
            case "/?":
                doHelp();
                break;
            case "/seed":
                writeToConsole("World Seed: " + gameWorld.getMetaData().getsSeed() + " (" + gameWorld.getMetaData().getSeed() + ")" );
                break;
            case "/tele":
                doTeleport(command);
                break;
            case "/save-structure":
                doSaveStructure(command);
                break;
            default:
                writeToConsole("Unknown Command: " + command[0].toLowerCase());
        }
    }

    private void doHelp() {
        writeToConsole("Commands:");
        writeToConsole("? / help -> List available commands.");
        writeToConsole("seed -> Display the seed of the current game world.");
        writeToConsole("tele [x] [z] -> Teleport Player to (x, z) without changing the Player's y position.");
        writeToConsole("tele [x] [y] [z] -> Teleport Player to (x, y, z).");
        writeToConsole("save-structure [fileName] (yOffset) -> Save the chunk that the Player is stood in to a structure file, ignoring the blocks at y=0.");
    }

    private void doSaveStructure(String[] command) {
        try {
            if (command.length == 2 || command.length == 3) {
                String fileName = command[1];
                int yOffset = 0;
                if (command.length == 3) {
                    yOffset = Integer.parseInt(command[2]);
                }

                Vector3f playerCoords = gameWorld.getPlayer().getPosition();
                int chunkX = Maths.worldCoordToChunkCoord(playerCoords.x);
                int chunkZ = Maths.worldCoordToChunkCoord(playerCoords.z);
                Block[][][] chunkBlocks = TerrainManager.getLoadedChunks().get(chunkX, chunkZ).getBlocks();

                // loop through blocks and build template
                HashMap<Vector3f, BlockType> markers = new HashMap<>();
                for (int x = 0; x < Settings.getChunkSize(); x += 1) {
                    for (int y = 1; y < Settings.getChunkHeight(); y += 1) {
                        for (int z = 0; z < Settings.getChunkSize(); z += 1) {
                            Block block = chunkBlocks[x][y][z];
                            if (block != null) {
                                markers.put(new Vector3f(x, y, z), block.getType());
                            }
                        }
                    }
                }
                if (StructureGenerator.createStructureFileFromMarkers(fileName, markers, yOffset)) {
                    writeToConsole("Saved current chunk to " + fileName + ".str");
                } else {
                    writeToConsole("Failed to save current chunk to " + fileName + ".str");
                }
            } else {
                writeToConsole("Wrong number of arguments. use /help for usage guidance.");
            }
        } catch (Exception e) {
            writeToConsole("Something went wrong.");
        }
    }

    private void doTeleport(String[] command) {
        try {
            Player player = gameWorld.getPlayer();
            if (command.length == 4) {
                float x = Float.parseFloat(command[1]);
                float y = Float.parseFloat(command[2]);
                float z = Float.parseFloat(command[3]);
                player.setPosition(new Vector3f(x, y, z));
                writeToConsole("Teleported Player to " + StringUtils.coordString(player.getPosition()));
            } else if (command.length == 3) {
                float x = Float.parseFloat(command[1]);
                float z = Float.parseFloat(command[2]);
                player.setPosition(new Vector3f(x, player.getPosition().y, z));
                writeToConsole("Teleported Player to " + StringUtils.coordString(player.getPosition()));
            } else {
                writeToConsole("Wrong number of arguments. use /help for usage guidance.");
            }
        } catch (NumberFormatException e) {
            writeToConsole("Wrong argument types. use /help for usage guidance.");
        }
    }


}
