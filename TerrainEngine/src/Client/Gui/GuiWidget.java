package Client.Gui;

import Client.Gui.Utils.GuiTexturePredef;
import Client.Rendering.GUIs.fontRendering.TextAlign;
import Core.Control.Settings;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

import java.util.ArrayList;

public class GuiWidget {

    protected GuiWidget parent;
    protected ArrayList<GuiWidget> children = new ArrayList<>();

    protected Vector2f location;
    protected Texture texture;

    protected String name;
    protected int fontSize = 1;
    protected TextAlign textAlign = TextAlign.TA_CENTERED;

    protected boolean pressed;
    protected boolean hidden = false;
    protected boolean locked = false;
    protected boolean selected;

    protected int pixelWidth;
    protected int pixelHeight;
    protected float width;
    protected float height;
    protected float imageScale;

    public GuiWidget(Vector2f location, String name, GuiTexturePredef texturePredef, float imageScale) {
        this.location = location;
        this.name = name;
        this.imageScale = imageScale;
        updateTexture(texturePredef);
    }

    public void update() {
        updateSelf();
        updateChildren();
    }

    public void draw() {
        if (!hidden) {
            drawSelf();
            drawChildren();
        }
    }

    public Vector2f getAbsoluteLocation() {
        if (parent != null) {
            Vector2f parentLocation = parent.getAbsoluteLocation();
            return new Vector2f(location.x + parentLocation.x, location.y + parentLocation.y);
        }
        return location;
    }

    public String getText() {
        return getName();
    }

    public int getFontSize() {
        return fontSize;
    }

    public TextAlign getTextAlign() {
        return textAlign;
    }

    public GuiWidget pushChild(GuiWidget widget) {
        children.add(0, widget.setParent(this));
        return widget;
    }

    public GuiWidget addChild(GuiWidget widget) {
        children.add(widget.setParent(this));
        return widget;
    }

    public GuiWidget getPressedChild() {
        for (GuiWidget child : children) {
            if (child.isPressed()) {
                return child;
            }
        }
        return null;
    }

    public GuiWidget getChildByName(String targetName) {
        for (GuiWidget child : children) {
            if (child.getName() != null && child.getName().equals(targetName)) {
                return child;
            }
        }
        return null;
    }

    // private

    protected void drawSelf() {
        Vector2f absoluteLocation = getAbsoluteLocation();
        if (texture != null) {
            GuiUtils.drawTextureToGui(texture, absoluteLocation.x, absoluteLocation.y, imageScale);
        }
        if (getText() != null) {
            GuiUtils.drawTextToScreen(getText(), getFontSize(), 0.5f + absoluteLocation.x / 2 , 0.5f - absoluteLocation.y / 2 - 0.012f * fontSize, 1.0f, getTextAlign());
        }
    }

    protected void drawChildren() {
        for (GuiWidget child : children) {
            child.draw();
        }
    }

    protected void updateSelf() {

    }

    protected boolean collidesWithCursor() {
        float cursorX = ((float) Mouse.getX() / (float) Settings.getDisplayWidth() - 0.5f) * 2;
        float cursorY = ((float) Mouse.getY() / (float) Settings.getDisplayHeight() - 0.5f) * 2;

        return (cursorX > getAbsoluteLocation().x - width
                && cursorX < getAbsoluteLocation().x + width
                && cursorY > getAbsoluteLocation().y - height
                && cursorY < getAbsoluteLocation().y + height );
    }

    protected void updateChildren() {
        if (!locked && !hidden) {
            for (GuiWidget child : children) {
                child.update();
            }
        }
    }

    public void updateTexture(GuiTexturePredef texturePredef) {
        if (texturePredef != null) {
            this.texture = texturePredef.getTexture();
            this.pixelWidth = texturePredef.getPixelWidth();
            this.pixelHeight = texturePredef.getPixelHeight();
            this.width = (float) pixelWidth / Settings.getDefaultDisplayWidth();
            this.height = (float) pixelHeight / Settings.getDefaultDisplayHeight();
        }
    }

    public void increasePosition(Vector2f delta) {
        Vector2f.add(location, delta, location);
    }

    // getters and setters

    public GuiWidget setParent(GuiWidget parent) {
        this.parent = parent;
        return this;
    }

    public GuiWidget setLocation(Vector2f location) {
        this.location = location;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<GuiWidget> getChildren() {
        return children;
    }

    public Vector2f getLocation() {
        return location;
    }

    public boolean isPressed() {
        return pressed;
    }

    public boolean isHidden() {
        return hidden;
    }

    public GuiWidget setHidden(boolean hidden) {
        this.hidden = hidden;
        return this;
    }

    public GuiWidget setLocked(boolean locked) {
        this.locked = locked;
        return this;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
