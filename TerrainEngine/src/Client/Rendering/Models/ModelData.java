package Client.Rendering.Models;

import Core.Control.Settings;
import Core.Utils.ListUtils;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class ModelData {

	private float[] vertices;
	private float[] textureCoords;
	private float[] normals;
	private int[] indices;

	public ModelData(float[] vertices, float[] textureCoords, float[] normals, int[] indices,
                     float furthestPoint) {
		this.vertices = vertices;
		this.textureCoords = textureCoords;
		this.normals = normals;
		this.indices = indices;
	}

	public static ModelData merge(List<ModelData> modelDatas, List<Vector3f> positions, List<Vector2f> spriteSheetPositions) {
		ArrayList<Float> vertices = new ArrayList<>();
		ArrayList<Float> texCoords = new ArrayList<>();
		ArrayList<Float> normals = new ArrayList<>();
		ArrayList<Integer> indices = new ArrayList<>();
		int offset = 0;
		int m = 0;

		for (int modelDataIndex = 0; modelDataIndex < modelDatas.size(); modelDataIndex += 1) {
			ModelData modelData = modelDatas.get(modelDataIndex);
			Vector2f spriteSheetPos = spriteSheetPositions.get(modelDataIndex);
			Vector3f position = positions.get(m);
			float[] mVertices = modelData.getVertices();
			float[] mNormals = modelData.getNormals();

			for (int index = 0; index < modelData.getVertices().length; index += 3) {
				vertices.add(mVertices[index] + position.x * Settings.getBlockSize());
				vertices.add(mVertices[index + 1] + position.y * Settings.getBlockSize());
				// TODO: investigate why the + 1 is needed?
				// TODO: + 1 is needed because z in block models is indexed in reverse
				// TODO: remove the + 1 once block models are defined more efficiently
				vertices.add(mVertices[index + 2] + (position.z + 1) * Settings.getBlockSize());

				normals.add(mNormals[index]);
				normals.add(mNormals[index + 1] );
				normals.add(mNormals[index + 2] );
			}

			// align textures
			ArrayList<Float> newTextureCoords = new ArrayList<>();
			float tile = Settings.getTerrainSpriteSheetSize() / Settings.getTerrainSpriteSheetTileSize();
			for (int index = 0; index < modelData.textureCoords.length; index += 2) {
				newTextureCoords.add((modelData.textureCoords[index] + spriteSheetPos.x) / tile);
				newTextureCoords.add((modelData.textureCoords[index + 1] + spriteSheetPos.y) / tile);
			}

			texCoords.addAll(newTextureCoords);
			int[] mIndices = modelData.getIndices();

			for (int index : mIndices) {
				indices.add(index + offset);
			}

			offset += mVertices.length / 3;
			m++;
		}

		return new ModelData(ListUtils.toPrimitiveFloatArray(vertices), ListUtils.toPrimitiveFloatArray(texCoords), ListUtils.toPrimitiveFloatArray(normals), ListUtils.toPrimitiveIntArray(indices), 0);
	}

	// getters and setters

	public float[] getVertices() {
		return vertices;
	}

	public float[] getTextureCoords() {
		return textureCoords;
	}

	public float[] getNormals() {
		return normals;
	}

	public int[] getIndices() {
		return indices;
	}

}
