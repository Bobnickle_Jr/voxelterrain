package Client.Rendering.Models;

public class RawModel {

	private int vaoID;
	private int[] vbos;
	private int vertexCount;
	
	public RawModel(int vaoID, int vboID, int vboID1, int vboID2, int vboID3, int vertexCount) {
		this.vaoID = vaoID;
		this.vbos = new int[] {vboID, vboID1, vboID2, vboID3};
		this.vertexCount = vertexCount;
	}

	public RawModel(int vaoID, int vboID1, int vertexCount){
		this.vaoID = vaoID;
		this.vbos = new int[] {vboID1};
		this.vertexCount = vertexCount;
	}

	public int getVaoID() {
		return vaoID;
	}

	public int getVertexCount() {
		return vertexCount;
	}
	
	public int[] getVBOs() {
		return vbos;
	}

}
