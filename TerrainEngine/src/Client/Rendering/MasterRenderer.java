package Client.Rendering;

import Client.Rendering.Models.TexturedModel;
import Client.Rendering.Shaders.StaticShader;
import Client.Rendering.Shadows.ShadowMapMasterRenderer;
import Client.Rendering.SkyBox.SkyBoxRenderer;
import Client.Rendering.Water.StaticWaterShader;
import Client.Rendering.Water.WaterEntityRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Control.Settings;
import Core.Entities.Camera;
import Core.Entities.Entity;
import Core.Entities.Light;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import java.util.*;

public class MasterRenderer {

    private static final float WAVE_SPEED = 0.03f;

    // sky colours
    private static final float RED = 182f / 255f;
    private static final float GREEN = 220f / 255f;
    private static final float BLUE = 254f / 255f;

    private Matrix4f projectionMatrix;

    private static float averageRenderTime;
    private static int totalRendersCompleted;

    private StaticShader shader = new StaticShader();
    private EntityRenderer renderer;
    private StaticWaterShader waterShader = new StaticWaterShader();
    private WaterEntityRenderer waterRenderer;
    private SkyBoxRenderer skyBoxRenderer;
    private ShadowMapMasterRenderer shadowMapRenderer;

    private float moveFactor = 0;

    private Map<TexturedModel, List<Entity>> entities = Collections.synchronizedMap(new HashMap<>());
    private Map<TexturedModel, List<Entity>> waterEntities = Collections.synchronizedMap(new HashMap<>());

    public MasterRenderer(Loader loader, Camera camera, WaterFrameBuffers fbos) {
        enableCulling();
        createProjectionMatrix();
        renderer = new EntityRenderer(shader, projectionMatrix);
        waterRenderer = new WaterEntityRenderer(loader, waterShader, projectionMatrix, fbos);
        skyBoxRenderer = new SkyBoxRenderer(loader, projectionMatrix);
        shadowMapRenderer = new ShadowMapMasterRenderer(camera);
    }

    public void renderScene(ArrayList<Entity> solidEntities, ArrayList<Entity> liquidEntities, Light sun, ArrayList<Light> lights, Camera camera, WaterFrameBuffers fbos) {
        long renderStartTime = new Date().getTime();
        if (Settings.isEnableShadows()) {
            renderShadowMap(solidEntities, sun);
        }
        float waterLevel = Settings.getWaterLevel() + 1.01f;
        // render to reflection buffer
        if (Settings.isEnableWaterReflections()) {
            fbos.bindReflectionFrameBuffer();
            float distance = 2 * (camera.getPosition().y - waterLevel);
            camera.getPosition().y -= distance;
            camera.invertPitch();
            prepare();
            renderSolidScene(solidEntities, lights, camera, new Vector4f(0, 1, 0, -waterLevel));
            camera.getPosition().y += distance;
            camera.invertPitch();
        }

        // render to refraction buffer
        fbos.bindRefractionFrameBuffer();
        prepare();
        renderSolidScene(solidEntities, lights, camera, new Vector4f(0, -1, 0, waterLevel));

        // render to display
        fbos.unbindCurrentFrameBuffer();
        prepare();
        renderSolidScene(solidEntities, lights, camera, new Vector4f(0, 1, 0, 1000000));
        processWaterEntities(liquidEntities);
        renderWater(sun, camera);

        long renderEndTime = new Date().getTime();
        float frameRenderTime = renderEndTime - renderStartTime;
        averageRenderTime = ((averageRenderTime * totalRendersCompleted) + frameRenderTime) / (totalRendersCompleted + 1);
        totalRendersCompleted += 1;
    }

    public void renderSolidScene(ArrayList<Entity> solidEntities, ArrayList<Light> lights, Camera camera, Vector4f clipPlane) {
        processEntities(solidEntities);
        render(lights, camera, clipPlane);
    }

    public void render(List<Light> lights, Camera camera, Vector4f clipPlane) {
        // set up and use entity shader
        shader.start();
        shader.loadClipPlane(clipPlane);
        shader.loadSkyColour(RED, GREEN, BLUE);
        float m = 0.04f;
        float i = -0.18f;
        float b = -0.02f;
        shader.loadFogSettings(m * (float) Math.pow(Settings.getRenderDistance(), i) + b, 5.0f);
        shader.loadLights(lights);
        shader.loadViewMatrix(camera);
        renderer.render(entities, shadowMapRenderer.getToShadowMapSpaceMatrix());
        shader.stop();

        // render sky box
        skyBoxRenderer.render(camera, RED, GREEN, BLUE);

        // clean up caching
        entities.clear();
    }

    public void renderWater(Light sun, Camera camera) {
        // set up and use entity shader
        waterShader.start();
        moveFactor += WAVE_SPEED * DisplayManager.getFrameTimeSeconds();
        moveFactor %= 1;
        waterShader.loadMoveFactor(moveFactor);
        waterShader.loadVisualSettings(Settings.isEnableWaterReflections(), Settings.isEnableWaterDistortions());
        waterShader.loadSkyColour(RED, GREEN, BLUE);
        float m = 0.04f;
        float i = -0.18f;
        float b = -0.02f;
        waterShader.loadFogSettings(m * (float) Math.pow(Settings.getRenderDistance(), i) + b, 5.0f);
        waterShader.loadLight(sun);
        waterShader.loadViewMatrix(camera);
        waterRenderer.render(waterEntities);
        waterShader.stop();

        // clean up caching
        waterEntities.clear();
    }

    public void processEntities(ArrayList<Entity> entityList) {
        for (Entity entity : entityList) {
            processEntity(entity);
        }
    }

    public void processEntity(Entity entity) {
        TexturedModel entityModel = entity.getModel();
        List<Entity> batch = entities.get(entityModel);
        if (batch != null) {
            batch.add(entity);
        } else {
            List<Entity> newBatch = new ArrayList<>();
            newBatch.add(entity);
            entities.put(entityModel, newBatch);
        }
    }

    public void processWaterEntities(ArrayList<Entity> entityList) {
        for (Entity entity : entityList) {
            processWaterEntity(entity);
        }
    }

    public void processWaterEntity(Entity entity) {
        TexturedModel entityModel = entity.getModel();
        List<Entity> batch = waterEntities.get(entityModel);
        if (batch != null) {
            batch.add(entity);
        } else {
            List<Entity> newBatch = new ArrayList<>();
            newBatch.add(entity);
            waterEntities.put(entityModel, newBatch);
        }
    }

    public void renderShadowMap(ArrayList<Entity> entityList, Light sun) {
        processEntities(entityList);
        shadowMapRenderer.render(entities, sun);
        entities.clear();
    }

    public int getShadowMapTexture() {
        return shadowMapRenderer.getShadowMap();
    }

    public void cleanUp() {
        shader.cleanUp();
        shadowMapRenderer.cleanUp();
        skyBoxRenderer.cleanUp();
    }

    public static void enableCulling() {
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);
    }

    public static void disableCulling() {
        GL11.glDisable(GL11.GL_CULL_FACE);
    }

    private void createProjectionMatrix(){
        projectionMatrix = new Matrix4f();
        float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
        float y_scale = (float) ((1f / Math.tan(Math.toRadians(Settings.FOV / 2f))));
        float x_scale = y_scale / aspectRatio;
        float frustum_length = Settings.FAR_PLANE - Settings.NEAR_PLANE;

        projectionMatrix.m00 = x_scale;
        projectionMatrix.m11 = y_scale;
        projectionMatrix.m22 = -((Settings.FAR_PLANE + Settings.NEAR_PLANE) / frustum_length);
        projectionMatrix.m23 = -1;
        projectionMatrix.m32 = -((2 * Settings.NEAR_PLANE * Settings.FAR_PLANE) / frustum_length);
        projectionMatrix.m33 = 0;
    }

    public void prepare() {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(RED, GREEN, BLUE, 0);
        GL13.glActiveTexture(GL13.GL_TEXTURE5);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, getShadowMapTexture());
        GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
    }

    public static float getAverageRenderTimeMilliseconds() {
        return averageRenderTime;
    }

    // getters and setters

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

}
