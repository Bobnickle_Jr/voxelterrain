package Client.Rendering.Water;

import Client.Rendering.Shaders.ShaderProgram;
import Core.Control.Settings;
import Core.Entities.Camera;
import Core.Entities.Light;
import Core.Utils.Maths;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class StaticWaterShader extends ShaderProgram {

	private static final String VERTEX_FILE = Settings.getShaderFileLocation() + "/water/waterVertex.txt";
	private static final String FRAGMENT_FILE = Settings.getShaderFileLocation() + "/water/waterFragment.txt";

	private int location_transformationMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int location_reflectionTexture;
	private int location_refractionTexture;
	private int location_dudvMap;
	private int location_normalMap;
	private int location_depthMap;
	private int location_moveFactor;
	private int location_lightPosition;
	private int location_lightColour;
	private int location_fogDensity;
	private int location_fogGradient;
	private int location_skyColour;
	private int location_enableReflection;
	private int location_enableDistortion;
	private int location_near;
	private int location_far;

	public StaticWaterShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoordinates");
		super.bindAttribute(2, "normal");
		super.bindAttribute(3, "in_textureCoords");
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
		location_viewMatrix = super.getUniformLocation("viewMatrix");

		location_reflectionTexture = getUniformLocation("reflectionTexture");
		location_refractionTexture = getUniformLocation("refractionTexture");
		location_dudvMap = getUniformLocation("dudvMap");
		location_normalMap = getUniformLocation("normalMap");
		location_depthMap = getUniformLocation("depthMap");

		location_moveFactor = getUniformLocation("moveFactor");
		location_skyColour = super.getUniformLocation("skyColour");
		location_fogDensity = super.getUniformLocation("fogDensity");
		location_fogGradient = super.getUniformLocation("fogGradient");

		location_lightPosition = super.getUniformLocation("lightPosition");
		location_lightColour = super.getUniformLocation("lightColour");

		location_enableReflection = super.getUniformLocation("enableReflection");
		location_enableDistortion = super.getUniformLocation("enableDistortion");
		location_near = super.getUniformLocation("near");
		location_far = super.getUniformLocation("far");
	}

	public void loadVisualSettings(boolean enableReflection, boolean enableDistortion) {
		super.loadBoolean(location_enableReflection, enableReflection);
		super.loadBoolean(location_enableDistortion, enableDistortion);
	}

	public void loadPOVSettings(float near, float far) {
		super.loadFloat(location_near, near);
		super.loadFloat(location_far, far);
	}

	public void loadMoveFactor(float moveFactor) {
		super.loadFloat(location_moveFactor, moveFactor);
	}

	public void connectTextureUnits() {
		super.loadInt(location_reflectionTexture, 0);
		super.loadInt(location_refractionTexture, 1);
		super.loadInt(location_dudvMap, 2);
		super.loadInt(location_normalMap, 3);
		super.loadInt(location_depthMap, 4);
	}

	public void loadFogSettings(float fogDensity, float fogGradient) {
		super.loadFloat(location_fogDensity, fogDensity);
		super.loadFloat(location_fogGradient, fogGradient);
	}

	public void loadSkyColour(float r, float b, float g) {
		super.loadVector(location_skyColour, new Vector3f(r, b, g));
	}

	public void loadLight(Light light) {
		super.loadVector(location_lightPosition, light.getPosition());
		super.loadVector(location_lightColour, light.getColour());
	}

	public void loadTransformationMatrix(Matrix4f matrix){
		super.loadMatrix(location_transformationMatrix, matrix);
	}
	
	public void loadViewMatrix(Camera camera) {
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		super.loadMatrix(location_viewMatrix, viewMatrix);
	}
	
	public void loadProjectionMatrix(Matrix4f projection){
		super.loadMatrix(location_projectionMatrix, projection);
	}
	
	

}
