package Client.Rendering;

import Client.Rendering.Models.RawModel;
import Client.Rendering.Textures.TextureData;
import Core.Control.Settings;
import de.matthiasmann.twl.utils.PNGDecoder;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.*;

public class Loader {

	private List<Integer> vaos = Collections.synchronizedList(new ArrayList<>());
	private List<Integer> vbos = Collections.synchronizedList(new ArrayList<>());
	private List<Integer> textures = Collections.synchronizedList(new ArrayList<>());

	private Map<String, Texture> filePathCache = Collections.synchronizedMap(new HashMap<>());

	public int[] loadToVAO(float[] positions, float[] textureCoords) {
		
		int vaoID = createVAO();
		int vboID1 = storeDataInAttributeList(0, 2, positions);
		int vboID2 = storeDataInAttributeList(1, 2, textureCoords);
		unbindVAO();
		
		return new int[] {vaoID, vboID1, vboID2};
	}

	public RawModel loadToVAO(float[] positions, float[] textureCoords, float[] normals, int[] indices) {
		
		int vaoID = createVAO();
		int vboID = bindIndicesBuffer(indices);
		int vboID1 = storeDataInAttributeList( 0, 3, positions);
		int vboID2 = storeDataInAttributeList( 1, 2, textureCoords);
		int vboID3 = storeDataInAttributeList( 2, 3, normals);
		unbindVAO();
		
		return new RawModel(vaoID, vboID, vboID1, vboID2, vboID3, indices.length);
	}

	public RawModel loadToVAO(float[] positions, int dimensions) {
		
		int vaoID = createVAO();
		int vboID = this.storeDataInAttributeList(0, dimensions, positions);
		unbindVAO();
		
		return new RawModel(vaoID, vboID, positions.length / dimensions);
	}

	public Texture loadTexture(String fileName) {
		
		Texture texture = null;
		if (filePathCache.get(fileName) != null) {
			return filePathCache.get(fileName);
		}
		try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream(fileName + ".png"));
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Tried to load texture " + fileName + ".png , didn't work");
			System.exit(-1);
		}
		textures.add(texture.getTextureID());
		filePathCache.put(fileName, texture);

		
		return texture;
	}

	public void cleanUp(){
		
		for(int vao:vaos){
			GL30.glDeleteVertexArrays(vao);
		}
		for(int vbo:vbos){
			GL15.glDeleteBuffers(vbo);
		}
		for(int texture:textures){
			GL11.glDeleteTextures(texture);
		}
		
	}

	public void unloadVAO(Integer vao) {
		
		GL30.glDeleteVertexArrays(vao);
		vaos.remove(vao);
		
	}

	public void unloadVBOs(int[] vbos) {
		
		for (Integer vbo : vbos) {
			unloadVBO(vbo);
		}
		
	}

	private void unloadVBO(Integer vbo) {
		GL15.glDeleteBuffers(vbo);
		vbos.remove(vbo);
	}

	private int createVAO(){
		int vaoID = GL30.glGenVertexArrays();
		vaos.add(vaoID);
		GL30.glBindVertexArray(vaoID);
		return vaoID;
	}

	private int storeDataInAttributeList(int attributeNumber, int coordinateSize, float[] data){
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		FloatBuffer buffer = storeDataInFloatBuffer(data);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attributeNumber,coordinateSize, GL11.GL_FLOAT,false,0,0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		return vboID;
	}

	private void unbindVAO(){
		GL30.glBindVertexArray(0);
	}

	private int bindIndicesBuffer(int[] indices){
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = storeDataInIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		return vboID;
	}

	private IntBuffer storeDataInIntBuffer(int[] data){
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}

	private FloatBuffer storeDataInFloatBuffer(float[] data){
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}

	public int loadCubeMap(String[] textureFiles) {
		
		int texID = GL11.glGenTextures();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, texID);

		for (int i = 0; i < textureFiles.length; i += 1) {
			TextureData data = decodeTextureFile(Settings.getTextureFileLocation() + "/skybox/" + textureFiles[i] + ".png");
			GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL11.GL_RGBA, data.getWidth(),
					data.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, data.getBuffer());
		}
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR); // smooth
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		textures.add(texID);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		
		return texID;
	}

	private TextureData decodeTextureFile(String fileName) {
		int width = 0;
		int height = 0;
		ByteBuffer buffer = null;
		try {
			FileInputStream in = new FileInputStream(fileName);
			PNGDecoder decoder = new PNGDecoder(in);
			width = decoder.getWidth();
			height = decoder.getHeight();
			buffer = ByteBuffer.allocateDirect(4 * width * height);
			decoder.decode(buffer, width * 4, PNGDecoder.Format.RGBA);
			buffer.flip();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Tried to load texture " + fileName + ", didn't work");
			System.exit(-1);
		}
		return new TextureData(buffer, width, height);
	}


	// getters and setters

	public List<Integer> getVaos() {
		return vaos;
	}

	public List<Integer> getVbos() {
		return vbos;
	}

	public List<Integer> getTextures() {
		return textures;
	}

}
