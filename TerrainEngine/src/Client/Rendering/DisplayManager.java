package Client.Rendering;

import Core.Control.Settings;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.*;

public class DisplayManager {

	private static long lastFrameTime = getCurrentTime();
	private static float delta;
	
	public static void createDisplay(){		
		ContextAttribs attribs = new ContextAttribs(3,2)
		.withForwardCompatible(true)
		.withProfileCore(true);
		try {
			Display.setDisplayMode(new DisplayMode(Settings.getDisplayWidth(), Settings.getDisplayHeight()));
			Display.create(new PixelFormat(), attribs);
			Display.setTitle(Settings.getAppTitle() + " - " + Settings.getAppVersion());
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		GL11.glViewport(0,0, Settings.getDisplayWidth(), Settings.getDisplayHeight());
	}
	
	public static void updateDisplay() {
		Display.sync(Settings.getFpsCap());
		Display.update();
		long currentFrameTime = getCurrentTime();
		delta = (currentFrameTime - lastFrameTime) / 1000f;
		lastFrameTime = currentFrameTime;
	}

	public static float getFrameTimeSeconds() {
		return delta;
	}

	private static long getCurrentTime() {
		return Sys.getTime() * 1000 / Sys.getTimerResolution();
	}

	public static void closeDisplay(){
		Display.destroy();
	}

}
