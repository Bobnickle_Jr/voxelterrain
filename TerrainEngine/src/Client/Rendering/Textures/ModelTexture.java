package Client.Rendering.Textures;

public class ModelTexture {
	
	private int textureID;

	private float shineDamper = 1;
	private float reflectivity = 0;

	private boolean hasTransparency = false;
	private boolean useFakeLighting = false;
	
	public ModelTexture(int texture){
		this.textureID = texture;
	}

	// getter and setters

	public boolean isUseFakeLighting() {
		return useFakeLighting;
	}

	public boolean isHasTransparency() {
		return hasTransparency;
	}

	public int getTextureID() {
		return textureID;
	}

	public float getShineDamper() {
		return shineDamper;
	}

	public float getReflectivity() {
		return reflectivity;
	}

}
