package Client.Rendering.GUIs.fontRendering;

import Client.Rendering.GUIs.fontMeshCreator.FontType;
import Client.Rendering.GUIs.fontMeshCreator.GUIText;
import Client.Rendering.GUIs.fontMeshCreator.TextMeshData;
import Client.Rendering.Loader;

import java.util.*;

public class TextMaster {
	
	private static Loader loader;
	private static Map<FontType, List<GUIText>> texts = new HashMap<FontType, List<GUIText>>();
	private static FontRenderer renderer;
	
	public static void init(Loader theLoader){
		renderer = new FontRenderer();
		loader = theLoader;
	}
	
	public static void render(){
		renderer.render(texts);
	}

	public static void loadText(GUIText text){
		FontType font = text.getFont();
		TextMeshData data = font.loadText(text);
		int[] vaoData = loader.loadToVAO(data.getVertexPositions(), data.getTextureCoords());
		int vao = vaoData[0];
		int[] vbos = Arrays.copyOfRange(vaoData, 1, vaoData.length);
		text.setMeshInfo(vao, data.getVertexCount(), vbos);
		List<GUIText> textBatch = texts.computeIfAbsent(font, k -> new ArrayList<>());
		textBatch.add(text);
	}

	public static void removeText(GUIText text){
		List<GUIText> textBatch = texts.get(text.getFont());
		textBatch.remove(text);
		loader.unloadVAO(text.getMesh());
		loader.unloadVBOs(text.getVbos());
	}
	
	public static void cleanUp(){
		renderer.cleanUp();
	}

}
