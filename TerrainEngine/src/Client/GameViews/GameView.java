package Client.GameViews;

import Client.Gui.GuiOverlay;
import Client.Rendering.GUIs.GuiRenderer;
import Client.Rendering.Loader;
import Client.Rendering.MasterRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Entities.Camera;
import Core.Utils.MousePicker;

public class GameView {

    protected ViewManager viewManager;
    protected GuiOverlay guiOverlay;

    public GameView(ViewManager viewManager, boolean setupGui) {
        this.viewManager = viewManager;
        if (setupGui) {
            setUpGui();
        }
    }

    public GameView(ViewManager viewManager) {
        this.viewManager = viewManager;
        setUpGui();
    }

    public void update(MasterRenderer renderer, GuiRenderer guiRenderer, Camera camera, Loader loader, MousePicker picker, WaterFrameBuffers fbos) {
        if (guiOverlay != null) {
            guiOverlay.update();
            guiOverlay.draw();
        }
    }

    protected void setUpGui() { }

    protected void closeView() { }
}
