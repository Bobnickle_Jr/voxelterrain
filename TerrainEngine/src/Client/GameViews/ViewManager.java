package Client.GameViews;

import Client.Gui.GuiUtils;
import Client.Rendering.DisplayManager;
import Client.Rendering.GUIs.GuiRenderer;
import Client.Rendering.GUIs.fontRendering.TextMaster;
import Client.Rendering.Loader;
import Client.Rendering.MasterRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Control.Settings;
import Core.Entities.Camera;
import Core.Utils.Logging.Log;
import Core.Utils.MousePicker;
import org.lwjgl.opengl.Display;

import java.util.ArrayList;

public class ViewManager {

    private ArrayList<GameView> viewStack = new ArrayList<>();

    /**
     * Push a view to the top of the game view stack
     */
    public void pushView(GameView gameView) {
        viewStack.add(0, gameView);
    }

    /**
     * Pop a view from the top of the game view stack
     */
    public void popView() {
        getCurrentView().closeView();
        viewStack.remove(getCurrentView());
    }

    /**
     * Start the main game loop as managed by the view manager
     * Each frame, update the game view at the top of the stack.
     * Ends when the stack is empty, or exit is requested by some other means.
     */
    public void start(MasterRenderer renderer, GuiRenderer guiRenderer, Camera camera, Loader loader, MousePicker picker, WaterFrameBuffers fbos) {
        // main game loop
        Log.info("Starting Main Loop...");
        while (!Settings.isExitRequested() && !Display.isCloseRequested() && getCurrentView() != null) {
            GuiUtils.clearCache();
            Settings.getControlScheme().updateButtons();

            GameView gameView = getCurrentView();
            gameView.update(renderer, guiRenderer, camera, loader, picker, fbos);

            // render
            guiRenderer.render(GuiUtils.getGuiTextures());
            TextMaster.render();
            DisplayManager.updateDisplay();
        }
        Log.info("Exited Main Loop.");
    }

    /**
     * Get the game view at the top of the stack
     */
    public GameView getCurrentView() {
        return viewStack.size() != 0 ? viewStack.get(0) : null;
    }
}
