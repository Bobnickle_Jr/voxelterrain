package Client.GameViews.Menus;

import Client.GameViews.GameView;
import Client.GameViews.ViewManager;
import Client.Gui.GuiOverlay;
import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Client.Gui.Widgets.*;
import Client.Rendering.GUIs.GuiRenderer;
import Client.Rendering.Loader;
import Client.Rendering.MasterRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Control.Settings;
import Core.Entities.Camera;
import Core.Utils.MousePicker;
import org.lwjgl.util.vector.Vector2f;

import static Client.Rendering.GUIs.fontRendering.TextAlign.TA_CENTERED;
import static Client.Rendering.GUIs.fontRendering.TextAlign.TA_LEFT;

public class MainMenuGameView extends GameView {

    private GuiWidget menuButtons;

    public MainMenuGameView(ViewManager viewManager) {
        super(viewManager);
        guiOverlay = new GuiOverlay();

        guiOverlay.addChild(new GuiImage(new Vector2f(0,0), GuiTexturePredef.GTP_MAIN_MENU_BACKGROUND, 1));

        GuiWidget titleBox = guiOverlay.addChild(new GuiWidgetBox(new Vector2f(0, 0.5f)));
        titleBox.addChild(new GuiLabel(new Vector2f(0, 0), Settings.getAppTitle() + " - " + Settings.getAppVersion(), GuiTexturePredef.GTP_384x64_BEIGE, 2, TA_CENTERED));

        menuButtons = guiOverlay.addChild(new GuiWidgetBox(new Vector2f(0, -0.75f)));
        menuButtons.addChild(new GuiButton(new Vector2f(0, 0.4f), "Create New World", GuiTexturePredef.GTP_256x32_BROWN));
        menuButtons.addChild(new GuiButton(new Vector2f(0, 0.3f), "Load World", GuiTexturePredef.GTP_256x32_BROWN));
        menuButtons.addChild(new GuiButton(new Vector2f(0, 0.2f), "Settings", GuiTexturePredef.GTP_256x32_BROWN));
        menuButtons.addChild(new GuiButton(new Vector2f(0, 0.1f), "Exit To Desktop", GuiTexturePredef.GTP_256x32_RED));

        guiOverlay.addChild(new GuiCopyrightWidget());
    }

    @Override
    public void update(MasterRenderer renderer, GuiRenderer guiRenderer, Camera camera, Loader loader, MousePicker picker, WaterFrameBuffers fbos) {
        super.update(renderer, guiRenderer, camera, loader, picker, fbos);

        GuiWidget pressedChild = menuButtons.getPressedChild();
        if (pressedChild != null) {
            switch (pressedChild.getText()) {
                case "Create New World":
                    viewManager.pushView(new CreateWorldGameView(viewManager));
                    break;
                case "Load World":
                    viewManager.pushView(new LoadWorldGameView(viewManager));
                    break;
                case "Settings":
                    viewManager.pushView(new SettingsGameView(viewManager));
                    break;
                case "Exit To Desktop":
                    Settings.setExitRequested(true);
                    break;
            }
        }

    }
}
