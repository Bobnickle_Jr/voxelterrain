package Client.GameViews.Menus;

import Client.GameViews.GameView;
import Client.GameViews.ViewManager;
import Client.Gui.GuiOverlay;
import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Client.Gui.Widgets.*;
import Client.Rendering.GUIs.GuiRenderer;
import Client.Rendering.Loader;
import Client.Rendering.MasterRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Control.GameWorld;
import Core.Entities.Camera;
import Core.Terrain.Data.WorldType;
import Core.Utils.MousePicker;
import org.lwjgl.util.vector.Vector2f;

import static Client.Rendering.GUIs.fontRendering.TextAlign.TA_CENTERED;

public class CreateWorldGameView extends GameView {

    private GuiWidget menuButtons;
    private GuiWidget worldTypeDescription;

    public CreateWorldGameView(ViewManager viewManager) {
        super(viewManager);
        guiOverlay = new GuiOverlay();

        guiOverlay.addChild(new GuiImage(new Vector2f(0,0), GuiTexturePredef.GTP_MAIN_MENU_BACKGROUND, 1));


        guiOverlay.addChild(new GuiLabel(new Vector2f(0, 0.45f), "Create New World", null, 2, TA_CENTERED));

        menuButtons = guiOverlay.addChild(new GuiWidgetBox(new Vector2f(0, -0.55f)));
        menuButtons.addChild(new GuiImage(new Vector2f(0, 0.55f), GuiTexturePredef.GTP_400x400_BROWN, 1));

        menuButtons.addChild(new GuiLabel(new Vector2f(0, 0.9f), "World Name:", null, 1, TA_CENTERED));
        menuButtons.addChild(new GuiTextBox(new Vector2f(0, 0.825f), "Name", "New World", 18, GuiTexturePredef.GTP_256x32_BEIGE, 1));

        menuButtons.addChild(new GuiLabel(new Vector2f(0, 0.7f), "Seed:", null, 1, TA_CENTERED));
        menuButtons.addChild(new GuiTextBox(new Vector2f(0, 0.625f), "Seed","Use Random Seed", 18, GuiTexturePredef.GTP_256x32_BEIGE, 1));

        menuButtons.addChild(new GuiLabel(new Vector2f(0, 0.45f), "Generation Options:", null, 1, TA_CENTERED));
        menuButtons.addChild(new GuiToggleButton(new Vector2f(0f, 0.375f), "World Type", GuiTexturePredef.GTP_192x32_BROWN, 0, WorldType.values()));
        worldTypeDescription = menuButtons.addChild(new GuiLabel(new Vector2f(0, 0.3f), WorldType.values()[0].getDescription(), null, 1, TA_CENTERED));

        menuButtons.addChild(new GuiButton(new Vector2f(-0.15f, 0.1f), "Back", GuiTexturePredef.GTP_192x32_RED));
        menuButtons.addChild(new GuiButton(new Vector2f(0.15f, 0.1f), "Create World", GuiTexturePredef.GTP_192x32_RED));

        guiOverlay.addChild(new GuiCopyrightWidget());
    }

    @Override
    public void update(MasterRenderer renderer, GuiRenderer guiRenderer, Camera camera, Loader loader, MousePicker picker, WaterFrameBuffers fbos) {
        super.update(renderer, guiRenderer, camera, loader, picker, fbos);

        GuiWidget pressedChild = menuButtons.getPressedChild();
        if (pressedChild != null) {
            switch (pressedChild.getName()) {
                case "World Type": {
                    WorldType worldType = (WorldType) ((GuiToggleButton) menuButtons.getChildByName("World Type")).getCurrentValue();
                    worldTypeDescription.setName(worldType.getDescription());
                    break;
                }
                case "Create World": {
                    viewManager.popView();
                    String sSeed = ((GuiTextBox) menuButtons.getChildByName("Seed")).getValue();
                    String fileName = ((GuiTextBox) menuButtons.getChildByName("Name")).getValue();
                    WorldType worldType = (WorldType) ((GuiToggleButton) menuButtons.getChildByName("World Type")).getCurrentValue();
                    GameWorld currentGameWorld = GameWorld.createGameWorld(sSeed, fileName, loader, viewManager, worldType);
                    currentGameWorld.init(camera);
                    viewManager.pushView(currentGameWorld);
                    break;
                }
                case "Back": {
                    viewManager.popView();
                    break;
                }
            }
        }

    }
}
