package Client.GameViews.Menus;

import Client.GameViews.GameView;
import Client.GameViews.ViewManager;
import Client.Gui.GuiOverlay;
import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Client.Gui.Widgets.*;
import Client.Rendering.GUIs.GuiRenderer;
import Client.Rendering.Loader;
import Client.Rendering.MasterRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Entities.Camera;
import Core.Utils.MousePicker;
import org.lwjgl.util.vector.Vector2f;

import static Client.Rendering.GUIs.fontRendering.TextAlign.TA_CENTERED;

public class SettingsGameView extends GameView {

    private GuiWidget menuButtons;

    public SettingsGameView(ViewManager viewManager) {
        super(viewManager);
        guiOverlay = new GuiOverlay();
        guiOverlay.addChild(new GuiImage(new Vector2f(0,0), GuiTexturePredef.GTP_MAIN_MENU_BACKGROUND, 1));
        guiOverlay.addChild(new GuiLabel(new Vector2f(0, 0.45f), "Settings", null, 2, TA_CENTERED));

        menuButtons = guiOverlay.addChild(new GuiSettingsWidget(new Vector2f(0, 0)));

        guiOverlay.addChild(new GuiCopyrightWidget());
    }

    @Override
    public void update(MasterRenderer renderer, GuiRenderer guiRenderer, Camera camera, Loader loader, MousePicker picker, WaterFrameBuffers fbos) {
        super.update(renderer, guiRenderer, camera, loader, picker, fbos);

        GuiWidget pressedButton = menuButtons.getPressedChild();
        if (pressedButton != null) {
            if ("Back".equals(pressedButton.getName())) {
                viewManager.popView();
            }
        }
    }
}
