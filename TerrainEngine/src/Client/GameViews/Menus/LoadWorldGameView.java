package Client.GameViews.Menus;

import Client.GameViews.GameView;
import Client.GameViews.ViewManager;
import Client.Gui.GuiOverlay;
import Client.Gui.GuiWidget;
import Client.Gui.Utils.GuiTexturePredef;
import Client.Gui.Widgets.*;
import Client.Rendering.GUIs.GuiRenderer;
import Client.Rendering.Loader;
import Client.Rendering.MasterRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Control.GameWorld;
import Core.Entities.Camera;
import Core.Utils.Logging.Log;
import Core.Utils.MousePicker;
import Core.Utils.Saving.SaveFileUtils;
import Core.Utils.Saving.WorldMetaData;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;

import static Client.Rendering.GUIs.fontRendering.TextAlign.TA_CENTERED;
import static Client.Rendering.GUIs.fontRendering.TextAlign.TA_LEFT;

public class LoadWorldGameView extends GameView {

    private GuiWidget menuButtons;
    GuiScrollableWidgetBox saveFilesList;

    public LoadWorldGameView (ViewManager viewManager) {
        super(viewManager);
        guiOverlay = new GuiOverlay();

        guiOverlay.addChild(new GuiImage(new Vector2f(0,0), GuiTexturePredef.GTP_MAIN_MENU_BACKGROUND, 1));

        guiOverlay.addChild(new GuiLabel(new Vector2f(0, 0.45f), "Load World", null, 2, TA_CENTERED));

        saveFilesList = (GuiScrollableWidgetBox) guiOverlay.addChild(new GuiScrollableWidgetBox(new Vector2f(0, 0), GuiTexturePredef.GTP_400x400_BROWN, 1));

        ArrayList<WorldMetaData> saveFiles = SaveFileUtils.listAllSaveFiles();
        for (WorldMetaData saveFile : saveFiles) {
            saveFilesList.addScrollableChild(new GuiButton(new Vector2f(), saveFile.getSaveFileName(), GuiTexturePredef.GTP_256x64_BROWN));
        }

        menuButtons = guiOverlay.addChild(new GuiWidgetBox(new Vector2f(0, -0.55f)));
        menuButtons.addChild(new GuiButton(new Vector2f(0.15f, 0.1f), "Load World", GuiTexturePredef.GTP_192x32_BROWN));
        menuButtons.addChild(new GuiButton(new Vector2f(-0.15f, 0.1f), "Back", GuiTexturePredef.GTP_192x32_BROWN));

        guiOverlay.addChild(new GuiCopyrightWidget());
    }

    @Override
    public void update(MasterRenderer renderer, GuiRenderer guiRenderer, Camera camera, Loader loader, MousePicker picker, WaterFrameBuffers fbos) {
        super.update(renderer, guiRenderer, camera, loader, picker, fbos);

        GuiWidget pressedChild = menuButtons.getPressedChild();
        if (pressedChild != null) {
            switch (pressedChild.getText()) {
                case "Load World":
                    GuiWidget guiWidget = saveFilesList.getSelectedScrollableChild();
                    if (guiWidget != null) {
                        viewManager.popView();
                        String fileName = guiWidget.getName();
                        GameWorld currentGameWorld = GameWorld.loadGameWorld(fileName, loader, viewManager);
                        if (currentGameWorld != null) {
                            currentGameWorld.init(camera);
                            viewManager.pushView(currentGameWorld);
                        } else {
                            Log.error("Failed to load world: " + fileName);
                        }
                    }
                    break;
                case "Back":
                    viewManager.popView();
                    break;
            }
        }

    }
}
