package Client.Control;

import Client.GameViews.GameView;
import Client.GameViews.Menus.MainMenuGameView;
import Client.GameViews.ViewManager;
import Client.Gui.GuiUtils;
import Client.Rendering.DisplayManager;
import Client.Rendering.GUIs.GuiRenderer;
import Client.Rendering.GUIs.fontRendering.TextMaster;
import Client.Rendering.Loader;
import Client.Rendering.MasterRenderer;
import Client.Rendering.Water.WaterFrameBuffers;
import Core.Control.Settings;
import Core.Entities.Camera;
import Core.Utils.Logging.Log;
import Core.Utils.MousePicker;

public class Main {

    public static void main(String... args) {
        // initialise
        Log.info("Initializing...");
        DisplayManager.createDisplay();
        Loader loader = new Loader();
        TextMaster.init(loader);
        GuiUtils.init(loader, Settings.getDisplayWidth(), Settings.getDisplayHeight());
        GuiRenderer guiRenderer = new GuiRenderer(loader);
        Camera camera = new Camera();
        WaterFrameBuffers fbos = new WaterFrameBuffers();
        MasterRenderer renderer = new MasterRenderer(loader, camera, fbos);
        MousePicker picker = new MousePicker(camera, renderer.getProjectionMatrix());
        Log.info("Initialization Complete.");

        // setup background thread
        BackgroundThread backgroundThread = new BackgroundThread();
        backgroundThread.start();

        // run
        ViewManager viewManager = new ViewManager();
        GameView mainMenu = new MainMenuGameView(viewManager);
        viewManager.pushView(mainMenu);
        viewManager.start(renderer, guiRenderer, camera, loader, picker, fbos);

        // clean up
        Log.info("Cleaning Up Resources...");
        TextMaster.cleanUp();
        fbos.cleanUp();
        guiRenderer.cleanUp();
        renderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();

        // exit
        Log.info("Exiting...");
    }
}
