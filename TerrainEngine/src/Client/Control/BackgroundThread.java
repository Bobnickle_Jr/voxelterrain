package Client.Control;

import Core.Control.Settings;
import Core.Terrain.TerrainManager;
import Core.Utils.Logging.Log;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

public class BackgroundThread {

    public BackgroundThread() {}

    /**
     * Start running the background thread
     * Once the Terrain Manager has been initialised, the background thread will update 
     * the terrain manager every tick (10 milliseconds)
     */
    public void start() {
        new Thread(() -> {
            Log.info("Background Thread started...");

            long lastTick = System.currentTimeMillis();
            while (Display.isCreated() && !Settings.isExitRequested() ) {
                long time = System.currentTimeMillis();
                // Update every 10 milliseconds
                if ( time - lastTick < 10 ) {
                    try {
                        Thread.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }

                if (TerrainManager.isInitialised()) {
                    Vector3f centerCoord = TerrainManager.getCurrentWorld().getPlayer().getPosition();
                    TerrainManager.updateLoadedChunks(centerCoord.x, centerCoord.z, true);
                }

                lastTick = System.currentTimeMillis();
            }

            Log.info("Background Thread closed...");
        }).start();
    }
}